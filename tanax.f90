MODULE gcmmn
  DOUBLE PRECISION, ALLOCATABLE :: x(:),y(:),z(:)
  DOUBLE PRECISION :: xbound1,xbound2,ybound1,ybound2
  DOUBLE PRECISION :: zbound1,zbound2
  INTEGER, ALLOCATABLE :: edge_connectivity1(:)
  INTEGER, ALLOCATABLE :: edge_connectivity2(:)
  INTEGER, ALLOCATABLE :: num_edge_points(:)
  INTEGER :: max_num_points_per_seg,max_sampled_points_per_seg
  INTEGER :: num_vessels,num_points
  INTEGER :: number_bott_nodes,extra_points
  INTEGER :: num_nodes,num_segments,max_segments
  INTEGER :: num_segments_old, num_nodes_old
  INTEGER :: xscale, yscale,zscale
  INTEGER :: multFact, hardLim
  LOGICAL :: new_tag
  DOUBLE PRECISION :: bottom_z,top_z
  DOUBLE PRECISION :: pit_dist,pit_dist_sq
  DOUBLE PRECISION :: end_tol,voxel_size,thetaguess
  DOUBLE PRECISION :: neighbor_factor,max_radius
  DOUBLE PRECISION :: vess_end_prob,convert_factor
  DOUBLE PRECISION :: xcenter, ycenter
  DOUBLE PRECISION, ALLOCATABLE :: xcenter_interp(:)
  DOUBLE PRECISION, ALLOCATABLE :: ycenter_interp(:)
  INTEGER:: num_backbone_nodes,num_backbone_segs
  INTEGER:: num_bridge_nodes,num_bridge_segs
  LOGICAL, ALLOCATABLE :: boundary_label(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: coordx(:,:),coordy(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: radius(:,:),coordz(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: coordx_in(:,:),coordy_in(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: coordz_in(:,:),radius_in(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: v_diam(:)
  INTEGER :: sampling_freq,bridge_tag,pitting_tag,number_begin_nodes
  INTEGER, ALLOCATABLE :: vessel_begin(:), vessel_finish(:)
  LOGICAL, ALLOCATABLE :: node_used(:)
  DOUBLE PRECISION ::  min_conn_length
  INTEGER :: num_vess_conns,cutoff_param,maxpoints,pit_seg_counter 
  INTEGER, ALLOCATABLE :: vessel1(:),vessel2(:),pit_no(:)
  INTEGER, ALLOCATABLE :: coordstart(:), coordend(:)
  DOUBLE PRECISION, ALLOCATABLE :: pit_z(:,:)
  INTEGER, ALLOCATABLE :: pit_node(:,:)
  INTEGER, ALLOCATABLE :: num_pitted_segs(:)
  INTEGER, ALLOCATABLE :: num_pitted_segs_new(:)
  INTEGER, ALLOCATABLE :: num_pit_connections(:)
  INTEGER, ALLOCATABLE :: num_pits_vv(:,:)
  INTEGER, ALLOCATABLE :: num_vv_connections(:)
END MODULE gcmmn

PROGRAM convert_am_file
  USE gcmmn
  IMPLICIT NONE
  
  !       Read input files
  call read_input
  
  !       Remove all repeated nodes, unconnected segments, segments connected to the same node, and duplicate segments
  !       Note that in the future some of these duplicate segments will be removed through the loop_segment subroutine
  call remove_segs

  !       Build the network from the bottom up & remove the nodes in between as you go
  call renumber

  if(bridge_tag.eq.1) then
     
     !       Bridges subroutine
     call bridges
     !       Build the network from the bottom up
     call renumber_bridges
     
  end if
  
  !       Extend vessels to the boundaries of the domain
  call extend_vessels

  !       Designate nodes based on sampling parameter
  call sampling

  !       Calculate vessel angle
  call vessel_angle

  !       Calculate mean vessel diameter and standard deviation
  call vessel_diameter

  !       Double up monomers?
  !	call polymer
  
  !       Vessel-Vessel Pitfield
  
  if(pitting_tag.eq.1) then
     !       Establish vessel-vessel connections
     call pitfield
  end if
  !       Write avizo export file
  call avizo_export

  if(pitting_tag.eq.1) then
     !       Pitfield changes
     call model_pitfield
  end if

  !       Designate nodes as bottom, internal, or top nodes
  call specify_nodes

  !       Write avizo export file
  call avizo_flow_input

  !	Deallocate variables
  call outro
  
END PROGRAM convert_am_file





SUBROUTINE read_input
  USE gcmmn
  IMPLICIT NONE
  INTEGER :: i,k
  character (len=20):: vertex
  character (len=18):: edge
  character (len=19):: points
  character (len=6) :: vertex2,edge2
  character (len=6) :: points2
  CHARACTER(Len=11) :: charc
  INTEGER :: IOSTATUS
  
  
  !       Read input file
  open(unit=10,file='Inputs.txt',status='old')

  !       Conversion factor 
  read(10,*) convert_factor  !converts from voxel size in non-meter units to meter units
  
  !       The center coordinates can be determined using a plumbline centroid method. These are used for determining pit angles and radial coordinates.
  read(10,*) xcenter  !x coordinate of the center at z=zmin slice
  read(10,*) ycenter  !y coordinate of the center at z=zmin slice
  
  !       thetaguess gives the estimated angle of drift of the vessels. This is used in the renumber routine to select 
  !       the appropriate segments to construct the backbone vessels that span the whole length of the segment of the stem. 
  !       Note that the output file avgangle.txt is useful in determining the appropriate thetaguess (in an iterative process)
  read(10,*) thetaguess
  
  !       Voxel_size should correspond to the voxel_size used in Avizo & from the CT scan
  read(10,*) voxel_size
  
  If(voxel_size.lt.0) then
     PRINT *, 'Error: Voxel size must be greater than 0.'
     pause
  end if
  
  !       Because the vessels in Avizo often end before the end of the domain of the boundary, this variable allows for an extra
  !       cushion to look for nodes that are near the end of the domain
  read(10,*) end_tol
  
  If(end_tol.lt.0) then
     PRINT *, 'Error: End tolerance must be greater than 0.'
     pause
  end if
  
  !       extra_points is a cushion variable to add extra storage to several matrices. 
  !       This is required because avizo sometimes generates more points than there are voxel slices
  read(10,*,IOSTAT=IOSTATUS) extra_points
  
  If(extra_points.lt.0) then
     PRINT *, 'Error: Extra points must be greater than or equal to 0.'
     pause
  end if
  
  IF(IOSTATUS>0) then
     PRINT *, 'ERROR: Invalid input. Number of extra points must be an integer.'
     pause
  end if
  
  read(10,*,IOSTAT=IOSTATUS) pitting_tag
  IF(IOSTATUS>0) then
     PRINT *, 'ERROR: Invalid input. Pitting tag must be an integer.'
     pause
  end if
  if(pitting_tag.eq.1) then
     
     !       Sampling_freq is the frequency that the program looks for vessel-vessel connections as well as measures things like average angle, etc.
     read(10,*,IOSTAT=IOSTATUS) sampling_freq
     
     IF(IOSTATUS>0) then
        PRINT *, 'ERROR: Invalid input. Sampling frequency must be an integer.'
        pause
     end if
     
     !       max_radius is used in the pitfield routine to determine the dimensions of the 
     !       box that defines where the program searches for possible boundary voxels
     read(10,*) max_radius
     
     If(max_radius.lt.0) then
        PRINT *, 'Error: Maximum radius must be greater than 0.'
        pause
     end if
     
     !       pit_dist is the threshold set by the user (determined experimentally with SEM) for what is 
     !       the maximum distance allowed to establish a vessel-vessel connection
     read(10,*) pit_dist
     
     If(pit_dist.lt.0) then
        PRINT *, 'Error: Pit distance must be greater than 0.'
        pause
     end if
     
     !       neighbor_factor is a multiplicative factor for searching for possible vessel-vessel connections 
     !       (e.g. a neighbor_factor of 1.5 would perform the detailed voxel by voxel analysis on any vessels 
     !       with a center to center distance less than or equal to 1.5 times the sum of the diameters plus the pit_dist).
     read(10,*) neighbor_factor
     IF(neighbor_factor.le.1) then
        PRINT *, 'ERROR: Invalid input. The neighbor_factor must be greater than 1.'
     end if
     
     read(10,*, IOSTAT=IOSTATUS) cutoff_param !parameter for adding missing connections if they are surrounded by connections
     IF(IOSTATUS>0) then
        PRINT *, 'ERROR: Invalid input. Cutoff parameter must be an integer.'
        pause
     end if
     read(10,*) min_conn_length !elimination of connections if the connection is too short
     If((cutoff_param.lt.1).or.(cutoff_param.gt.10)) then
        PRINT *, 'Error: Incorrect cutoff parameter. Please input an integer value between 1 and 10.'
        pause
     end if
     ! Bridges are an anatomical feature found in grapevine. These are short vessel elements that often 
     
     read(10,*) bridge_tag ! If bridge tag is 0, then the program does not search for bridges. If it is 1, then the program looks for bridges.
     if((bridge_tag.ne.0) .and. (bridge_tag.ne.1)) then
        PRINT *, 'ERROR: Bridge_tag must be either 0 or 1. 0=no bridges, 1=bridges.'
        pause
     end if
  elseif(pitting_tag.eq.0) then 
     sampling_freq=1 !Sample every point
  else
     PRINT *, 'ERROR: Pitting_tag must be either 0 or 1. 0=no vessel-vessel connections, 1=vessel-vessel connections.'
     pause
  end if
  
  
  if(IOSTATUS<0) then
     PRINT *, 'ERROR: Reached end of Inputs file. Please check to make sure all parameters are included'
     pause
  end if
  
  !       Store a few parameters in a file for the flow program
  open(unit=74,file='flowparam.txt',status='unknown')
  write(74,*) convert_factor
  write(74,*) xcenter
  write(74,*) ycenter
  write(74,*) voxel_size
  write(74,*) sampling_freq
  write(74,*) bridge_tag
  close(74)
  
  !       Add one voxel to the pit distance because the boundary voxels
  !       are on the inside (i.e. vessel side)
  pit_dist=pit_dist+voxel_size
  
  !       Find boundaries of domain with distance map
  
  open(unit=47,file='distmap.am',status='old')
  
  do i=1,7
     read(47,*)
  end do
  
  !       Read the boundaries of the distance map
  read(47,*,IOSTAT=IOSTATUS)charc,xbound1,xbound2,ybound1,ybound2,zbound1,zbound2
  if(IOSTATUS>0) then !Chamfer factor was specified
     backspace(47)
     new_tag=.true.
     read(47,*)charc,xbound1,xbound2,ybound1,ybound2,zbound1,zbound2
  end if
  xbound1=xbound1*convert_factor
  xbound2=xbound2*convert_factor
  ybound1=ybound1*convert_factor
  ybound2=ybound2*convert_factor
  zbound1=zbound1*convert_factor
  zbound2=zbound2*convert_factor
  
  close(47)
  
  if(end_tol.ge.(zbound2-zbound1)) then
     PRINT *,'Error: End tolerance is greater than the length of the domain. Change end tolerance to a smaller value'
  end if
  
  pit_dist_sq=pit_dist*pit_dist
  !       Add extra points because sometimes there are more points than there are voxel slices
  !       Add two points for the beginning/end points
  max_num_points_per_seg=INT((zbound2-zbound1)/voxel_size+2+extra_points)
  max_sampled_points_per_seg=INT((zbound2-zbound1)/(voxel_size*sampling_freq)+2+extra_points)
  open(unit=11,file='Input.am',status='old')
  
  !       Read intro part of file
  
  do i=1,3
     read(11,*)
  end do
  read(11,"(A20)") vertex
  read(11,"(A18)") edge
  read(11,"(A19)") points
  
  do i=1,13
     read(11,*)
  end do
  
  vertex2=vertex(15:20)
  edge2=edge(13:18)
  points2=points(13:18)
  read(vertex2,*) num_nodes
  read(edge2,*) num_segments
  read(points2,*) num_points
  
  !       CHANGE THIS EVENTUALLY TO ALLOW FOR VARIABLE NUMBERS OF SEGMENTS, etc.
  multFact=100;
  ALLOCATE(x(multFact*num_nodes))
  ALLOCATE(y(multFact*num_nodes))
  ALLOCATE(z(multFact*num_nodes))
  ALLOCATE(edge_connectivity1(multFact*num_segments))
  ALLOCATE(edge_connectivity2(multFact*num_segments))
  ALLOCATE(num_edge_points(multFact*num_segments))
  
  max_segments=multFact*num_segments
  hardLim=1000000

  edge_connectivity1=0
  edge_connectivity2=0
  num_edge_points=0
  
  !       Coordinates of vertices
  do 12 i=1,num_nodes
     read(11,*,iostat=k) x(i),y(i),z(i)
     !       convert units to meters
     x(i)=x(i)*convert_factor
     y(i)=y(i)*convert_factor
     z(i)=z(i)*convert_factor
12   continue
     
     read(11,*)
     read(11,*)
     
     !       Connections between vertices
     do 13 i=1,num_segments
        read(11,*,iostat=k) edge_connectivity1(i),edge_connectivity2(i)
        IF(k>0) exit
        !       Add one because the export format starts with index 0
        edge_connectivity1(i)=edge_connectivity1(i)+1
        edge_connectivity2(i)=edge_connectivity2(i)+1
13	continue
        
      END SUBROUTINE read_input
      
      
      
 SUBROUTINE remove_segs
 USE gcmmn
 IMPLICIT NONE
 INTEGER:: i,j,k
 DOUBLE PRECISION :: point_thick
 INTEGER, ALLOCATABLE :: num_edge_points_new(:)
 DOUBLE PRECISION :: blank1,blank2,blank3
 LOGICAL :: radius_remove_tag
 
 ALLOCATE(num_edge_points_new(multFact*num_segments))
 
 !       radius filter
 !       read the rest & eliminate anything on the fly that has minimum radius
 
 read(11,*)
 read(11,*)
 
 !       Number of edge points between the nodes of each segment
 do 14 i=1,num_segments
    read(11,*) num_edge_points_new(i)
14 continue
    
    read(11,*)
    read(11,*)
    
    !       Edge point coordinates
    do 15 i=1,num_segments
       do 54 j=1,num_edge_points_new(i)
          read(11,*)
54     continue
15  continue
          read(11,*)
          read(11,*)
          
          radius_remove_tag=.true.
          !       Remove segments that are entirely composed of the minimum radius	
          do 71 i=1,num_segments
             do 64 j=1,num_edge_points_new(i)
                read(11,*) point_thick
                if(point_thick.gt.(voxel_size/2.d0)) then
                   radius_remove_tag=.false.
                end if
64              continue
                if(radius_remove_tag.eqv..true.) then
                   edge_connectivity1(i)=-1
                   edge_connectivity2(i)=-1
                end if
71              continue
                
                DEALLOCATE(num_edge_points_new)
                close(11)
                open(unit=11,file='Input.am',status='old')
                
                !       Read intro part of file again
                
                do i=1,19
                   read(11,*)
                end do
                
                
                !       Coordinates of vertices
                do 12 i=1,num_nodes
                   read(11,*,iostat=k) 
12                 continue
                   
                   read(11,*)
                   read(11,*)
                   
                   !       Connections between vertices
                   do 13 i=1,num_segments
                      read(11,*,iostat=k) 
                      IF(k>0) exit
13                    continue
                      
                      read(11,*)
                      read(11,*)
                      
                      
                      
                      !       Determine top_z and bottom_z
                      top_z=z(1)
	bottom_z=z(1)
	do 17 k=2,num_nodes
	   if(z(k).ge.top_z) then
	      top_z=z(k)
	   elseif(z(k).le.bottom_z) then
	      bottom_z=z(k)
	   end if
 17	continue
	top_z=top_z-end_tol
	bottom_z=bottom_z+end_tol

 if(bottom_z.gt.top_z) then
    PRINT *,'Error: End tolerance is too high. Bottom z is greater than top z. Please specify a smaller end tolerance.'
    pause
    end if

!       Remove nodes that are in the same position
	do 60 i=1,num_nodes-1
	   do 61 j=i+1,num_nodes
	      if((x(i).eq.x(j)).and.(y(i).eq.y(j)).and.(z(i).eq.z(j))) then
		 do 62 k=1,num_segments
		    if(edge_connectivity1(k).eq.j) then
		       edge_connectivity1(k)=i
		    end if
		    if(edge_connectivity2(k).eq.j) then
		       edge_connectivity2(k)=i
		    end if
 62		 continue	
	      end if
 61	      continue
 60	   continue



!       Remove segments that do not advance in z
	   do j=1,num_segments
	      if(z(edge_connectivity2(j)).le.z(edge_connectivity1(j)))then
		 edge_connectivity1(j)=-1
		 edge_connectivity2(j)=-1
	      end if
	   end do


!       Remove segments that are connected to the same node
	do 57 i=1,num_segments
	   if(edge_connectivity1(i).eq.edge_connectivity2(i)) then
		 edge_connectivity1(i)=-1
		 edge_connectivity2(i)=-1
	   end if
 57	   continue



!       Remove duplicate segments
	do 50 i=1,num_segments-1
	   do 51 j=i+1,num_segments
	   if((edge_connectivity1(i).eq.edge_connectivity1(j)) .and. (edge_connectivity2(i).eq.edge_connectivity2(j))) then
		 edge_connectivity1(j)=-1
		 edge_connectivity2(j)=-1
	   end if
 51	   continue
 50	continue



!       Store the current network
	call avizo_store

	END SUBROUTINE remove_segs



	SUBROUTINE RENUMBER
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j,k,l,m,n,p,q,r,s,t,u,v,w,node
	DOUBLE PRECISION :: dist_sq,distnew_sq,d
	INTEGER, ALLOCATABLE :: seg(:), node_store(:)
	DOUBLE PRECISION, ALLOCATABLE :: xnew(:),ynew(:),znew(:)
	DOUBLE PRECISION, ALLOCATABLE :: xnewtemp(:),ynewtemp(:)
	DOUBLE PRECISION, ALLOCATABLE :: znewtemp(:)
	INTEGER :: nodetemp,num_steps	
	DOUBLE PRECISION, ALLOCATABLE :: radius_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordx_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordy_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordz_new(:,:)
	INTEGER, ALLOCATABLE :: edge1new(:), edge2new(:),num_edge_points_new(:)
	LOGICAL, ALLOCATABLE :: node_tag(:)
        INTEGER, ALLOCATABLE :: segment_number_list(:)
	INTEGER, ALLOCATABLE :: segment_list(:),num_points_new(:)
	INTEGER, ALLOCATABLE :: segment_list_new(:)
	INTEGER, ALLOCATABLE :: segment_store(:,:)
	DOUBLE PRECISION, ALLOCATABLE ::zlist(:)
        INTEGER :: correct_seg, node_counter, bottom_fail
	LOGICAL :: top_node, firsttheta,deadendtag
	DOUBLE PRECISION :: length,pitangle,currentz,coordz_initial
	INTEGER :: current_bottom_node,rej_seg,rej_counter,init
	DOUBLE PRECISION :: point_thick,blank1,blank2,blank3
	character (len=20):: vertex
	character (len=18):: edge
	character (len=6) :: vertex2,edge2

	open(unit=11,file='Input.am',status='old')

	open(unit=19,file='bottom_nodes.txt',status='unknown')
!       So, the idea is that you build up from the bottom simultaneously. Thus, each group of segments is built from scratch.
	
	number_bott_nodes=0
	
	do 59 i=1,num_nodes
	   if (z(i).le.bottom_z) then
	      write(19,*) i
	      number_bott_nodes=number_bott_nodes+1
	   end if
 59	continue
	close(19)

	ALLOCATE(xnew(2*number_bott_nodes))
	ALLOCATE(ynew(2*number_bott_nodes))
	ALLOCATE(znew(2*number_bott_nodes))
	ALLOCATE(edge1new(number_bott_nodes))
	ALLOCATE(edge2new(number_bott_nodes))
	ALLOCATE(num_edge_points_new(number_bott_nodes))
	ALLOCATE(coordx_new(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(coordy_new(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(coordz_new(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(radius_new(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(xnewtemp(num_segments))
	ALLOCATE(ynewtemp(num_segments))
	ALLOCATE(znewtemp(num_segments))
	ALLOCATE(seg(num_segments))
	ALLOCATE(node_tag(num_nodes))
	ALLOCATE(node_used(2*num_nodes))
	ALLOCATE(node_store(number_bott_nodes))
	ALLOCATE(segment_number_list(number_bott_nodes))
	ALLOCATE(segment_list(number_bott_nodes))
	ALLOCATE(segment_list_new(number_bott_nodes))
	ALLOCATE(zlist(number_bott_nodes))
	ALLOCATE(segment_store(number_bott_nodes,max_num_points_per_seg))

	firsttheta=.false.
	segment_store=0


	open(unit=22,file='bottom_nodes.txt',status='unknown')

!       find segment that bottom node is a part of

	m=1			! new node label
	n=1			! new segment label
	segment_list=0
	segment_list_new=0
	segment_number_list=0
	edge2new=0
!       renumber nodes and segments


!       Keep a list of all used nodes so that you don't reuse them
	do i=1,num_nodes
	   node_used(i)=.false.
	end do
!       set everything to zbound2 so that 'bottom nodes' that don't actually go anywhere don't mess up everything else
	zlist=zbound2
	do i=1,number_bott_nodes
	   read(22,*) node_store(i)
	   xnew(m)=x(node_store(i))
	   ynew(m)=y(node_store(i))
	   znew(m)=z(node_store(i))
	   m=m+1
	   edge1new(i)=i
	   do j=1,num_segments
	      if(node_store(i).eq.edge_connectivity1(j)) then
!	Define z coordinates of all bottom nodes
		 zlist(i)=z(node_store(i))
		 exit
	      end if
!       otherwise, zlist stays at max z level		
	   end do
	end do

	top_node=.false.

!       Code_long_term

!       Set to a high number to ensure that you reach the end of all of the segments
	do 62 k=1,100000
	      q=1
!       Find node with the lowest z coordinate
	      if(minval(zlist).lt.zbound2) then
		 do i=1,number_bott_nodes
		    if(zlist(i).eq.minval(zlist)) then
		       if(segment_list(i).eq.0) then
			  node=node_store(i)
		       else
			  node=edge_connectivity2(segment_list(i))
		       end if
		       if(node_used(node).eqv..false.) then
			  node_used(node)=.true.
			  current_bottom_node=i
			  EXIT
		       else
			  !still acceptable if it is an internal node
			  if(segment_list(i).eq.0) then
			     zlist(i)=zbound2
			     GOTO 62
			  else
			     current_bottom_node=i
			     EXIT
			  end if
		       end if
		    end if
		 end do
	      else
		 GOTO 63
	      end if


!       Check to see if segment contains a top node
	      if((z(node).ge.top_z) .and.(edge_connectivity1(segment_list(current_bottom_node)).ne.-1)) THEN
		 top_node=.true.
!       Specify the node that you accept as used
		 node_used(node)=.true.
	 
!       Set edge_connectivities to -1 to ensure that the segment is not reused
		 edge_connectivity1(segment_list(current_bottom_node))=-1
		 edge_connectivity2(segment_list(current_bottom_node))=-1
		
		 zlist(current_bottom_node)=zbound2
		 xnew(m)=x(node)
		 ynew(m)=y(node)
		 znew(m)=z(node)
   edge2new(current_bottom_node)=m
		  node_used(node)=.true.
		
		 m=m+1
		 n=n+1

		 GOTO 62
	      end if

	      deadendtag=.false.
	      do 23 j=1,num_segments
		 

		 if((edge_connectivity2(j).ne.-1) .and.((node_used(edge_connectivity2(j))).neqv..true.).and.(node.eq.edge_connectivity1(j)))then
!     &          .and.(z(edge_connectivity2(j)).ge.z(edge_
!     &           connectivity1(j)))) then
		


		    nodetemp=edge_connectivity2(j)
		    xnewtemp(q)=x(nodetemp)
		    ynewtemp(q)=y(nodetemp)
		    znewtemp(q)=z(nodetemp)
		    seg(q)=j
		    q=q+1

!       Check to see if segment contains a top node
		    if((z(edge_connectivity2(j)).ge.top_z) .and. (edge_connectivity1(j).ne.-1)) THEN
		       top_node=.true.
		       GOTO 23
		    end if


!       If not a top node, check to ensure that connection is not a dead end
		    do 24 r=1,num_segments
		       if(edge_connectivity2(j).eq.edge_connectivity1(r)) then !not a dead end	
			  GOTO 23
		       else
			  deadendtag=.true.
		       end if
 24		    continue
		    q=q-1

	      end if

!       If it is the end of a segment, finish off the calculation
	      if((j.eq.num_segments).and.(q.eq.1).and.(deadendtag.eqv..true.)) then
		 q=2
	      end if

 23	      continue

!       m-1 (i.e. previous segment)
	      correct_seg=seg(1)

! Think about other ways to get this to work!!
!Maybe you could try to find some distance that is reasonably long to go over to check for angles??
!It could be adaptive based on the amount of change that you see
!or you could specify the distance based on variability? thus, as variability drops, then you must be nearing the right answer??
!Would be useful for node calculations

!	      bottom_fail=0
!		 pitangle=0
!		 do r=1,number_bott_nodes
!		    if(segment_list(r).eq.0) Exit
!		    if(node_tag(r).eqv..true.) then
!!problem is that edge 1 is set at negative 1
!		 pitangle=ATAN(dsqrt(((xnew(node_listedge1(r)))
!     &        -xnew(node_listedge2(r)))**2+
!     &		 (ynew(node_listedge1(r))
!     &        -ynew(node_listedge2(r)))**2)/(znew(node_listedge2(r))
!     &        -znew(node_listedge1(r))))+pitangle
!		 PRINT *,r,ATAN(dsqrt(((xnew(node_listedge1(r)))
!     &        -xnew(node_listedge2(r)))**2+
!     &		 (ynew(node_listedge1(r))
!     &        -ynew(node_listedge2(r)))**2)/(znew(node_listedge2(r))
!     &        -znew(node_listedge1(r)))),xnew(node_listedge1(r)),
!     &         ynew(node_listedge1(r)),znew(node_listedge1(r)),
!     &          xnew(node_listedge2(r)),
!     &         ynew(node_listedge2(r)),znew(node_listedge2(r))
!		    else
!		       bottom_fail=bottom_fail+1
!		 end if
!		 if(r.eq.number_bott_nodes) then
!		    if(firsttheta.eqv..false.) then
!		       PRINT *,'Initial theta:', thetaguess
!		    end if
!		    thetaguess=pitangle/(number_bott_nodes-bottom_fail)
!		    pause
!		    if(firsttheta.eqv..false.) then
!		       firsttheta=.true.
!		       PRINT *,'Actual theta:', thetaguess
!		       pause
!		    end if
!		 end if
!		 end do

!       bottom segment
	      if(segment_list(current_bottom_node).eq.0) then
		 segment_list(current_bottom_node)=-2
		 segment_list_new(current_bottom_node)=-2
		 edge_connectivity2(segment_list(current_bottom_node))=node_store(current_bottom_node)
	      end if

	      distnew_sq=(xnewtemp(1)-x(edge_connectivity2(segment_list(current_bottom_node))))**2.d0+&
	      &(ynewtemp(1)-y(edge_connectivity2(segment_list(current_bottom_node))))**2.d0
	      d=((znewtemp(1)-z(edge_connectivity2(segment_list(current_bottom_node))))*TAN(thetaguess))**2.d0
	      distnew_sq=distnew_sq-d
	      q=q-1

	      if(q.gt.0) then

		 do 61 s=1,q
		    dist_sq=(xnewtemp(s)-x(edge_connectivity2(segment_list(current_bottom_node))))**2.d0+&
		    &(ynewtemp(s)-y(edge_connectivity2(segment_list(current_bottom_node))))**2.d0
		d=((znewtemp(s)-z(edge_connectivity2(segment_list(current_bottom_node))))*TAN(thetaguess))**2.d0

		    dist_sq=ABS(dist_sq-d)

		    if(dist_sq.lt.distnew_sq) then
		       correct_seg=seg(s)
		       distnew_sq=dist_sq
		    end if
 61		 continue

		 length=xnewtemp(s)-x(edge_connectivity2(segment_list(current_bottom_node)))+&
		 &ynewtemp(s)-y(edge_connectivity2(segment_list(current_bottom_node)))&
		 &+znewtemp(s)-z(edge_connectivity2(segment_list(current_bottom_node)))

		 if(edge_connectivity1(correct_seg).ne.-1) then
		    node=edge_connectivity2(correct_seg)

!		
!       Specify the node that you accept as used
		    node_used(node)=.true.
		    
!       get rid of coordinates if node is already taken?
		    do i=1,number_bott_nodes
		       if(node_store(i).eq.node) then !node is shared
!       remove the repeated node from contention
			  zlist(i)=zbound2
			  if(segment_list(current_bottom_node).eq.-2) then
			     edge1new(current_bottom_node)=current_bottom_node
			  end if
			  exit
		       end if
		    end do

!       Set edge_connectivities to -1 to ensure that the segment is not reused
		    edge_connectivity1(correct_seg)=-1	

!       Should be bottom node for i!!
		    segment_number_list(current_bottom_node)=segment_number_list(current_bottom_node)+1

		    segment_store(current_bottom_node,segment_number_list(current_bottom_node))=correct_seg

		    edge_connectivity2(segment_list(current_bottom_node))=-1
		    segment_list(current_bottom_node)=correct_seg
		    segment_list_new(current_bottom_node)=n
		    zlist(current_bottom_node)=z(edge_connectivity2(correct_seg))

		    if((zlist(current_bottom_node).ge.top_z).and.(top_node.eqv..true.)) then
		       xnew(m)=x(node)
		       ynew(m)=y(node)
		       znew(m)=z(node)
		       edge2new(current_bottom_node)=m
		       m=m+1
		       n=n+1
		       node_used(node)=.true.
		       top_node=.false.
		       zlist(current_bottom_node)=zbound2
		       GOTO 62
		    end if
		 else		! node is already used
!       bottom node fails
!       end the segment
		    edge2new(current_bottom_node)=m
		    m=m+1
		    n=n+1
!       loop through and make all nodes false if they don't reach the top boundary
!		    rej_seg=segment_list_new(current_bottom_node)
!		    if(rej_seg.eq.-2) then
!		       node_tag(current_bottom_node)=.false.
!		    else
!		       node_tag(edge2new(rej_seg))=.false.
!		       edge2new(rej_seg)=-1
!		
!		       do 28 u=1,segment_number_list(current_bottom_node)
!			  do 29 w=1,n
!			     if(edge2new(w).eq.edge1new(rej_seg)) then
!				node_tag(edge1new(rej_seg))=.false.
!				edge1new(rej_seg)=-1
!				edge2new(w)=-1
!				rej_seg=w
!				GOTO 28
!			     end if
! 29			  continue
! 28		       continue
!!       Last node
!		       edge1new(rej_seg)=-1
!		    end if
!       Add so that code doesn't continually look for next segment from the same node
		    zlist(current_bottom_node)=zbound2
		    GOTO 62
		 end if
	      else
!       bottom node fails
!       end segment
		 if(segment_number_list(current_bottom_node).gt.0) then
		    xnew(m)=x(node)
		    ynew(m)=y(node)
		    znew(m)=z(node)
		    edge2new(current_bottom_node)=m
		    m=m+1
		    n=n+1
		 node_used(node)=.true.
		 end if
!!       loop through and make all nodes false if they don't reach the top
!		 rej_seg=segment_list_new(current_bottom_node)
!		 if(rej_seg.eq.-2) then
!		    node_tag(current_bottom_node)=.false.
!		 else
!		    node_tag(edge2new(rej_seg))=.false.
!		    edge2new(rej_seg)=-1
!
!
!		    do 37 u=1,segment_number_list(current_bottom_node)
!		       do 35 w=1,n
!			  if(edge2new(w).eq.edge1new(rej_seg)) then
!!       currently some of the segments are zero
!			     node_tag(edge1new(rej_seg))=.false.
!			     edge1new(rej_seg)=-1
!			     edge2new(w)=-1
!			     rej_seg=w
!			     GOTO 37
!			  end if
! 35		       continue
! 37		    continue
!
!!       Last node
!		    node_tag(current_bottom_node)=.false.
!		    edge1new(rej_seg)=-1
!		 end if
!       Add so that the code doesn't continually look for next segment from the same node
		 zlist(current_bottom_node)=zbound2
		 GOTO 62
	      end if
 62	   continue
 63	   continue

	num_nodes=m-1
	num_segments=n-1
	edge_connectivity1=-1
	edge_connectivity2=-1
	node_tag=.true.



	p=1
!       Remove segments that are not connected
	do i=1,number_bott_nodes
	   if(edge2new(i).ne.0)then
	      edge1new(p)=edge1new(i)
	      edge2new(p)=edge2new(i)
	      do k=1,max_num_points_per_seg
		 segment_store(p,k)=segment_store(i,k)
	      end do
	      p=p+1
	   end if
	end do
	number_bott_nodes=p-1
	

!       Remove nodes that are in the same position
	do 34 i=1,num_nodes-1
	   do 68 j=i+1,num_nodes
	      if((xnew(i).eq.xnew(j)).and.(ynew(i).eq.ynew(j)).and.(znew(i).eq.znew(j))) then
		 do 69 k=1,num_segments
		    if(edge1new(k).eq.j) then
		       edge1new(k)=i
		    end if
		    if(edge2new(k).eq.j) then
		       edge2new(k)=i
		    end if
 69		 continue
		 node_tag(j)=.false.
	      end if
 68	   continue
 34	continue

!       Remove nodes that have no connections
	do 88 j=1,num_nodes
	   do i=1,num_segments
	      if((edge1new(i).eq.j) .or.(edge2new(i).eq.j)) then
		 GOTO 88
	      end if
!       no connection found
	      if(i.eq.num_segments) then
		 node_tag(j)=.false.
	      end if
	   end do
 88	   continue



!       Remove extra nodes
	p=1
	do 71 i=1,num_nodes
	   if(node_tag(i).eqv..true.) then
	   x(p)=xnew(i)
	   y(p)=ynew(i)
	   z(p)=znew(i)

	   do 72 j=1,num_segments
	      if(edge1new(j).eq.i) then
		 edge_connectivity1(j)=p
	      end if
	      if(edge2new(j).eq.i) then
		 edge_connectivity2(j)=p
	      end if	
 72	   continue
	   p=p+1
	   end if
 71	   continue
	
	   num_nodes=p-1
	   node_tag=.true.

!       Call the stored network info
	open(unit=31,file='avizo_store.am',status='old')
!       Read intro part of file

	do i=1,3
	   read(31,*)
	end do
	read(31,"(A20)") vertex
	read(31,"(A18)") edge

	do i=1,14
	   read(31,*)
	end do

	vertex2=vertex(15:20)
	edge2=edge(13:18)

	read(vertex2,*) num_nodes_old
	read(edge2,*) num_segments_old
	close(31)

!       Don't read the coordinates (already read them in the intro subroutine)
	do i=1,21
	   read(11,*)
	end do
	do 12 i=1,num_nodes_old
	   read(11,*,iostat=k) blank1,blank2,blank3
 12	continue
	
	read(11,*)
	read(11,*)

	do 13 i=1,num_segments_old
	   read(11,*,iostat=k) blank1,blank2
	   IF(k>0) exit
 13	continue

!       Number of edge points between the nodes of each segment
	do 14 i=1,num_segments_old
	   read(11,*) num_edge_points(i)
 14	continue

	read(11,*)
	read(11,*)

!       There are some extraneous segments that avizo adds, but these are removed
!       in the remove_nodes subroutine before the segments are extended to the boundaries
	ALLOCATE(coordx_in(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(coordy_in(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(coordz_in(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(radius_in(number_bott_nodes,max_num_points_per_seg))
	ALLOCATE(num_points_new(number_bott_nodes))

	coordx_in=0
	coordy_in=0
	coordz_in=0
	radius_in=0

	num_points_new=0

	r=number_bott_nodes

!       Edge point coordinates
	do 15 i=1,num_segments_old
          do j=1,r
	      do k=1,max_num_points_per_seg
		 if(segment_store(j,k).eq.i) then
		    if((num_edge_points(i)+num_points_new(j)).gt.(INT((zbound2 -zbound1)/voxel_size+extra_points))) then
		       PRINT *,'Error: Not enough extra_points specified. Modify the input file to include more extra points. Minimum of', &
		       &(num_edge_points(i)+num_points_new(j)-INT((zbound2-zbound1)/voxel_size)), &
		       &'extra points required. Ignoring this segment and all connected segments that follow.' 
		       do l=1,num_edge_points(i)
			  read (11,*) blank1,blank2,blank3
		       end do


		       do l=k,max_num_points_per_seg
!       eliminate any additional segments coming off of this bottom node
			  segment_store(j,l)=0
		       end do
!		       number_bott_nodes=number_bott_nodes-1

		       GOTO 15
		    else
		       do 51 l=1,num_edge_points(i)
			  read(11,*) coordx_in(j,l+num_points_new(j)),coordy_in(j,l+num_points_new(j)),coordz_in(j,l+num_points_new(j))
!       convert to meters
			  coordx_in(j,l+num_points_new(j))=coordx_in(j,l+num_points_new(j))*convert_factor
			  coordy_in(j,l+num_points_new(j))=coordy_in(j,l+num_points_new(j))*convert_factor
			  coordz_in(j,l+num_points_new(j))=coordz_in(j,l+num_points_new(j))*convert_factor
 51		       continue
		       num_points_new(j)=num_points_new(j)+num_edge_points(i)
		       GOTO 15
		    end if 
		 end if
	      end do
	   end do
!       did not find any matching segs
	   do l=1,num_edge_points(i)
	      read(11,*) blank1,blank2,blank3 
	   end do
 15	continue
	read(11,*)
	read(11,*)

	do i=1,num_segments
	   write(37,*) edge_connectivity1(i),edge_connectivity2(i)
	end do

	num_points_new=0
!       Thickness (i.e. radius) of vessel at each edge point	
	do 73 i=1,num_segments_old
	   do j=1,r
	      do k=1,max_num_points_per_seg
		 if(segment_store(j,k).eq.i) then
		    do l=1,num_edge_points(i)
		       read(11,*) point_thick
		       
!       convert units to meters
		       radius_in(j,l+num_points_new(j))=point_thick*convert_factor
		    end do
		    num_points_new(j)=num_points_new(j)+num_edge_points(i)
		    GOTO 73
		 end if
	      end do
	   end do

	      

!       did not find any matching segs
	   do l=1,num_edge_points(i)
	      read(11,*) blank1
	   end do
	   
 73	continue

!       Rewrite coordinates and radius in case any of the segments that were too long

	   p=1
	   do j=1,num_nodes
	      if(node_tag(j).eqv..true.) then
		 x(p)=x(j)
		 y(p)=y(j)
		 z(p)=z(j)
		 do 70 k=1,num_nodes
		    if(edge_connectivity1(k).eq.j) then
		       edge_connectivity1(k)=p
		    end if
		    if(edge_connectivity2(k).eq.j) then
		       edge_connectivity2(k)=p
		    end if	
 70		 continue
		 p=p+1
	      end if
	   end do
	   
           q=1
	   do 33 j=1,num_segments
	      if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
		 edge_connectivity1(q)=edge_connectivity1(j)
		 edge_connectivity2(q)=edge_connectivity2(j)
		 num_points_new(q)=num_points_new(j)
		 do m=1,num_points_new(j)
		      coordx_in(q,m)=coordx_in(j,m)
		      coordy_in(q,m)=coordy_in(j,m)
		      coordz_in(q,m)=coordz_in(j,m)
		      radius_in(q,m)=radius_in(j,m)
		   end do
		 q=q+1
	      end if
 33	   continue

	   num_nodes=p-1
	   num_segments=q-1
	   num_edge_points=0

	   p=1
	   do j=1,num_segments
	      if(num_points_new(j).ne.0) then
		 num_edge_points(p)=num_points_new(j)
		 p=p+1
	      end if
	   end do

	   do k=1,num_segments
!       new point counter
	      u=2
              currentz=coordz_in(k,1)
              coordz_initial=coordz_in(k,1)
	      coordz_new(k,1)=coordz_in(k,1)
	      coordx_new(k,1)=coordx_in(k,1)
	      coordy_new(k,1)=coordy_in(k,1)
	      radius_new(k,1)=radius_in(k,1)
	      num_edge_points_new(k)=num_edge_points(k)
	      do n=1,num_edge_points(k)-1
		 if(NINT((coordz_in(k,n+1)-currentz)/voxel_size).ge.1) then
!       Skipped a couple of points, so add them back in
		    if(NINT((coordz_in(k,n+1)-currentz)/voxel_size).ge.2) then
		       num_steps=NINT((coordz_in(k,n+1)-currentz)/voxel_size)
		       do v=1,num_steps-1
			  coordz_new(k,u)=voxel_size*(u-1)+coordz_initial
			  coordx_new(k,u)=coordx_in(k,n+1)
			  coordy_new(k,u)=coordy_in(k,n+1)
			  radius_new(k,u)=radius_in(k,n+1)
			  num_edge_points_new(k)=num_edge_points_new(k)+1
			  u=u+1
		       end do
		       coordz_new(k,u)=voxel_size*(u-1)+coordz_initial
		    else
		       coordz_new(k,u)=voxel_size*(u-1)+coordz_initial
		    end if
		    coordx_new(k,u)=coordx_in(k,n+1)
		    coordy_new(k,u)=coordy_in(k,n+1)
		    currentz=coordz_new(k,u)
		    radius_new(k,u)=radius_in(k,n+1)
		    u=u+1
		 else
		    num_edge_points_new(k)=num_edge_points_new(k)-1
		 end if

	      end do
	      num_edge_points(k)=num_edge_points_new(k)
	   end do

	
	q=1
	do 25 j=1,num_segments
	   if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
	      num_edge_points(q)=num_edge_points_new(j)
	      do m=1,num_edge_points_new(j)
		 coordx_in(q,m)=coordx_new(j,m)
		 coordy_in(q,m)=coordy_new(j,m)
		 coordz_in(q,m)=coordz_new(j,m)
		 radius_in(q,m)=radius_new(j,m)
	      end do
	      q=q+1
	   end if
 25	continue
	
	num_segments=q-1	
!       Change the node coordinates to correspond with final points

	do j=1,num_segments
	   x(edge_connectivity2(j))=coordx_in(j,num_edge_points(j))
	   y(edge_connectivity2(j))=coordy_in(j,num_edge_points(j))
	   z(edge_connectivity2(j))=coordz_in(j,num_edge_points(j))
	end do

	DEALLOCATE(coordx_new)
	DEALLOCATE(coordy_new)
	DEALLOCATE(coordz_new)
	DEALLOCATE(radius_new)
	DEALLOCATE(node_tag)
	DEALLOCATE(node_store)
	DEALLOCATE(segment_number_list)
	DEALLOCATE(segment_list)
	DEALLOCATE(segment_list_new)
        DEALLOCATE(zlist)
	DEALLOCATE(edge1new)
	DEALLOCATE(edge2new)
	DEALLOCATE(num_edge_points_new)
	DEALLOCATE(xnewtemp)
	DEALLOCATE(ynewtemp)
	DEALLOCATE(znewtemp)
	DEALLOCATE(seg)
	DEALLOCATE(xnew)
	DEALLOCATE(ynew)
	DEALLOCATE(znew)

	num_backbone_nodes=num_nodes
	num_backbone_segs=num_segments
	close(22)
	close(11)
!	call avizo_test
	PRINT *,'renumber'

	END SUBROUTINE renumber

	SUBROUTINE bridges
	USE gcmmn
	IMPLICIT NONE
	DOUBLE PRECISION, ALLOCATABLE :: xold(:),yold(:),zold(:)
	LOGICAL, ALLOCATABLE :: node_tag(:)
	LOGICAL, ALLOCATABLE :: seg_tag(:)
	INTEGER, ALLOCATABLE :: edge1old(:), edge2old(:)
	INTEGER, ALLOCATABLE :: edge1new(:), edge2new(:)
	character (len=20):: vertex
	character (len=18):: edge
	character (len=6) :: vertex2,edge2
	INTEGER :: i,j,k,p,q,m,r
	DOUBLE PRECISION :: point_thick
	LOGICAL, ALLOCATABLE :: seg_add1(:),seg_add2(:)

!       Call the stored network info
	open(unit=11,file='avizo_store.am',status='old')
!       Read intro part of file

	do i=1,3
	   read(11,*)
	end do
	read(11,"(A20)") vertex
	read(11,"(A18)") edge

	do i=1,14
	   read(11,*)
	end do

	vertex2=vertex(15:20)
	edge2=edge(13:18)

	read(vertex2,*) num_nodes_old
	read(edge2,*) num_segments_old
	ALLOCATE(xold(num_nodes_old))
	ALLOCATE(yold(num_nodes_old))
	ALLOCATE(zold(num_nodes_old))

! Coordinates of vertices
	do 12 i=1,num_nodes_old
	   read(11,*,iostat=k) x(i+num_backbone_nodes), y(i+num_backbone_nodes),z(i+num_backbone_nodes)
!       convert units to meters
	   x(i+num_backbone_nodes)=x(i+num_backbone_nodes)*convert_factor
	   y(i+num_backbone_nodes)=y(i+num_backbone_nodes)*convert_factor
	   z(i+num_backbone_nodes)=z(i+num_backbone_nodes)*convert_factor
 12	continue
	
	read(11,*)
	read(11,*)

	ALLOCATE(edge1old(num_segments_old))
	ALLOCATE(edge2old(num_segments_old))
	ALLOCATE(edge1new(num_segments_old))
	ALLOCATE(edge2new(num_segments_old))
!       Connections between vertices
	do 13 i=1,num_segments_old
	   read(11,*,iostat=k) edge_connectivity1(i+num_backbone_segs),edge_connectivity2(i+num_backbone_segs)
	   IF(k>0) exit
!       Add one because the export format starts with index 0, add backbone nodes because they are already accounted for
	   edge_connectivity1(i+num_backbone_segs)=edge_connectivity1(i+num_backbone_segs)+1+num_backbone_nodes
	   edge_connectivity2(i+num_backbone_segs)=edge_connectivity2(i+num_backbone_segs)+1+num_backbone_nodes
 13	continue

	num_nodes=num_nodes_old+num_backbone_nodes
        num_segments=num_segments_old+num_backbone_segs

	read(11,*)
	read(11,*)

!       There are some extraneous segments that avizo adds, but these are removed
!       in the remove_nodes subroutine before the segments are extended to the boundaries


	ALLOCATE(seg_add1(num_segments_old))
	ALLOCATE(seg_add2(num_segments_old))


	ALLOCATE(node_tag(num_nodes_old+num_backbone_nodes))
	ALLOCATE(seg_tag(num_segments_old+num_backbone_segs))
	node_tag=.true.
	seg_tag=.true.

!       Eliminate any nodes that are already used
	do i=1,num_nodes_old
	   if(node_used(i).eqv..true.) then
	      node_tag(i+num_backbone_nodes)=.false.
	   end if
	end do

!       Reset node_used
	node_used=.false.
	do i=1,num_nodes
	   if(node_tag(i).eqv..false.) then
	   node_used(i)=.true.
	   end if
	end do

!       Eliminate segments that are connected to already used nodes
	do i=num_backbone_segs+1,num_segments
	   if((node_tag(edge_connectivity1(i)).eqv..false.).or.(node_tag(edge_connectivity2(i)).eqv..false.)) then
	      seg_tag(i)=.false.
	      edge_connectivity1(i)=-1
	      edge_connectivity2(i)=-1
	   end if
	end do

!       Reset node tag
	do i=1,num_nodes_old
	   node_tag(i)=.true.
	end do

!       Remove nodes that have no connections
	do 88 j=num_backbone_nodes+1,num_nodes
	   do i=num_backbone_segs+1,num_segments
	      if((edge_connectivity1(i).eq.j) .or.(edge_connectivity2(i).eq.j)) GOTO 88
	      !no segment found
	      if(i.eq.num_segments) then
		 node_tag(j)=.false.
	      end if
	   end do
 88	continue
	
!       Remove nodes that are in the same position
	do 60 i=1,num_nodes-1
	   do 61 j=i+1,num_nodes
	      if((x(i).eq.x(j)).and.(y(i).eq.y(j)).and.(z(i).eq.z(j))) then
		 do 62 k=1,num_segments
		    if(edge_connectivity1(k).eq.j) then
		       edge_connectivity1(k)=i
		    end if
		    if(edge_connectivity2(k).eq.j) then
		       edge_connectivity2(k)=i
		    end if
 62		 continue	
	      end if
 61	      continue
 60	   continue
	  
!       Remove segments that do not advance in z
	   do j=1,num_segments
	      if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
		 if((z(edge_connectivity2(j)).le.z(edge_connectivity1(j)))) then
		    edge_connectivity1(j)=-1
		    edge_connectivity2(j)=-1
		 end if
	      end if
	   end do

!       Remove segments that are connected to the same node
	do 57 i=1,num_segments
	   if(edge_connectivity1(i).eq.edge_connectivity2(i)) then
		 edge_connectivity1(i)=-1
		 edge_connectivity2(i)=-1
	   end if
 57	   continue

!       Remove duplicate segments
	do 50 i=1,num_segments-1
	   do 51 j=i+1,num_segments
	   if((edge_connectivity1(i).eq.edge_connectivity1(j)) .and.(edge_connectivity2(i).eq.edge_connectivity2(j)) ) then
		 edge_connectivity1(j)=-1
		 edge_connectivity2(j)=-1
	   end if
 51	   continue
 50	continue

	PRINT *,'bridges'
!	pause
	DEALLOCATE(xold)
	DEALLOCATE(yold)
	DEALLOCATE(zold)
	DEALLOCATE(edge1old)
	DEALLOCATE(edge2old)
	DEALLOCATE(edge1new)
	DEALLOCATE(edge2new)
	DEALLOCATE(node_tag)
	DEALLOCATE(seg_add1)
	DEALLOCATE(seg_add2)
	END SUBROUTINE bridges





	SUBROUTINE renumber_bridges
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j,k,l,m,n,p,q,r,s,t,u,v,w,node
	DOUBLE PRECISION :: dist_sq,distnew_sq,d,point_thick
	INTEGER, ALLOCATABLE :: seg(:), node_store(:)
	DOUBLE PRECISION, ALLOCATABLE :: xnew(:),ynew(:),znew(:)
	DOUBLE PRECISION, ALLOCATABLE :: xnewtemp(:),ynewtemp(:)
	DOUBLE PRECISION, ALLOCATABLE :: znewtemp(:)
	DOUBLE PRECISION, ALLOCATABLE :: radius_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordx_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordy_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordz_new(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: radius_new2(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordx_new2(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordy_new2(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: coordz_new2(:,:)
	INTEGER, ALLOCATABLE :: edge1new(:), edge2new(:),num_edge_points_new(:), num_edge_points_new2(:)
	LOGICAL, ALLOCATABLE :: node_tag(:)
        INTEGER, ALLOCATABLE :: segment_number_list(:)
	INTEGER, ALLOCATABLE :: segment_list(:),num_points_new(:)
	INTEGER, ALLOCATABLE :: node_listedge1(:), node_listedge2(:)
	INTEGER, ALLOCATABLE :: segment_list_new(:),num_edge_points_old(:)
	INTEGER, ALLOCATABLE :: segment_store(:,:)
	DOUBLE PRECISION, ALLOCATABLE :: zlist(:)
	INTEGER :: nodetemp,num_steps
        INTEGER :: correct_seg, node_counter, bottom_fail
	LOGICAL :: top_node, firsttheta,deadendtag,started
	DOUBLE PRECISION :: length,pitangle,blank1,blank2,blank3
	INTEGER :: current_begin_node,rej_seg,rej_counter
	DOUBLE PRECISION :: currentz,coordz_initial
	character (len=20):: vertex
	character (len=18):: edge
	character (len=6) :: vertex2,edge2


	ALLOCATE(xnew(num_nodes_old))
	ALLOCATE(ynew(num_nodes_old))
	ALLOCATE(znew(num_nodes_old))
	ALLOCATE(edge1new(num_segments_old))
	ALLOCATE(edge2new(num_segments_old))
	ALLOCATE(num_edge_points_new(num_segments_old))
	ALLOCATE(num_edge_points_new2(num_segments_old))
	ALLOCATE(num_edge_points_old(num_segments_old))
	ALLOCATE(xnewtemp(num_segments_old))
	ALLOCATE(ynewtemp(num_segments_old))
	ALLOCATE(znewtemp(num_segments_old))
	ALLOCATE(seg(num_segments_old))
	ALLOCATE(node_tag(num_nodes_old))


	firsttheta=.false.

!       So, the idea is that you build up from the bottom simultaneously. Thus, each group of segments is built from scratch.
	
!       First, find all possible starting nodes
	number_begin_nodes=0

	open(unit=19,file='begin_nodes.txt',status='unknown')
	
	do 59 i=num_backbone_nodes+1,num_nodes
	   started=.false.
	   do j=num_backbone_segs+1,num_segments
	      if(edge_connectivity2(j).eq.i) EXIT
	      if(edge_connectivity1(j).eq.i) started=.true.
	      if((j.eq.num_segments).and.(started.eqv..true.)) then
		 write(19,*) i
		 number_begin_nodes=number_begin_nodes+1
	      end if
	   end do
 59	continue


	close(19)
	
	ALLOCATE(node_store(number_begin_nodes))
	ALLOCATE(segment_number_list(number_begin_nodes))
	ALLOCATE(segment_list(number_begin_nodes))
	ALLOCATE(segment_list_new(number_begin_nodes))
	ALLOCATE(node_listedge1(number_begin_nodes))
	ALLOCATE(node_listedge2(number_begin_nodes))
	ALLOCATE(zlist(number_begin_nodes))
	ALLOCATE(segment_store(number_begin_nodes,max_num_points_per_seg))

!       find segment that bottom node is a part of?

	m=1			! new node label
	n=1			! new segment label
	node_tag=.false.
	segment_list=0
	segment_list_new=0
	segment_store=0
	segment_number_list=0
	node_listedge1=0
	node_listedge2=0
	node_store=0
!       renumber nodes and segments

!       set everything to zbound2 so that 'bottom nodes' that don't actually go anywhere don't mess up everything else
	zlist=zbound2

	open(unit=19,file='begin_nodes.txt',status='old')

	do i=1,number_begin_nodes
	   read(19,*) node_store(i)
	   node_tag(m)=.true.
	   node_listedge1(i)=m
	   xnew(m)=x(node_store(i))
	   ynew(m)=y(node_store(i))
	   znew(m)=z(node_store(i))
	   m=m+1
	   do j=num_backbone_segs+1,num_segments
	      if(node_store(i).eq.edge_connectivity1(j)) then
!	Define z coordinates of all bottom nodes
		 zlist(i)=z(node_store(i))
		 exit
	      end if
!     otherwise, zlist stays at max z level		
	   end do
	end do

	top_node=.false.
	started=.false.
!       Code_long_term


!       Set to a high number to ensure that you reach the end of all of the segments
	do 62 k=1,100000
	      q=1
! Find node with the lowest z coordinate
	      if(minval(zlist).lt.zbound2) then
		 do i=1,number_begin_nodes
		    if(zlist(i).eq.minval(zlist)) then
		       if(segment_list(i).eq.0) then
			  node=node_store(i)
		       else
			  node=edge_connectivity2(segment_list(i))
		       end if
		       if(node_used(node).eqv..false.) then
			  node_used(node)=.true.
			  current_begin_node=i
			  EXIT
		       else
			  !still acceptable if it is an internal node
			  if(segment_list(i).eq.0) then
			     zlist(i)=zbound2
			     GOTO 62
			  else
			     current_begin_node=i
			     EXIT
			  end if
		       end if
		    end if
		 end do
	      else
		 GOTO 63
	      end if

!       Check to see if segment contains a top node
	      if((z(node).ge.top_z) .and.(edge_connectivity1(segment_list(current_begin_node)).ne.-1)) THEN
		 top_node=.true.
!       Specify the node that you accept as used
		 node_used(node)=.true.

		 if(segment_list(current_begin_node).eq.0)then
		    started=.false.
		 end if
		
		 zlist(current_begin_node)=zbound2

		 if(started.eqv..false.) then
		    edge1new(n)=current_begin_node
		    edge2new(n)=m
		    node_tag(m)=.true.


		    do i=num_backbone_segs+1,num_segments
		       if(edge_connectivity1(i).eq.node) then
			  xnew(m)=x(edge_connectivity2(i))
			  ynew(m)=y(edge_connectivity2(i))
			  znew(m)=z(edge_connectivity2(i))
			  segment_number_list(current_begin_node)=segment_number_list(current_begin_node)+1
			  segment_list(current_begin_node)=i
			  segment_list_new(current_begin_node)=n
			  segment_store(current_begin_node,segment_number_list(current_begin_node))=i

!       Set edge_connectivities to -1 to ensure that the segment is not reused
			  edge_connectivity1(i)=-1
			  edge_connectivity2(i)=-1
		       end if
		    end do
		 else
		    edge1new(n)=m
		    edge2new(n)=m+1


!       Specify top node as well
		    node_tag(m+1)=.true.
		    xnew(m)=x(edge_connectivity2(segment_list(current_begin_node)))
		    ynew(m)=y(edge_connectivity2(segment_list(current_begin_node)))
		    znew(m)=z(edge_connectivity2(segment_list(current_begin_node)))
		    num_edge_points_new(n)=num_edge_points(segment_list(current_begin_node))

!       Set edge_connectivities to -1 to ensure that the segment is not reused
		    edge_connectivity1(segment_list(current_begin_node))=-1
		    edge_connectivity2(segment_list(current_begin_node))=-1
		 end if
		
		 m=m+1
		 n=n+1
		 GOTO 62
	      end if

	      deadendtag=.false.
	      do 23 j=num_backbone_segs+1,num_segments


		 if((edge_connectivity2(j).ne.-1) .and.((node_used(edge_connectivity2(j))).neqv..true.).and.(node.eq.edge_connectivity1(j))) then
		    nodetemp=edge_connectivity2(j)
		    xnewtemp(q)=x(nodetemp)
		    ynewtemp(q)=y(nodetemp)
		    znewtemp(q)=z(nodetemp)
		    seg(q)=j
		    q=q+1

!       Check to see if segment contains a top node
		    if((z(edge_connectivity2(j)).ge.top_z) .and. (edge_connectivity1(j).ne.-1)) THEN
		       top_node=.true.
		       GOTO 23
		    end if

!       If not a top node, check to ensure that connection is not a dead end
		    do 24 r=num_backbone_segs+1,num_segments
		       if(edge_connectivity2(j).eq.edge_connectivity1(r)) then !not a dead end	
			  GOTO 23
		       else
			  deadendtag=.true.
		       end if
 24		    continue
		    q=q-1

	      end if

!       If it is the end of a segment, finish off the calculation
	      if((j.eq.num_segments).and.(q.eq.1).and.(deadendtag.eqv..true.)) then
		 q=2
	      end if

 23	      continue

!       m-1 (i.e. previous segment)
	      correct_seg=seg(1)

!       bottom segment
	      if(segment_list(current_begin_node).eq.0) then
		 segment_list(current_begin_node)=-2
		 segment_list_new(current_begin_node)=-2
		 edge_connectivity2(segment_list(current_begin_node))=node_store(current_begin_node)
	      end if

	      distnew_sq=(xnewtemp(1)-x(edge_connectivity2(segment_list(current_begin_node))))**2.d0&
	      &+(ynewtemp(1)-y(edge_connectivity2(segment_list(current_begin_node))))**2.d0
	      d=((znewtemp(1)-z(edge_connectivity2(segment_list(current_begin_node))))*TAN(thetaguess))**2.d0
	      distnew_sq=distnew_sq-d
	      q=q-1

	      if(q.gt.0) then
		 do 61 s=1,q
		    dist_sq=(xnewtemp(s)-x(edge_connectivity2(segment_list(current_begin_node))))**2.d0&
		    &+(ynewtemp(s)-y(edge_connectivity2(segment_list(current_begin_node))))**2.d0
		    d=((znewtemp(s)-z(edge_connectivity2(segment_list(current_begin_node))))*TAN(thetaguess))**2.d0

		    dist_sq=ABS(dist_sq-d)

		    if(dist_sq.lt.distnew_sq) then
		       correct_seg=seg(s)
		       distnew_sq=dist_sq
		    end if
 61		 continue

		 length=xnewtemp(s)-x(edge_connectivity2(segment_list(current_begin_node)))&
		 &+ynewtemp(s)-y(edge_connectivity2(segment_list(current_begin_node)))&
		 &+znewtemp(s)-z(edge_connectivity2(segment_list(current_begin_node)))

		 if(edge_connectivity1(correct_seg).ne.-1) then
		    node=edge_connectivity2(correct_seg)

!		
!       Specify the node that you accept as used
		    node_used(node)=.true.
		
!       Set edge_connectivities to -1 to ensure that the segment is not reused
		    edge_connectivity1(correct_seg)=-1	
		    node_tag(m)=.true.

!       problem is on second step
		    if(node_listedge2(current_begin_node).ne.0) then
		       node_listedge1(current_begin_node)=node_listedge2(current_begin_node)
		    end if
		    node_listedge2(current_begin_node)=m

		    segment_number_list(current_begin_node)=segment_number_list(current_begin_node)+1

		    segment_store(current_begin_node,segment_number_list(current_begin_node))=correct_seg
		    zlist(current_begin_node)=z(edge_connectivity2(correct_seg))
		    edge_connectivity2(segment_list(current_begin_node))=-1
		    segment_list(current_begin_node)=correct_seg
		    segment_list_new(current_begin_node)=n


		    if((zlist(current_begin_node).ge.top_z) .and. (top_node.eqv..true.)) then
		       top_node=.false.
		       zlist(current_begin_node)=zbound2
		       xnew(m)=x(node)
		       ynew(m)=y(node)
		       znew(m)=z(node)
		       edge1new(n)=current_begin_node
		       edge2new(n)=m
		       n=n+1
		       m=m+1
		       GOTO 62
		    end if
		 else		! node is already used
!       Add so that code doesn't continually look for next segment from the same node
		    edge2new(current_begin_node)=m
		    m=m+1
		    n=n+1
		    zlist(current_begin_node)=zbound2
		    GOTO 62
		 end if
	      else
!       Add so that the code doesn't continually look for next segment from the same node
		 if(segment_number_list(current_begin_node).gt.0) then
		    xnew(m)=x(node)
		    ynew(m)=y(node)
		    znew(m)=z(node)
		    edge1new(n)=current_begin_node
		    edge2new(n)=m
		    m=m+1
		    n=n+1
		    node_used(node)=.true.
		 end if
		 zlist(current_begin_node)=zbound2
		 GOTO 62
	      end if
 62	   continue
 63	   continue

	num_nodes=m-1+num_backbone_nodes
	num_segments=n-1+num_backbone_segs

	do i=num_backbone_segs+1,num_segments
	   edge_connectivity1(i)=-1
	   edge_connectivity2(i)=-1
	end do

	do i=num_backbone_segs+1,num_segments
	   edge_connectivity1(i)=edge1new(i-num_backbone_segs)
	   edge_connectivity2(i)=edge2new(i-num_backbone_segs)
	end do

!       eliminate begin nodes that never produced
	p=num_backbone_nodes+1
	do i=1,m-1
	   do j=1,n-1
	      if((edge_connectivity1(j+num_backbone_segs).eq.i).or.(edge_connectivity2(j+num_backbone_segs).eq.i)) then
		 x(p)=xnew(i)
		 y(p)=ynew(i)
		 z(p)=znew(i)
		 do  k=1,n-1
		    if(edge_connectivity1(k+num_backbone_segs).eq.i) then
		       edge1new(k)=p
		    end if
		    if(edge_connectivity2(k+num_backbone_segs).eq.i) then
		       edge2new(k)=p
		    end if	
		 end do
		 p=p+1
	      end if
	   end do
	end do
	num_nodes=p-1

!       now sort the bottom nodes according to edge1
	p=num_backbone_nodes+1
	q=num_backbone_segs+1
	do i=1,m-1
	   do j=1,n-1
	   if(edge1new(j).eq.(i+num_backbone_nodes)) then
	      xnew(i)=x(i+num_backbone_nodes)
	      ynew(i)=y(i+num_backbone_nodes)
	      znew(i)=z(i+num_backbone_nodes)
	      edge_connectivity1(q)=edge1new(j)
	      edge_connectivity2(q)=edge2new(j)
	      p=p+1
	      q=q+1
	   end if
	   end do
	end do

	do i=1,m-1
	   x(i+num_backbone_nodes)=xnew(i)
	   y(i+num_backbone_nodes)=ynew(i)
	   z(i+num_backbone_nodes)=znew(i)
	end do


	ALLOCATE(coordx_new(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(coordy_new(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(coordz_new(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(radius_new(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(coordx_new2(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(coordy_new2(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(coordz_new2(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))
	ALLOCATE(radius_new2(num_backbone_segs+number_begin_nodes,max_num_points_per_seg+extra_points))

	ALLOCATE(num_points_new(num_segments_old))

	num_points_new=0


	do i=1,num_backbone_segs
	do j=1,num_edge_points(i)
            coordx_new(i,j)=coordx_in(i,j)
            coordy_new(i,j)=coordy_in(i,j)
            coordz_new(i,j)=coordz_in(i,j)
            radius_new(i,j)=radius_in(i,j)
	end do
	end do


        DEALLOCATE(coordx_in)
        DEALLOCATE(coordy_in)
        DEALLOCATE(coordz_in)
        DEALLOCATE(radius_in)
	ALLOCATE(coordx_in(num_backbone_segs+number_begin_nodes,max_num_points_per_seg))
	ALLOCATE(coordy_in(num_backbone_segs+number_begin_nodes,max_num_points_per_seg))
	ALLOCATE(coordz_in(num_backbone_segs+number_begin_nodes,max_num_points_per_seg))
	ALLOCATE(radius_in(num_backbone_segs+number_begin_nodes,max_num_points_per_seg))

	do i=1,num_backbone_segs
	do j=1,num_edge_points(i)
            coordx_in(i,j)=coordx_new(i,j)
            coordy_in(i,j)=coordy_new(i,j)
            coordz_in(i,j)=coordz_new(i,j)
            radius_in(i,j)=radius_new(i,j)
	end do
	end do

	coordx_new=0
	coordy_new=0
	coordz_new=0
	radius_new=0

	num_points_new=0


	do i=1,num_segments_old
	read(11,*) num_edge_points_old(i)
	end do
	read(11,*)
	read(11,*)

!       Edge point coordinates
	do 15 i=1,num_segments_old
          do j=1,number_begin_nodes
	      do k=1,max_num_points_per_seg
		 if((segment_store(j,k)-num_backbone_segs).eq.i) then
		    do 51 l=1,num_edge_points_old(i)
		       if((l+num_points_new(j)).gt.(max_num_points_per_seg+extra_points)) then
			  PRINT *,'Error: Not enough extra_points specified. Modify the input file to include more extra points.'
			  exit
		       end if
		       
		       read(11,*) coordx_new(j,l+num_points_new(j)),coordy_new(j,l+num_points_new(j)),coordz_new(j,l+num_points_new(j))
		       
!       convert to meters
		       coordx_new(j,l+num_points_new(j))=coordx_new(j,l+num_points_new(j))*convert_factor
		       coordy_new(j,l+num_points_new(j))=coordy_new(j,l+num_points_new(j))*convert_factor
		       coordz_new(j,l+num_points_new(j))=coordz_new(j,l+num_points_new(j))*convert_factor	
 51		    continue
		    
		    num_points_new(j)=num_points_new(j)+num_edge_points_old(i)		
		    GOTO 15
		 end if
	      end do
	   end do
!       did not find any matching segs

	   do l=1,num_edge_points_old(i)
	      read(11,*) blank1,blank2,blank3 
	   end do

 15	continue
	read(11,*)
	read(11,*)

	do i=1,num_segments
	   write(37,*) edge_connectivity1(i),edge_connectivity2(i)
	end do
	do i=1,number_begin_nodes
	num_points_new(i)=0
	end do


!       Thickness (i.e. radius) of vessel at each edge point	
	do 73 i=1,num_segments_old
	   do j=1,number_begin_nodes
	      do k=1,max_num_points_per_seg
		 if((segment_store(j,k)-num_backbone_segs).eq.i) then
		    do l=1,num_edge_points_old(i)
		       read(11,*) point_thick
!       convert units to meters
		       radius_new(j,l+num_points_new(j))=point_thick*convert_factor
		    end do
		    num_points_new(j)=num_points_new(j)+num_edge_points_old(i)
		    GOTO 73
		 end if
	      end do
	   end do

!       did not find any matching segs
	   do l=1,num_edge_points_old(i)
	      read(11,*) blank1
	   end do
 73	continue

	

!       Transfer all of the coords/radius to new indices

	p=1
	do j=1,number_begin_nodes
	   if(num_points_new(j).ne.0) then
	      num_edge_points_new(p)=num_points_new(j)
	      num_edge_points_new2(p)=num_points_new(j)
	      do n=1,num_edge_points_new(p)
		 coordx_new(p,n)=coordx_new(j,n)
		 coordy_new(p,n)=coordy_new(j,n)
		 coordz_new(p,n)=coordz_new(j,n)
		 radius_new(p,n)=radius_new(j,n)
	      end do
	      p=p+1
	   end if
	end do
	number_begin_nodes=p-1


	do k=1,number_begin_nodes
!       new point counter
	   coordz_new2(k,1)=coordz_new(k,1)
	   coordx_new2(k,1)=coordx_new(k,1)
	   coordy_new2(k,1)=coordy_new(k,1)
	   radius_new2(k,1)=radius_new(k,1)
	   u=2
	   currentz=coordz_new(k,1)
	   coordz_initial=coordz_new(k,1)

!       PROBLEM IS THAT THE NUMEDGEPOINTS IS THE TOTAL FOR THE SEGMENT

	   do n=1,num_edge_points_new(k)-1
	      if(NINT((coordz_new(k,n+1)-currentz)/voxel_size).ge.1) then
!       Skipped a couple of points, so add them back in
		 if(NINT((coordz_new(k,n+1)-currentz)/voxel_size).gt.2) then
		    num_steps=NINT((coordz_new(k,n+1)-currentz)/voxel_size)
		    do v=1,num_steps-1
		       coordz_new2(k,u)=voxel_size*(u-1)+coordz_initial
		       coordx_new2(k,u)=coordx_new(k,n+1)
		       coordy_new2(k,u)=coordy_new(k,n+1)
		       radius_new2(k,u)=radius_new(k,n+1)
		       num_edge_points_new2(k)=num_edge_points_new2(k)+1
		       u=u+1
		    end do
		 end if
		 coordz_new2(k,u)=voxel_size*(u-1)+coordz_initial
		 coordx_new2(k,u)=coordx_new(k,n+1)
		 coordy_new2(k,u)=coordy_new(k,n+1)
		 currentz=coordz_new2(k,u)
		 radius_new2(k,u)=radius_new(k,n+1)
		 u=u+1             		
	      else
		 num_edge_points_new2(k)=num_edge_points_new2(k)-1
	      end if
	      
	   end do
	   num_edge_points(k+num_backbone_segs)=num_edge_points_new2(k)
	end do


	q=1
	do 25 j=num_backbone_segs+1,num_segments
	   if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
	      num_edge_points(q+num_backbone_segs)=num_edge_points(j)
	      do m=1,num_edge_points(j)
		 coordx_in(q+num_backbone_segs,m)=coordx_new2(j-num_backbone_segs,m)
		 coordy_in(q+num_backbone_segs,m)=coordy_new2(j-num_backbone_segs,m)
		 coordz_in(q+num_backbone_segs,m)=coordz_new2(j-num_backbone_segs,m)
		 radius_in(q+num_backbone_segs,m)=radius_new2(j-num_backbone_segs,m)
	      end do
	      q=q+1
	   end if
 25	continue

!       Change the node coordinates to correspond with final points

	do j=num_backbone_segs+1,num_segments
	   x(edge_connectivity2(j))=coordx_in(j,num_edge_points(j))
	   y(edge_connectivity2(j))=coordy_in(j,num_edge_points(j))
	   z(edge_connectivity2(j))=coordz_in(j,num_edge_points(j))
	end do

!       Add new stuff about coordx_in, etc.
	num_bridge_nodes=num_nodes-num_backbone_nodes
	num_bridge_segs=num_segments-num_backbone_segs


	close(19)
!	call avizo_test
	PRINT *,'renumber_bridges'
!	pause

	close(11,status='delete')

        DEALLOCATE(zlist)
	DEALLOCATE(num_edge_points_old)
	DEALLOCATE(xnew)
	DEALLOCATE(ynew)
	DEALLOCATE(znew)
	DEALLOCATE(edge1new)
	DEALLOCATE(edge2new)
	DEALLOCATE(num_edge_points_new)
	DEALLOCATE(num_edge_points_new2)
	DEALLOCATE(xnewtemp)
	DEALLOCATE(ynewtemp)
	DEALLOCATE(znewtemp)
	DEALLOCATE(seg)
	DEALLOCATE(node_tag)
	DEALLOCATE(coordx_new)
	DEALLOCATE(coordy_new)
	DEALLOCATE(coordz_new)
	DEALLOCATE(radius_new)
	DEALLOCATE(coordx_new2)
	DEALLOCATE(coordy_new2)
	DEALLOCATE(coordz_new2)
	DEALLOCATE(radius_new2)
	DEALLOCATE(node_store)
	DEALLOCATE(segment_number_list)
	DEALLOCATE(segment_list)
	DEALLOCATE(segment_list_new)
	DEALLOCATE(node_listedge1)
	DEALLOCATE(node_listedge2)


	END SUBROUTINE renumber_bridges


	SUBROUTINE extend_vessels
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j,k,m,p,node_counter,num_new_points
	DOUBLE PRECISION :: dxdz,dydz
	DOUBLE PRECISION, ALLOCATABLE :: storecoordx(:),storecoordy(:),storecoordz(:),storeradius(:)
	DOUBLE PRECISION :: tot_radius,avg_radius

	ALLOCATE(storecoordx(max_num_points_per_seg))
	ALLOCATE(storecoordy(max_num_points_per_seg))
	ALLOCATE(storecoordz(max_num_points_per_seg))
	ALLOCATE(storeradius(max_num_points_per_seg))
	ALLOCATE(vessel_begin(num_segments))
	ALLOCATE(vessel_finish(num_segments))
	DEALLOCATE(node_used)
	num_vessels=num_segments
	vessel_begin=-1
	vessel_finish=-1

!       Extend bottom nodes to reach boundary
	do 77 i=1,num_backbone_nodes
	   if(z(i).le.bottom_z) then
	      node_counter=0
	      do 20 k=1,num_backbone_segs
		 if((edge_connectivity1(k).eq.i).or.(edge_connectivity2(k).eq.i)) then
		    node_counter=node_counter+1
		 end if
 20	      continue
	   else
	      node_counter=2
	   end if

	   if(node_counter.eq.1) then !bottom node
	      do 24 k=1,num_backbone_segs
		 if((edge_connectivity1(k).eq.i).and.(z(edge_connectivity2(k)).ge.top_z)) then !node matches bottom node
!       also, segment extends to top
		    num_new_points=NINT((z(i)-zbound1)/voxel_size)
		    tot_radius=0

		    do j=1,num_edge_points(k)
		       storecoordx(j)=coordx_in(k,j)
		       storecoordy(j)=coordy_in(k,j)
		       storecoordz(j)=coordz_in(k,j)
		       storeradius(j)=radius_in(k,j)
		       tot_radius=tot_radius+radius_in(k,j)
		    end do
		
		    avg_radius=tot_radius/num_edge_points(k)
		    dxdz=(x(edge_connectivity2(k))-x(edge_connectivity1(k)))/(z(edge_connectivity2(k))-z(edge_connectivity1(k)))
		    dydz=(y(edge_connectivity2(k))-y(edge_connectivity1(k)))/(z(edge_connectivity2(k))-z(edge_connectivity1(k)))
		    num_edge_points(k)=num_edge_points(k)+num_new_points

		    do m=1,num_new_points
!       Add new points, assuming straight vessel
		       coordz_in(k,m)=zbound1+(m-1)*voxel_size
		       coordx_in(k,m)=storecoordx(1)-dxdz*(storecoordz(1)-coordz_in(k,m))
		       coordy_in(k,m)=storecoordy(1)-dydz*(storecoordz(1)-coordz_in(k,m))
		       radius_in(k,m)=avg_radius
		    end do
		    do m=num_new_points+1,num_edge_points(k)
!       Add new points
		       coordx_in(k,m)=storecoordx(m-num_new_points)
		       coordy_in(k,m)=storecoordy(m-num_new_points)
		       coordz_in(k,m)=storecoordz(m-num_new_points)
		       radius_in(k,m)=storeradius(m-num_new_points)
		    end do
		
!       Change vertex coordinates
		    x(i)=coordx_in(k,1)
		    y(i)=coordy_in(k,1)
		    z(i)=zbound1
		 end if
 24	      continue
	   end if
 77	continue


!       Extend top_nodes
	do 78 i=1,num_backbone_nodes
	   if(z(i).ge.top_z) then
	      node_counter=0
	      do 22 k=1,num_backbone_segs
		 if((edge_connectivity1(k).eq.i).or.(edge_connectivity2(k).eq.i)) then
		    node_counter=node_counter+1
		 end if
 22	      continue
	      if(node_counter.eq.1) then !top node
	      do 25 k=1,num_backbone_segs
		 if((edge_connectivity2(k).eq.i).and.(z(edge_connectivity1(k)).le.bottom_z)) then !node matches top node
		    num_new_points=NINT((zbound2-z(i))/voxel_size)
		    if(num_new_points.lt.0) then
		       num_edge_points(k)=num_edge_points(k)+num_new_points-1
		    else
		       tot_radius=0
		       do j=1,num_edge_points(k)
			  tot_radius=tot_radius+radius_in(k,j)
		       end do
		
		       avg_radius=tot_radius/num_edge_points(k)
		       dxdz=(x(edge_connectivity2(k))-x(edge_connectivity1(k)))/(z(edge_connectivity2(k))-z(edge_connectivity1(k)))
		       dydz=(y(edge_connectivity2(k))-y(edge_connectivity1(k)))/(z(edge_connectivity2(k))-z(edge_connectivity1(k)))

		       do m=1,num_new_points
!       Add new points
			  coordz_in(k,m+num_edge_points(k))=z(i)+m*voxel_size
			  coordx_in(k,m+num_edge_points(k))=coordx_in(k,num_edge_points(k))&
			  &+dxdz*(coordz_in(k,m+num_edge_points(k))-coordz_in(k,num_edge_points(k)))
			  coordy_in(k,m+num_edge_points(k))=coordy_in(k,num_edge_points(k))&
			  &+dydz*(coordz_in(k,m+num_edge_points(k))-coordz_in(k,num_edge_points(k)))
			  radius_in(k,m+num_edge_points(k))=avg_radius		
		       end do
		       coordz_in(k,num_new_points+num_edge_points(k))=zbound2
		       num_edge_points(k)=num_edge_points(k)+num_new_points
		    end if
		    !Change vertex coordinate
		    x(i)=coordx_in(k,num_edge_points(k))
		    y(i)=coordy_in(k,num_edge_points(k))
		    z(i)=zbound2

		 end if
 25	      continue
	   end if
	   else
	      node_counter=0
	      do k=1,num_backbone_segs
		 if((edge_connectivity1(k).eq.i).or.(edge_connectivity2(k).eq.i)) then
		    node_counter=node_counter+1
		 end if
	      end do
	  
	      node_counter=2
	   end if
 78	continue

	do k=1,num_segments
!       Add half a voxel to ensure that points close by are also included
	    vessel_begin(k)=FLOOR((z(edge_connectivity1(k))+0.5*voxel_size)/(sampling_freq*voxel_size))+1
	    vessel_finish(k)=FLOOR((z(edge_connectivity2(k)))/(sampling_freq*voxel_size))+2
	    if((((z(edge_connectivity2(k))-((vessel_finish(k)-1)*voxel_size*sampling_freq))).lt.(0.5*voxel_size)).and.&
	    &((z(edge_connectivity2(k))-((vessel_finish(k)-1)*voxel_size*sampling_freq)).gt.0)) then
	       vessel_finish(k)=vessel_finish(k)-1
	    end if

	    if(((z(edge_connectivity2(k)))/(sampling_freq*voxel_size)).eq.(vessel_finish(k)-2))then
	       vessel_finish(k)=vessel_finish(k)-1
	    end if
	 end do

	DEALLOCATE(storecoordx)
	DEALLOCATE(storecoordy)
	DEALLOCATE(storecoordz)
	DEALLOCATE(storeradius)


!	PRINT *,'extend'
!	call avizo_test
!	pause
	END SUBROUTINE extend_vessels



	SUBROUTINE sampling
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j,k,p,q,m,edge_point_ct,edge_pt,init,num_backbone_segs_bef
	LOGICAL, Allocatable:: node_tag(:)

	ALLOCATE(node_tag(num_nodes))
	node_tag=.true.
	num_backbone_segs_bef=num_backbone_segs
	if(sampling_freq.gt.1) then

!       Re-write network in terms of fewer points
	   do 15 i=1,num_segments

	      edge_point_ct=FLOOR(DBLE(num_edge_points(i))/DBLE(sampling_freq)+1)

	      if(edge_point_ct.lt.2) edge_point_ct=2

	      if((FLOOR(DBLE(num_edge_points(i))/DBLE(sampling_freq))).ge.2) then

!       Change coordinates so that they land on exact spots
!       Find initial start point
		 init=NINT(ABS(FLOOR(coordz_in(i,1)/sampling_freq/voxel_size+1)*sampling_freq*voxel_size-coordz_in(i,1))/voxel_size)
		 
		 edge_point_ct=vessel_finish(i)-vessel_begin(i)+1
		 
		 coordx_in(i,1)=x(edge_connectivity1(i))
		 coordy_in(i,1)=y(edge_connectivity1(i))
		 coordz_in(i,1)=z(edge_connectivity1(i))
		 radius_in(i,1)=radius_in(i,init+1)
		 
		 edge_pt=1
		 
!       Start at 2 in order to keep initial point
		 do 26 j=2,edge_point_ct-1
		    if((coordz_in(i,1).eq.coordz_in(i,1+init))) then
		       edge_pt=(j-1)*sampling_freq+1
		       coordx_in(i,j)=coordx_in(i,edge_pt+init)
		       coordy_in(i,j)=coordy_in(i,edge_pt+init)
		       if(j.gt.2) then
			  coordz_in(i,j)=coordz_in(i,j-1)+sampling_freq*voxel_size
		       else
			  coordz_in(i,j)=vessel_begin(i)*sampling_freq*voxel_size
		       end if
		       radius_in(i,j)=radius_in(i,edge_pt+init)
		    else
		       edge_pt=(j-2)*sampling_freq+1
		       coordx_in(i,j)=coordx_in(i,edge_pt+init)
		       coordy_in(i,j)=coordy_in(i,edge_pt+init)
		       if(j.gt.2) then
			  coordz_in(i,j)=coordz_in(i,j-1)+sampling_freq*voxel_size
		       else
			  coordz_in(i,j)=vessel_begin(i)*sampling_freq*voxel_size
		       end if
		       radius_in(i,j)=radius_in(i,edge_pt+init)
		    end if
 26		 continue
		 
		 if(num_edge_points(i).gt.(edge_pt+init)) then
!       last point
		    coordx_in(i,edge_point_ct)=x(edge_connectivity2(i))
		    coordy_in(i,edge_point_ct)=y(edge_connectivity2(i))
		    coordz_in(i,edge_point_ct)=z(edge_connectivity2(i))
		    radius_in(i,edge_point_ct)=radius_in(i,num_edge_points(i))	
		    num_edge_points(i)=edge_point_ct
		 else
		    coordx_in(i,edge_point_ct-1)=x(edge_connectivity2(i))
		    coordy_in(i,edge_point_ct-1)=y(edge_connectivity2(i))
		    coordz_in(i,edge_point_ct-1)=z(edge_connectivity2(i))
		    num_edge_points(i)=edge_point_ct-1
		    vessel_finish(i)=vessel_finish(i)-1
		 end if
	      else		!elminate segment because it is too short
		 node_tag(edge_connectivity1(i))=.false.
		 node_tag(edge_connectivity2(i))=.false.

		 if(i.le.num_backbone_segs_bef) then
		    num_backbone_segs=num_backbone_segs-1
		    num_backbone_nodes=num_backbone_nodes-2
		 end if
	      end if
 15	   continue
	end if
	
!redefine max segments if the number of possible pits is greater
 if((num_segments*max_sampled_points_per_seg*2).lt.max_segments) then
   	max_segments=num_segments*max_sampled_points_per_seg*2
 end if


!       Add this in for memory concerns. If this is not enough segments, there will be an error later.
	if((max_segments*max_sampled_points_per_seg).gt.4E7)then
    max_segments=NINT(4E7/max_sampled_points_per_seg)
    end if
!       write out new network
	p=1
	do 21 k=1,num_nodes
	   if(node_tag(k).eqv..true.) then
	      x(p)=x(k)
	      y(p)=y(k)
	      z(p)=z(k)
	      
	      do 22 j=1,num_segments
		 if(edge_connectivity1(j).eq.k) then
		    edge_connectivity1(j)=p
		 end if
		 if(edge_connectivity2(j).eq.k) then
		    edge_connectivity2(j)=p
		 end if
		 
 22	      continue
	      
	      p=p+1
	   else
	      do 23 j=1,num_segments
		 if(edge_connectivity1(j).eq.k) then
		    edge_connectivity1(j)=-1
		 end if
		 if(edge_connectivity2(j).eq.k) then
		    edge_connectivity2(j)=-1
		 end if
 23	      continue
	   end if
 21	continue
	q=1
	do 25 j=1,num_segments
	   if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
	      vessel_begin(q)=vessel_begin(j)
	      vessel_finish(q)=vessel_finish(j)
	      edge_connectivity1(q)=edge_connectivity1(j)
	      edge_connectivity2(q)=edge_connectivity2(j)
	      num_edge_points(q)=num_edge_points(j)
	      do m=1,num_edge_points(j)
		 coordx_in(q,m)=coordx_in(j,m)
		 coordy_in(q,m)=coordy_in(j,m)
		 coordz_in(q,m)=coordz_in(j,m)
		 radius_in(q,m)=radius_in(j,m)
	      end do
	      q=q+1
	   end if
 25	continue
	
	
	num_segments=q-1
	num_nodes=p-1
	number_begin_nodes=num_segments
!       Set other num_edge_points to zero
	do j=num_segments+1,size(num_edge_points)
	   num_edge_points(j)=0
	end do
	
!	PRINT *,'sampling'
	call avizo_test
!       pause
	ALLOCATE(coordx(max_segments,max_sampled_points_per_seg))
	ALLOCATE(coordy(max_segments,max_sampled_points_per_seg))
	ALLOCATE(coordz(max_segments,max_sampled_points_per_seg))
	ALLOCATE(radius(max_segments,max_sampled_points_per_seg))
	

	do i=1,num_segments
	   do j=1,num_edge_points(i)
	      coordx(i,j)=coordx_in(i,j)
	      coordy(i,j)=coordy_in(i,j)
	      coordz(i,j)=coordz_in(i,j)
	      radius(i,j)=radius_in(i,j)
	   end do
	end do
	
	DEALLOCATE(coordx_in)
	DEALLOCATE(coordy_in)
	DEALLOCATE(coordz_in)
	DEALLOCATE(radius_in)
	DEALLOCATE(node_tag)

	END SUBROUTINE sampling

	SUBROUTINE vessel_angle
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j,num_vess
	DOUBLE PRECISION :: deltax,deltay,avg_angle
	DOUBLE PRECISION :: avg_x_angle,avg_y_angle
	DOUBLE PRECISION :: deltaz,deltar
	DOUBLE PRECISION :: sum_angles,sum_x_angle,sum_y_angle

	open(unit=36,file='output-vessel_properties.txt',status='unknown')
	sum_angles=0
	do 55 j=1,num_backbone_segs
	   deltax=x(edge_connectivity1(j))-x(edge_connectivity2(j))
	   deltay=y(edge_connectivity1(j))-y(edge_connectivity2(j))
	   deltaz=z(edge_connectivity1(j))-z(edge_connectivity2(j))
	   deltar=dsqrt(deltax**2.d0+deltay**2.d0)
	   sum_x_angle=ATAN(deltax/deltaz)+sum_x_angle
	   sum_y_angle=ATAN(deltay/deltaz)+sum_y_angle
	   sum_angles=ATAN(deltar/deltaz)+sum_angles
 55	continue

	avg_angle=(sum_angles/num_backbone_segs)*180.0/3.1415927
	avg_x_angle=(sum_x_angle/num_backbone_segs)*180.0/3.1415927
	avg_y_angle=(sum_y_angle/num_backbone_segs)*180.0/3.1415927
	write(36,*) 'Average Vessel Angle (degrees): ', avg_angle

	
!       Find center coordinates as a function of z
	ALLOCATE(xcenter_interp(max_sampled_points_per_seg-extra_points))
	ALLOCATE(ycenter_interp(max_sampled_points_per_seg-extra_points))
	
	xcenter_interp=0
	ycenter_interp=0

	xcenter_interp(1)=xcenter
	ycenter_interp(1)=ycenter
	do i=2,max_sampled_points_per_seg-extra_points
	   xcenter_interp(i)=xcenter+i*voxel_size*sampling_freq*SIN(avg_x_angle*3.1415927/180.d0)
	   ycenter_interp(i)=ycenter+i*voxel_size*sampling_freq*SIN(avg_y_angle*3.1415927/180.d0)
	end do

	END SUBROUTINE vessel_angle

	SUBROUTINE vessel_diameter
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j
	DOUBLE PRECISION :: sum_diameters,avg_diameter
	DOUBLE PRECISION :: deviation_sq,stdev
	DOUBLE PRECISION :: dist_center
	
	open(unit=55,file='output-vessel_diameter_distribution.txt',status='unknown')	

	ALLOCATE(v_diam(num_segments))

	write(55,*) 'Vessel no.   Vessel Diameter           Radial Coordinate          x                            y         '&
        &'                z'
	sum_diameters=0
	deviation_sq=0
	v_diam=0

	do 53 j=1,num_backbone_segs
	   dist_center=0

	   do 56 i=1,num_edge_points(j)
	      v_diam(j)=2.d0*radius(j,i)+v_diam(j)
	      sum_diameters=sum_diameters+2.d0*radius(j,i)/num_edge_points(j)
	      dist_center=dsqrt((x(edge_connectivity1(j))-xcenter_interp(INT((max_sampled_points_per_seg-extra_points)/2)))**2&
	      &+(y(edge_connectivity1(j))-ycenter_interp(INT((max_sampled_points_per_seg-extra_points)/2)))**2)+dist_center
 56	   continue
	   v_diam(j)=v_diam(j)/num_edge_points(j)
	   write(55,'(I3,11x,F23.21,3x,F23.21,5x,F23.21,5x,F23.21,5x,F23.21)')j, v_diam(j), dist_center/num_edge_points(j),&
              &x(edge_connectivity1(j)),y(edge_connectivity1(j)),z(edge_connectivity1(j))
 53	continue

	avg_diameter=sum_diameters/num_backbone_segs

	do 54 j=1,num_backbone_segs
        do 57 i=1,num_edge_points(j)
	     deviation_sq=(2.d0*radius(j,i)-avg_diameter)**2.d0/num_edge_points(j)+deviation_sq
 57	   continue
 54	continue

	stdev=sqrt(deviation_sq/num_backbone_segs)
	write(36,*) 'Number of Vessels: ',num_backbone_segs
	write(36,*) 'Average Vessel Diameter (meters): ', avg_diameter
	write(36,*) 'Standard Deviation (meters): ', stdev

	do 63 j=num_backbone_segs+1,num_segments
	   dist_center=0
	   do 66 i=1,num_edge_points(j)
	      v_diam(j)=2.d0*radius(j,i)+v_diam(j)
 66	   continue
	   v_diam(j)=v_diam(j)/num_edge_points(j)
 63	continue

	END SUBROUTINE vessel_diameter



	SUBROUTINE pitfield
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j,k,l,m,p,q,r,s,u,pit_node_counter
	INTEGER :: firstnode1,firstnode2,secondnode1,secondnode2
	INTEGER, ALLOCATABLE :: pit_number(:)
	DOUBLE PRECISION :: dist,dist_sq,a,b,c,t,ctocdist,z_last_edge
	DOUBLE PRECISION :: xc1,yc1,zc1,xc2,yc2,zc2,d
	INTEGER :: bound1x,bound1y,bound2x,bound2y
	INTEGER :: vess_count
        DOUBLE PRECISION :: threshold
	INTEGER :: last_edge,edgeptcounter,num_points_seg
	INTEGER ::  point_store,boundary1,boundary2,num_steps
	LOGICAL, ALLOCATABLE :: pit_now(:)
	DOUBLE PRECISION :: pi
	LOGICAL :: find_boundary
	INTEGER :: z_count,xbox,ybox,zbox
	DOUBLE PRECISION :: xmin_dp,xmax_dp,ymin_dp,ymax_dp
	DOUBLE PRECISION :: zmin_dp,zmax_dp
	INTEGER :: xmin,xmax,ymin,ymax,zmin,zmax,smin


	open(unit=47,file='distmap.am',status='old')
	do i=1,15
	   read(47,*)
	end do

 if(new_tag.eqv..true.) then
    read(47,*)
 end if
	xmin_dp=xbound1/voxel_size
	xmax_dp=xbound2/voxel_size
	ymin_dp=ybound1/voxel_size
        ymax_dp=ybound2/voxel_size
	zmin_dp=zbound1/voxel_size
        zmax_dp=zbound2/voxel_size


	xmin=NINT(xmin_dp)
	ymin=NINT(ymin_dp)
	zmin=NINT(zmin_dp)
	xmax=NINT(xmax_dp)
	ymax=NINT(ymax_dp)
	zmax=NINT(zmax_dp)

	if((NINT(xmin_dp).ne.INT(xmin_dp+0.1d0)).or.(NINT(xmax_dp).ne.INT(xmax_dp+0.1d0)).or.(NINT(ymin_dp).ne.INT(ymin_dp+0.1d0))&
	&.or.(NINT(ymax_dp).ne.INT(ymax_dp+0.1d0)).or.(NINT(zmin_dp).ne.INT(zmin_dp+0.1d0)).or.(NINT(zmax_dp).ne.INT(zmax_dp+0.1d0))) then
	   PRINT *, 'Error: Points are not lined up on the voxels.','xmin:', NINT(xmin_dp),INT(xmin_dp+0.1d0),'xmax:', &
	   &NINT(xmax_dp),INT(xmax_dp+0.1d0),'ymin:', NINT(ymin_dp),INT(ymin_dp+0.1d0),'ymax:', NINT(ymax_dp),INT(ymax_dp+0.1d0),&
	   &'zmin:', NINT(zmin_dp),INT(zmin_dp+0.1d0),'zmax:', NINT(zmax_dp),INT(zmax_dp+0.1d0)
	end if

!       Rescale variables so that you can index them properly
	xscale=-xmin+1
	yscale=-ymin+1
	zscale=-zmin+1
	xmax=xmax+xscale
	ymax=ymax+yscale
	zmax=zmax+zscale
	xmin=1
	ymin=1
	zmin=1
	PRINT *,(xmax-xmin)*(ymax-ymin)*(zmax-zmin),' number of distance map elements'          

!	if(((xmax-xmin)*(ymax-ymin)*(zmax-zmin)).gt.) then
!	   PRINT *, 'ERROR: Probably out of memory. Sampling frequency
!               too low.'
! 	end if

	Z_count=1

	ALLOCATE(pit_z(num_segments,max_sampled_points_per_seg))
	ALLOCATE(pit_node(num_segments,max_sampled_points_per_seg))
	ALLOCATE(boundary_label(xmax-xmin+1,ymax-ymin+1))
	ALLOCATE(vessel1(multFact*num_segments))
	ALLOCATE(vessel2(multFact*num_segments))
	ALLOCATE(coordstart(multFact*num_segments))
	ALLOCATE(coordend(multFact*num_segments))
	ALLOCATE(num_pits_vv(num_segments,num_segments))
	ALLOCATE(num_pitted_segs(num_segments))
	ALLOCATE(num_vv_connections(num_segments))
	ALLOCATE(pit_now(num_segments))
	ALLOCATE(pit_number(num_segments))
	ALLOCATE(num_pit_connections(num_segments))


	boundary_label=.false.

	num_pitted_segs=0
	pit_z=0
	pit_node=0
	pit_seg_counter=0
	pit_node_counter=0
	num_pit_connections=0
	num_pits_vv=0
	num_vv_connections=0
	p=num_segments
	point_store=0
	pi=2.d0*ACOS(0.d0)
	num_vess_conns=1


!       all segments should go from bottom to top, so just specify the maximum number
!       of points on a segment, assuming at least one segment goes from the bottom to the top
!       note that end nodes are not included for pitting
  smin=FLOOR(zbound1/(sampling_freq*voxel_size))+2
	maxpoints=maxval(num_edge_points)-1
	do 42 s=smin,smin+maxpoints-2
	   boundary_label=.false.
	   if(s.eq.2) then	! first step need to add one
	      do ybox=ymin,ymax
		 do xbox=xmin,xmax
		    read(47,*)dist
		 end do
	      end do
	   end if
	   
	   do zbox=1,sampling_freq
	      do ybox=ymin,ymax
		 do xbox=xmin,xmax  
		    read(47,*) dist
		    if((zbox.eq.sampling_freq) .or. (z_count.eq.zmax)) then
		       if((dist.le.(DSQRT(2.d0)*voxel_size)).and.(dist.gt.0)) then
			  boundary_label(xbox,ybox)=.true.
		       else
			  boundary_label(xbox,ybox)=.false.
		       end if
		    end if
		 end do
	      end do
	      z_count=z_count+1
	   end do

	   pit_now=.false.
!       Check voxels on neighboring vessels
	   do 43 i=1,p-1
	      firstnode1=edge_connectivity1(i)
	      firstnode2=edge_connectivity2(i)

	      if((s.gt.vessel_begin(i)).and.(s.lt.vessel_finish(i))) then
		 
		 do 44 j=i+1,p
		    
		    secondnode1=edge_connectivity1(j)
		    secondnode2=edge_connectivity2(j)
		    
		    if((s.gt.vessel_begin(j)).and.(s.lt.vessel_finish(j))) then
		       
		       xc1=coordx(i,s-vessel_begin(i)+1)
		       xc2=coordx(j,s-vessel_begin(j)+1)
		       yc1=coordy(i,s-vessel_begin(i)+1)
		       yc2=coordy(j,s-vessel_begin(j)+1)
		       zc1=coordz(i,s-vessel_begin(i)+1)
		       zc2=coordz(j,s-vessel_begin(j)+1)
		       
!       Calculate distance between centers of neighboring vessels
		       ctocdist=dsqrt((xc1-xc2)**2+(yc1-yc2)**2+(zc1-zc2)**2)
		       
		       threshold=neighbor_factor*(radius(i,1)+radius(j,1))+pit_dist
	
		       if(ctocdist.lt.threshold) then
!       Set dist high so that no extraneous pits are added
			  dist=pit_dist+1
			  dist_sq=dist*dist
			  
			  
			  CALL pit_test(xc1,yc1,zc1,xc2,yc2,zc2,dist_sq,boundary1,boundary2,bound1x,bound1y,bound2x,bound2y,find_boundary)
		
			  If((dist_sq.gt.pit_dist_sq) .and. (dist_sq.lt.4*pit_dist_sq) .and. (find_boundary.eqv..true.)) then
			     call pit_boundary(i,j,s-vessel_begin(i)+1,s-vessel_begin(j)+1,dist_sq,bound1x,bound1y,bound2x,bound2y)
			  end if
			 
			  if(dist_sq.le.pit_dist_sq) then
!       Pit connection
!       check to make sure that pits are not redundant

			     if(pit_now(i).eqv..false.) then
				pit_now(i)=.true.
				pit_number(i)=num_nodes+1
				x(num_nodes+1)=xc1
				y(num_nodes+1)=yc1
				z(num_nodes+1)=zc1
				if(num_pitted_segs(i).eq.0) then
				   num_pitted_segs(i)=num_pitted_segs(i)+2
				else
				   num_pitted_segs(i)=num_pitted_segs(i)+1
				end if
				pit_z(i,num_pitted_segs(i)-1)=z(num_nodes+1)
				pit_node(i,num_pitted_segs(i)-1)=num_nodes+1
				edge_connectivity1(num_segments+1)=num_nodes+1
				num_nodes=num_nodes+1
				pit_node_counter=pit_node_counter+1
			     else
				edge_connectivity1(num_segments+1)=pit_number(i)
			     end if
			     
			           
			     if(pit_now(j).eqv..false.) then
				pit_now(j)=.true.
				pit_number(j)=num_nodes+1
				x(num_nodes+1)=xc2	
				y(num_nodes+1)=yc2
				z(num_nodes+1)=zc2
				if(num_pitted_segs(j).eq.0) then
				   num_pitted_segs(j)=num_pitted_segs(j)+2
				else
				   num_pitted_segs(j)=num_pitted_segs(j)+1
				end if
!       Label new pit nodes and z coordinates of pit nodes
				pit_z(j,num_pitted_segs(j)-1)=z(num_nodes+1)
				pit_node(j,num_pitted_segs(j)-1)=num_nodes+1
				edge_connectivity2(num_segments+1)=num_nodes+1
				num_nodes=num_nodes+1
				pit_node_counter=pit_node_counter+1
			     else
				edge_connectivity2(num_segments+1)=pit_number(j)
			     end if
			     num_pit_connections(i)=num_pit_connections(i)+1
			     num_pit_connections(j)=num_pit_connections(j)+1

			     if((edge_connectivity1(num_segments+1).eq.131) .and. (edge_connectivity2(num_segments+1).eq.565)) then
                                 PRINT *,'ERROR: distant centres?'
                                 
                             end if


			     do q=1,num_vess_conns
				if((vessel1(q).eq.i).and.(vessel2(q).eq.j).and.(coordend(q).eq.(s-1)))then
				   coordend(q)=s
				   exit
				end if
				if(q.eq.num_vess_conns) then
				   vessel1(num_vess_conns)=i
				   vessel2(num_vess_conns)=j
				   coordstart(num_vess_conns)=s
				   coordend(num_vess_conns)=s
				   num_vess_conns=num_vess_conns+1
				end if
			     end do
			     if(num_pits_vv(i,j).eq.0) then
				num_vv_connections(i)=num_vv_connections(i)+1
				num_vv_connections(j)=num_vv_connections(j)+1
			     end if
			     
			     num_pits_vv(i,j)=num_pits_vv(i,j)+1
			     num_segments=num_segments+1
			     pit_seg_counter=pit_seg_counter+1
			     
        
        if((num_segments.ge.max_segments) .or. (num_segments.gt.hardLim)) then
           PRINT *,'Pitting Error: Maximum number of segments exceeded'
           PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
           pause
        end if
			     num_edge_points(num_segments)=2
			     coordx(num_segments,1)=xc1
			     coordy(num_segments,1)=yc1
			     coordz(num_segments,1)=zc1
			     coordx(num_segments,2)=xc2
			     coordy(num_segments,2)=yc2
			     coordz(num_segments,2)=zc2
			     
			     
			     if(radius(i,s-vessel_begin(i)+1).gt.radius(j,s-vessel_begin(j)+1)) then
				radius(num_segments,1)=radius(j,s-vessel_begin(j)+1)
				radius(num_segments,2)=radius(j,s-vessel_begin(j)+1)
			     else
				radius(num_segments,1)=radius(i,s-vessel_begin(i)+1)
				radius(num_segments,2)=radius(i,s-vessel_begin(i)+1)
			     end if
			  end if
		       end if
		    end if
 44		 continue
	      end if
 43	   continue
 42	continue

	close(47)
	num_vess_conns=num_vess_conns-1

!       Add pits and do analysis on them
	r=num_segments-pit_seg_counter
!       pit_z gives you z coordinate of pit so that you can determine the no. of edge points

	do 48 j=1,r
	   z_last_edge=z(edge_connectivity2(j))
	   last_edge=edge_connectivity2(j)
	   num_points_seg=num_edge_points(j)
	   vess_count=1

	   if(num_pitted_segs(j).eq.2) then
	      edge_connectivity1(num_segments+1)=pit_node(j,1)
	      edge_connectivity2(num_segments+1)=edge_connectivity2(j)
	      edge_connectivity2(j)=pit_node(j,1)
	
	      do m=1,num_points_seg
		 if(pit_z(j,1).eq.coordz(j,m)) then
		    num_edge_points(num_segments+1)=num_points_seg-m+1
		    num_edge_points(j)=m
		    exit
		 end if
	      end do

	      do q=1,num_edge_points(num_segments+1)
		 coordx(num_segments+1,q)=coordx(j,q+num_edge_points(j)-1)
		 coordy(num_segments+1,q)=coordy(j,q+num_edge_points(j)-1)
		 coordz(num_segments+1,q)=coordz(j,q+num_edge_points(j)-1)
		 radius(num_segments+1,q)=radius(j,q+num_edge_points(j)-1)
	      end do

	      num_segments=num_segments+1
	      if((num_segments.gt.max_segments) .or. (num_segments.gt.hardLim)) then
		 PRINT *,'Pitting Error: Maximum number of segments exceeded'
   PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
                 pause
	      end if
	   elseif(num_pitted_segs(j).gt.2) then
		edgeptcounter=0
		point_store=0
	      do 86 k=1,num_pitted_segs(j)
		 vess_count=vess_count+1

		 if(k.eq.num_pitted_segs(j)) then
		    edge_connectivity2(num_segments+1)=last_edge

		    num_edge_points(num_segments+1)=num_points_seg-edgeptcounter+1
  		    do q=1,num_edge_points(num_segments+1)
		       coordx(num_segments+1,q)=coordx(j,q+point_store)
		       coordy(num_segments+1,q)=coordy(j,q+point_store)
		       coordz(num_segments+1,q)=coordz(j,q+point_store)
		       radius(num_segments+1,q)=radius(j,q+point_store)
		    end do

		    if(num_edge_points(num_segments+1).eq.1) then
		       num_edge_points(num_segments+1)=2
		       coordx(num_segments+1,2)=coordx(num_segments+1,1)
		       coordy(num_segments+1,2)=coordy(num_segments+1,1)
		       coordz(num_segments+1,2)=coordz(num_segments+1,1)
		       radius(num_segments+1,2)=radius(num_segments+1,1)
		    end if

		    num_segments=num_segments+1

		    if((num_segments.gt.max_segments).or.(num_segments.gt.hardLim)) then
		       PRINT *,'Pitting Error: Maximum number of segments exceeded'
PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
		    end if
		 else
		    if(k.eq.1) then
		       edge_connectivity2(j)=pit_node(j,k)
		       edge_connectivity1(num_segments+1)=pit_node(j,k)

		       do m=1,num_points_seg
			  if(pit_z(j,k).eq.coordz(j,m)) then
			     num_edge_points(j)=m
			     edgeptcounter=num_edge_points(j)
			     exit
			  end if
		       end do
		       point_store=num_edge_points(j)-1
		    else
		       do m=1,num_points_seg
			  if(pit_z(j,k).eq.coordz(j,m)) then
			     num_edge_points(num_segments+1)=m-edgeptcounter+1
			     edgeptcounter=num_edge_points(num_segments+1)+edgeptcounter-1	  
			     exit
			  end if
		       end do

		       edge_connectivity2(num_segments+1)=pit_node(j,k)
		       edge_connectivity1(num_segments+2)=pit_node(j,k)

		       do q=1,num_edge_points(num_segments+1)
			  coordx(num_segments+1,q)=coordx(j,q+point_store)
			  coordy(num_segments+1,q)=coordy(j,q+point_store)
			  coordz(num_segments+1,q)=coordz(j,q+point_store)
			  radius(num_segments+1,q)=radius(j,q+point_store)
		       end do
		       if(num_edge_points(num_segments).eq.1) then
			  num_edge_points(num_segments+1)=2
			  coordx(num_segments+1,2)=coordx(num_segments+1,1)
			  coordy(num_segments+1,2)=coordy(num_segments+1,1)
			  coordz(num_segments+1,2)=coordz(num_segments+1,1)
			  radius(num_segments+1,2)=radius(num_segments+1,1)
		       end if

		       point_store=num_edge_points(num_segments+1)+point_store-1
		
		       num_segments=num_segments+1

		       if((num_segments.gt.max_segments).or.(num_segments.gt.hardLim)) then
            PRINT *,'Pitting Error: Maximum number of segments exceeded'
PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
            pause
         end if
      end if

		 end if
 86	      continue
	
	   end if
 48	continue


!       Fill in missing connections in a pitfield based on user input
	call fill_in_connections

 !      Remove connections in pitfield that are too short based on user input
	call remove_short_connections

	DEALLOCATE(num_pit_connections)
	DEALLOCATE(pit_node)	
	DEALLOCATE(pit_z)
	DEALLOCATE(num_pitted_segs)
	DEALLOCATE(boundary_label)
	DEALLOCATE(pit_number)
	DEALLOCATE(pit_now)
	END SUBROUTINE pitfield

	SUBROUTINE pit_test(xc1,yc1,zc1,xc2,yc2,zc2,dist_sq,bound1,bound2,bound1x,bound1y,bound2x,bound2y,find_boundary)
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: l,num_steps
	DOUBLE PRECISION :: t,a,b,c
	INTEGER :: nearest_x,nearest_y
	DOUBLE PRECISION,INTENT(IN) :: xc1,yc1,zc1,xc2,yc2,zc2
        INTEGER,INTENT(OUT) :: bound1x,bound1y
	INTEGER,INTENT(OUT) :: bound2x,bound2y
        DOUBLE PRECISION,INTENT(OUT) :: dist_sq
	DOUBLE PRECISION :: ctocdist,threshold
	INTEGER,INTENT(OUT) :: bound1,bound2
	LOGICAL :: boundary_tag
	LOGICAL,INTENT(OUT) :: find_boundary

	c=(zc2-zc1)
	b=(yc2-yc1)
	a=(xc2-xc1)
	find_boundary=.true.
	ctocdist=dsqrt((xc1-xc2)**2+(yc1-yc2)**2+(zc1-zc2)**2)

!       Advance one half voxel at a time between center points
	num_steps=ctocdist/(0.5*voxel_size)
	boundary_tag=.false.
	do 84 l=1,num_steps
	   if(l.eq.num_steps) THEN
!       Add just in case all of the last elements are boundary elements
	      If(boundary_tag.eqv..true.) exit
	      PRINT *,'ERROR: Boundary 1 not found. x1=',xc1,'y1=',yc1,'z1=',zc1,'x2=',xc2,'y2=',yc2,'z2=',zc2
	      find_boundary=.false.
!       Make large so that no v-v connection is formed
!	      dist_sq=10*pit_dist
	      dist_sq=pit_dist*pit_dist
	      GOTO 86
	   end if
	   t=DBLE(l-1)/DBLE(num_steps)
!       Scaled by voxel size
	   nearest_x=NINT((xc1+a*t)/voxel_size+xscale)
	   nearest_y=NINT((yc1+b*t)/voxel_size+yscale)
	   if(boundary_label(nearest_x,nearest_y).eqv..true.) then
	      bound1x=nearest_x
	      bound1y=nearest_y
	      bound1=l
	      boundary_tag=.true.

	   elseif(boundary_tag.eqv..true.) then !no more boundary elements
	      exit
	   end if

 84	continue
	boundary_tag=.false.
	do 85 l=1,num_steps
	   if(l.eq.num_steps) THEN
!       Add just in case all of the last elements are boundary elements
	      If(boundary_tag.eqv..true.) exit
	      PRINT *,'ERROR: Boundary 2 not found. x=',xc2,'y=',yc2,'z=',zc2
	      find_boundary=.false.
!       Make large so that no v-v connection is formed
!	      dist_sq=10*pit_dist
	      dist_sq=pit_dist*pit_dist
	      GOTO 86
	   end if

!       Scaled by voxel size
	   t=DBLE(l-1)/DBLE(num_steps)
	   nearest_x=NINT((xc2-a*t)/voxel_size+xscale)
	   nearest_y=NINT((yc2-b*t)/voxel_size+yscale)

	   if(boundary_label(nearest_x,nearest_y).eqv..true.) then
	      bound2x=nearest_x
	      bound2y=nearest_y
	      bound2=l
	      dist_sq=(DBLE(bound2x)-DBLE(bound1x))**2+(DBLE(bound2y)-DBLE(bound1y))**2
!       Scale back to actual distance
	      dist_sq=dist_sq*voxel_size*voxel_size
	      boundary_tag=.true.
	   elseif(boundary_tag.eqv..true.) then !no more boundary elements
	      exit
	   end if
 85	continue
 86	continue
	END SUBROUTINE pit_test

	SUBROUTINE pit_boundary(j,k,s1,s2,dist_sq,bound1x, bound1y,bound2x,bound2y)
	USE gcmmn
	IMPLICIT NONE
	INTEGER:: h,i,l,m,n,o,p,q,max_boundary_voxels
	INTEGER, INTENT(IN) :: j,k,s1,s2
	INTEGER:: x_voxels,y_voxels,num_steps,next_x,next_y
	DOUBLE PRECISION,INTENT(OUT) :: dist_sq
	INTEGER, DIMENSION(2) :: counter
	INTEGER, ALLOCATABLE :: sugg_voxels(:,:)
	LOGICAL :: go_tag,reverse_tag
	INTEGER, ALLOCATABLE :: bound_x(:,:),bound_y(:,:)
        INTEGER :: xmin,xmax,ymin,ymax,nearest_x,nearest_y
	DOUBLE PRECISION :: xmin_dp,xmax_dp,ymin_dp,ymax_dp
	INTEGER:: maxbox_x,minbox_x,maxbox_y,minbox_y
	INTEGER,INTENT(IN) :: bound1x,bound1y,bound2x,bound2y

	ALLOCATE(bound_x(2,10000))
	ALLOCATE(bound_y(2,10000))

	xmin_dp=xbound1/voxel_size
	xmax_dp=xbound2/voxel_size
	ymin_dp=ybound1/voxel_size
        ymax_dp=ybound2/voxel_size
	
	xmin=NINT(xmin_dp)
	ymin=NINT(ymin_dp)

	xmax=NINT(xmax_dp)
	ymax=NINT(ymax_dp)

!       Make square smaller--coordinates
	maxbox_x=coordx(j,s1)/voxel_size
	maxbox_y=coordy(j,s1)/voxel_size
	minbox_x=coordx(k,s2)/voxel_size
	minbox_y=coordy(k,s2)/voxel_size

	if(coordx(k,s2).gt.coordx(j,s1)) THEN
	   maxbox_x=coordx(k,s2)/voxel_size
	   minbox_x=coordx(j,s1)/voxel_size
	end if
	
	if(coordy(k,s2).gt.coordy(j,s1)) THEN
	   maxbox_y=coordy(k,s2)/voxel_size
	   minbox_y=coordy(j,s1)/voxel_size
	end if

	maxbox_x=maxbox_x+max_radius/voxel_size+xscale
	minbox_x=minbox_x-max_radius/voxel_size+xscale
	maxbox_y=maxbox_y+max_radius/voxel_size+yscale
	minbox_y=minbox_y-max_radius/voxel_size+yscale

	if(minbox_x.lt.1) then
	   minbox_x=0
	end if

	if(minbox_y.lt.1) then
	   minbox_y=0
	end if

	if(maxbox_x.gt.1) then
	   maxbox_x=xmax
	end if

	if(maxbox_y.gt.1) then
	   maxbox_y=ymax
	end if

	ALLOCATE(sugg_voxels(maxbox_x-minbox_x+1,maxbox_y-minbox_y+1))

	x_voxels=maxbox_x-minbox_x+1
	y_voxels=maxbox_y-minbox_y+1

!       0 indicates untried, 1 indicates tried and worked (need to check neighbors), 2 indicates finished
	sugg_voxels=0
	bound_x=0
	bound_y=0

	max_boundary_voxels=(maxbox_x-minbox_x+1)*(maxbox_y-minbox_y+1)


	do 90 h=1,2
	   counter(h)=0
	   if(h.eq.1) then
	      i=j
	      next_x=bound1x
	      next_y=bound1y
              sugg_voxels(next_x-minbox_x,next_y-minbox_y)=1
	   else
	      i=k
	      next_x=bound2x
	      next_y=bound2y
              sugg_voxels(next_x-minbox_x,next_y-minbox_y)=1
	   end if

	
!       Find all of the boundary elements
	   do 85 o=1,max_boundary_voxels
	      do 15 p=1,x_voxels
		 do 16 q=1,y_voxels
		    if(sugg_voxels(p,q).eq.1) then
		       next_x=p+minbox_x
		       next_y=q+minbox_y
		       go_tag=.true.
		
		       do 79 n=1,max_boundary_voxels
			  if((next_x.eq.nearest_x) .and. (next_y.eq.nearest_y)) GOTO 85 !no other easily accessible boundary voxels
			  nearest_x=next_x
			  nearest_y=next_y

			  if(boundary_label(nearest_x,nearest_y).eqv..true.) then
			     if(go_tag.eqv..false.) then
				GOTO 85 !No more easy to find boundary voxels for this round
			     else
				go_tag=.false.
				counter(h)=counter(h)+1
				if(counter(h).ge.10000) THEN
				   PRINT *, 'Error: Number of boundary voxels too large'
				   exit
				end if
			
				bound_x(h,counter(h))=nearest_x
				bound_y(h,counter(h))=nearest_y
				sugg_voxels(nearest_x-minbox_x,nearest_y-minbox_y)=2
				
				do m=1,8
				   if(m.eq.1) then
				      if(nearest_x.lt.INT((xbound2-xbound1)/voxel_size-minbox_x)) then
					 if(sugg_voxels(nearest_x+1,nearest_y-minbox_y).ne.2)then
					    if(boundary_label(nearest_x+1,nearest_y).eqv..true.) then
					       if((nearest_x+2).lt.maxbox_x) then
						  next_x=nearest_x+1
						  next_y=nearest_y
						  go_tag=.true.
						  sugg_voxels(nearest_x+1-minbox_x,nearest_y-minbox_y)=1
					       else
						  sugg_voxels(nearest_x+1-minbox_x,nearest_y-minbox_y)=2
					       end if
					    else
					       sugg_voxels(nearest_x+1-minbox_x,nearest_y-minbox_y)=2
					    end if
					 end if
				      end if
				   elseif(m.eq.2) then
				     if((nearest_x.lt.INT((xbound2-xbound1)/voxel_size-minbox_x)).and.((nearest_y-minbox_y).gt.1)) then
					 if(sugg_voxels(nearest_x+1-minbox_x,nearest_y-1-minbox_y).ne.2) then	
					    if(boundary_label(nearest_x+1,nearest_y-1).eqv..true.) then
					       if(((nearest_y-2).gt.minbox_y) .and. ((nearest_x+2).lt.maxbox_x)) then
						  next_x=nearest_x+1
						  next_y=nearest_y-1
						  go_tag=.true.
						  sugg_voxels(nearest_x+1-minbox_x,nearest_y-1-minbox_y)=1
					       else
						  sugg_voxels(nearest_x+1-minbox_x,nearest_y-1-minbox_y)=2
					   end if
					    else
					      sugg_voxels(nearest_x+1-minbox_x,nearest_y-1-minbox_y)=2
					    end if
					 end if
				      end if
				   elseif(m.eq.3) then
				      if((nearest_y-minbox_y).gt.1) then
					 if(sugg_voxels(nearest_x-minbox_x,nearest_y-1-minbox_y).ne.2) then
					    if(boundary_label(nearest_x,nearest_y-1).eqv..true.) then
					       if((nearest_y-2).gt.minbox_y) then
						  next_x=nearest_x
						  next_y=nearest_y-1
						  go_tag=.true.
						  sugg_voxels(nearest_x-minbox_x,nearest_y-1-minbox_y)=1
					       else
						  sugg_voxels(nearest_x-minbox_x,nearest_y-1-minbox_y)=2
					       end if
					    else
					       sugg_voxels(nearest_x-minbox_x,nearest_y-1-minbox_y)=2
					    end if
					 end if
				      end if
				   elseif(m.eq.4) then
				      if(((nearest_x-minbox_x).gt.1) .and.((nearest_y-minbox_y).gt.1)) then
					 if(sugg_voxels(nearest_x-1-minbox_x,nearest_y-1-minbox_y).ne.2)then
					    if(boundary_label(nearest_x-1,nearest_y-1).eqv..true.) then
					       if(((nearest_x-2).gt.minbox_x) .and. ((nearest_y-2).gt.minbox_y)) then
						  next_x=nearest_x-1
						  next_y=nearest_y-1
						  go_tag=.true.
						  sugg_voxels(nearest_x-1-minbox_x,nearest_y-1-minbox_y)=1
					     end if
					  else
					     sugg_voxels(nearest_x-1-minbox_x,nearest_y-1-minbox_y)=2
					  end if
					 end if
				      end if
				   elseif(m.eq.5) then
				      if((nearest_x-minbox_x).gt.1) then
					 if(sugg_voxels(nearest_x-1-minbox_x,nearest_y-minbox_y).ne.2)then
					    if(boundary_label(nearest_x-1,nearest_y).eqv..true.) then
					       if((nearest_x-2).gt.minbox_x) then
						  next_x=nearest_x-1
						  next_y=nearest_y
						  go_tag=.true.
						  sugg_voxels(nearest_x-1-minbox_x,nearest_y-minbox_y)=1
					       end if
					    else
					       sugg_voxels(nearest_x-1-minbox_x,nearest_y-minbox_y)=2
					    end if
					 end if
				      end if
				   elseif(m.eq.6) then
				      if(((nearest_x-minbox_x).gt.1) .and.(nearest_y.lt.INT((ybound2-ybound1)/voxel_size)-minbox_y)) then
					 if(sugg_voxels(nearest_x-1-minbox_x,nearest_y+1-minbox_y).ne.2) then
					    if(boundary_label(nearest_x-1,nearest_y+1).eqv..true.) then
					       if(((nearest_x-2).gt.minbox_x) .and. ((nearest_y+2).lt.maxbox_y)) then
						  next_x=nearest_x-1
						  next_y=nearest_y+1
						  go_tag=.true.
						  sugg_voxels(nearest_x-1-minbox_x,nearest_y+1-minbox_y)=1
					       end if
					    else
					       sugg_voxels(nearest_x-1-minbox_x,nearest_y+1-minbox_y)=2
					    end if
					 end if
				      end if
				   elseif(m.eq.7) then
				      if(nearest_y.lt.INT((ybound2-ybound1)/voxel_size)-minbox_y) then
					 if(sugg_voxels(nearest_x-minbox_x,nearest_y+1-minbox_y).ne.2)then
					    if(boundary_label(nearest_x,nearest_y+1).eqv..true.) then
					       if((nearest_y+2).lt.maxbox_y) then
						  next_x=nearest_x
						  next_y=nearest_y+1
						  go_tag=.true.
						  sugg_voxels(nearest_x-minbox_x,nearest_y+1-minbox_y)=1
					       end if
					    else
					       sugg_voxels(nearest_x-minbox_x,nearest_y+1-minbox_y)=2
					    end if
					 end if
				      end if
				   elseif(m.eq.8) then
				      if((nearest_x.lt.INT((xbound2-xbound1)/voxel_size-minbox_x)) .and.&
				      &(nearest_y.lt.INT((ybound2-ybound1)/voxel_size)-minbox_y)) then
					 if(sugg_voxels(nearest_x+1-minbox_x,nearest_y+1-minbox_y).ne.2)then
					    if(boundary_label(nearest_x+1,nearest_y+1).eqv..true.) then
					       if(((nearest_y+2).lt.maxbox_y) .and.((nearest_x+2).lt.maxbox_x)) then
						  next_x=nearest_x+1
						  next_y=nearest_y+1
						  go_tag=.true.
						  sugg_voxels(nearest_x+1-minbox_x,nearest_y+1-minbox_y)=1
					       end if
					    else
					       sugg_voxels(nearest_x+1-minbox_x,nearest_y+1-minbox_y)=2
					    end if
					 end if
				      end if
				   end if
				end do
			     end if
			  else
			     sugg_voxels(nearest_x-minbox_x,nearest_y-minbox_y)=2
			  end if
 79		       continue
		    end if
 16		 continue
		 if(p.eq.x_voxels) GOTO 90 !No more sugg_voxels
 15	      continue
 85	   continue
 90	continue
	

	DEALLOCATE(sugg_voxels)
	
	  do 86 m=1,counter(1)
	     do 87 n=1,counter(2)
!       Use distance squared rather than distance because it is more efficient
		dist_sq=((DBLE(bound_x(1,m))-DBLE(bound_x(2,n)))**2+(DBLE(bound_y(1,m))-DBLE(bound_y(2,n)))**2)
!       convert to actual distance squared
		dist_sq=dist_sq*voxel_size*voxel_size
		if(dist_sq.le.pit_dist_sq) then
		   GOTO 24
		end if
 87	     continue
 86	  continue


 24	continue

	DEALLOCATE(bound_x)
	DEALLOCATE(bound_y)

	END SUBROUTINE pit_boundary

	
 SUBROUTINE fill_in_connections
   USE gcmmn
   IMPLICIT NONE
   INTEGER :: i,j,k,lbegin,lend,l,m,n,start,start2,count
   LOGICAL, ALLOCATABLE :: connection_vector(:)   
   LOGICAL, ALLOCATABLE :: new_connection_vector(:)
   INTEGER :: zedge,counting
   INTEGER :: nodestart,edge,currentseg1
   INTEGER :: currentseg2,firstedge2,secondedge2,edge2
   INTEGER :: firstnode2,secondnode2,s
   DOUBLE PRECISION :: xc1,yc1,zc1,xc2,yc2,zc2,eps
   INTEGER :: q,adv_par,ending,ending2, adv
   INTEGER :: cutoff,firststart,secstart,firstedge1,secondedge1
   INTEGER :: num_points_seg1,num_points_seg2,lastedge1
   INTEGER :: lastedge2,nextseg1,nextseg2,counter
   INTEGER, ALLOCATABLE :: coordstartnew(:), coordendnew(:)
   INTEGER, ALLOCATABLE :: vessel1new(:),vessel2new(:)
   
   ALLOCATE(coordstartnew(2*pit_seg_counter*(10-cutoff_param+1)))
   ALLOCATE(coordendnew(2*pit_seg_counter*(10-cutoff_param+1)))
   ALLOCATE(vessel1new(2*pit_seg_counter*(10-cutoff_param+1)))
   ALLOCATE(vessel2new(2*pit_seg_counter*(10-cutoff_param+1)))
   ALLOCATE(connection_vector(maxpoints))
   ALLOCATE(new_connection_vector(maxpoints))
   
   !       Check connections
   !       how far to advance
   if(maxpoints.le.11) then
      adv_par=maxpoints-1
      cutoff_param=NINT(DBLE(cutoff_param)*(DBLE(adv_par)/10.d0))
   else
      adv_par=10
   end if
   
   eps=1E-7
   counter=1
   counting=0
   
   !       Ensure that the cutoff parameter is at least equal to one (i.e. not every segment is automatically filled in)
   if(cutoff_param.eq.0) cutoff_param=1

   do i=1,number_begin_nodes-1
      do j=i+1,number_begin_nodes
         cutoff=cutoff_param
         adv=adv_par
         new_connection_vector=.false.
         connection_vector=.false.
         do k=1,num_vess_conns
            if(((vessel1(k).eq.i).and.(vessel2(k).eq.j)).or.((vessel2(k).eq.i).and.(vessel1(k).eq.j))) then
               !       find length of connection
               lbegin=NINT(coordstart(k)-zbound1/(sampling_freq*voxel_size))
               lend=NINT(coordend(k)-zbound1/(sampling_freq*voxel_size))
               !       store them in a logical vector
               do l=lbegin,lend
                  connection_vector(l)=.true.
                  counting=counting+1
               end do
            end if
         end do
         
         
!       basically, something about beginning/ending?? this may be okay, actually
         if(vessel_begin(i).gt.vessel_begin(j)) then
            start=vessel_begin(i)+1
         else
            start=vessel_begin(j)+1
         end if
         if(vessel_finish(i).lt.vessel_finish(j)) then
            ending=vessel_finish(i)-1
         else
            ending=vessel_finish(j)-1
         end if
         start=start-minval(vessel_begin)+1
         start2=start
         ending=ending-minval(vessel_begin)+1
         if((ending.gt.1).and.(ending.ge.start2)) then
            do m=FLOOR(DBLE(ending-start)/DBLE(adv))*adv+2,ending-start+2
               if((ending-start2).lt.10.d0) then
                  cutoff=NINT(DBLE(cutoff_param)*(DBLE(ending-start2)/10.d0))
                  adv=ending-start2+1
               else
                  cutoff=cutoff_param
               end if
               
               count=0
               
               do n=start2,start2+adv-1
                  
                  if(connection_vector(n).eqv..true.) then
                     count=count+1
                  end if
               end do
               
               if(count.ge.cutoff_param) then
                  do n=start2,start2+adv-1
                     if(connection_vector(n).eqv..false.) then
                        counting=counting+1
                        new_connection_vector(n)=.true.
                        num_pits_vv(i,j)=num_pits_vv(i,j)+1
                     end if
                  end do
               end if
               start2=start2+adv
            end do
            
            
            !----------------------------------------------------------------
            !       All new connections are defined
            
            !       Define new vessel connections with length
            
            do n=start,ending
               if((new_connection_vector(n).eqv..true.) .or.(connection_vector(n).eqv..true.)) then
                  if(n.eq.start) then
                     coordstartnew(counter)=n
                     coordendnew(counter)=n
                     !       This occurs if the starting and ending points in the connection are the same
                     if(n.eq.ending) then
                        vessel1new(counter)=i
                        vessel2new(counter)=j
                        counter=counter+1
                     end if
                  elseif(n.eq.ending) then
                     if((new_connection_vector(n-1).eqv..false.).and. (connection_vector(n-1).eqv..false.)) then
                        coordstartnew(counter)=n
                     end if
                     
                     coordendnew(counter)=n
                     vessel1new(counter)=i
                     vessel2new(counter)=j 
                     counter=counter+1
                  elseif((new_connection_vector(n-1).eqv..false.).and. (connection_vector(n-1).eqv..false.)) then
                     coordstartnew(counter)=n
                     coordendnew(counter)=n
                  else
                     coordendnew(counter)=n
                  end if
               else
                  if((new_connection_vector(n-1).eqv..true.) .or. (connection_vector(n-1).eqv..true.)) then
                     vessel1new(counter)=i
                     vessel2new(counter)=j 
                     counter=counter+1			  
                  end if
               end if
            end do
            
            
            !       loop through and get all of the connected segments
            
            start=2
            ending=FLOOR((zbound2-zbound1)/(voxel_size*sampling_freq))+1
            
            firststart=2
            secstart=2
            
            do k=start,ending !loop through all segments
               
               nextseg1=0
               nextseg2=0
               
               !       First edge
               if(firststart.eq.2) then
                  zedge=1
                  firstedge1=edge_connectivity1(i)
                  firstedge2=edge_connectivity2(i)
                  num_points_seg1=num_edge_points(i)
                  currentseg1=i
                  lastedge1=vessel_begin(i)-minval(vessel_begin)+1
               else
                  if(k.gt.NINT((z(edge_connectivity2(currentseg1))-zbound1)/(voxel_size*sampling_freq)+1)) then 
                     do l=1,num_segments
                        if((edge_connectivity1(l).eq.firstedge2)) then
                           if((z(edge_connectivity2(l))-z(edge_connectivity1(l)).ge.voxel_size)) then
                              firstedge1=edge_connectivity1(l)
                              firstedge2=edge_connectivity2(l)
                              num_points_seg1=num_edge_points(l)
                              currentseg1=l
                              exit
                           end if
                        end if
                     end do
                  end if
               end if
               firststart=firststart+1
               
               !       Second edge
               if(secstart.eq.2) then
                  zedge=1
                  secondedge1=edge_connectivity1(j)
                  secondedge2=edge_connectivity2(j)
                  num_points_seg2=num_edge_points(j)
                  currentseg2=j
                  lastedge2=vessel_begin(j)-minval(vessel_begin)+1
               else
                  if(k.gt.NINT((z(edge_connectivity2(currentseg2))-zbound1)/(voxel_size*sampling_freq)+1)) then  ! 
                     do l=1,num_segments
                        if((edge_connectivity1(l).eq.secondedge2).and.(z(edge_connectivity2(l))-&
                             &z(edge_connectivity1(l)).ge.voxel_size)) then
                           secondedge1=edge_connectivity1(l)
                           secondedge2=edge_connectivity2(l)
                           num_points_seg2=num_edge_points(l)
                           currentseg2=l
                           exit
                        end if
                     end do
                  end if
               end if
               secstart=secstart+1
               
               if((new_connection_vector(k).eqv..true.) .and. (connection_vector(k).eqv..false.)) then !these are segments that were missing and now are going to be added		   
                  
                  !       Find edge1 that matches the last good node
                  !       z coordinate
                  
                  !       If the initial point is not a pit, add one 
                  xc1=coordx(currentseg1,k-lastedge1+1)
                  xc2=coordx(currentseg2,k-lastedge2+1)
                  yc1=coordy(currentseg1,k-lastedge1+1)
                  yc2=coordy(currentseg2,k-lastedge2+1)
                  zc1=coordz(currentseg1,k-lastedge1+1)
                  zc2=coordz(currentseg2,k-lastedge2+1)
                  
                  
                  
                  !       First, create a segment out of the first vessel if there is not already an end point there
                  
                  if((lastedge1+num_points_seg1-1).ne.k) then
                     x(num_nodes+1)=xc1
                     y(num_nodes+1)=yc1
                     z(num_nodes+1)=zc1
                     if(num_pitted_segs(i).eq.0) then
                        num_pitted_segs(i)=num_pitted_segs(i)+2
                     else
                        num_pitted_segs(i)=num_pitted_segs(i)+1
                     end if
                     
                     pit_node(i,num_pitted_segs(i)-1)=num_nodes+1
                     pit_z(i,num_pitted_segs(i)-1)=zc1
                     
                     edge_connectivity2(num_segments+1)=edge_connectivity2(currentseg1)
                     edge_connectivity2(currentseg1)=num_nodes+1
                     edge_connectivity1(num_segments+1)=num_nodes+1
                     
                     num_nodes=num_nodes+1
                     
                     num_edge_points(num_segments+1)=num_edge_points(currentseg1)+lastedge1-k
                     
                     num_edge_points(currentseg1)=k-lastedge1+1
                     
                     lastedge1=lastedge1+num_edge_points(currentseg1)-1
                     num_points_seg1=num_edge_points(num_segments+1)
                     nextseg1=num_segments+1
                     do q=1,num_edge_points(num_segments+1)
                        coordx(num_segments+1,q)=coordx(currentseg1,q+num_edge_points(currentseg1)-1)
                        coordy(num_segments+1,q)=coordy(currentseg1,q+num_edge_points(currentseg1)-1)
                        coordz(num_segments+1,q)=coordz(currentseg1,q+num_edge_points(currentseg1)-1)
                        radius(num_segments+1,q)=radius(currentseg1,q+num_edge_points(currentseg1)-1)
                     end do
                     if(num_edge_points(num_segments+1).eq.1) then
                        num_edge_points(num_segments+1)=2
                        coordx(num_segments+1,2)=coordx(num_segments+1,1)
                        coordy(num_segments+1,2)=coordy(num_segments+1,1)
                        coordz(num_segments+1,2)=coordz(num_segments+1,1)
                        radius(num_segments+1,2)=radius(num_segments+1,1)
                     end if
                     
                     
                     num_segments=num_segments+1
                  else
                     lastedge1=lastedge1+num_points_seg1-1
                  end if
                  
                  if((lastedge2+num_points_seg2-1).ne.k) then
                     x(num_nodes+1)=xc2
                     y(num_nodes+1)=yc2
                     z(num_nodes+1)=zc2
                     
                     if(num_pitted_segs(j).eq.0) then
                        num_pitted_segs(j)=num_pitted_segs(j)+2
                     else
                        num_pitted_segs(j)=num_pitted_segs(j)+1
                     end if
                     
                     pit_node(j,num_pitted_segs(j)-1)=num_nodes+1
                     pit_z(j,num_pitted_segs(j)-1)=zc2
                     
                     edge_connectivity2(num_segments+1)=edge_connectivity2(currentseg2)
                     edge_connectivity2(currentseg2)=num_nodes+1
                     edge_connectivity1(num_segments+1)=num_nodes+1
                     
                     num_nodes=num_nodes+1
                     
                     num_edge_points(num_segments+1)=num_edge_points(currentseg2)+lastedge2-k
                     
                     num_edge_points(currentseg2)=k-lastedge2+1
                     
                     lastedge2=lastedge2+num_edge_points(currentseg2)-1
                     num_points_seg2=num_edge_points(num_segments+1)
                     nextseg2=num_segments+1
                     do q=1,num_edge_points(num_segments+1)
                        coordx(num_segments+1,q)=coordx(currentseg2,q+num_edge_points(currentseg2)-1)
                        coordy(num_segments+1,q)=coordy(currentseg2,q+num_edge_points(currentseg2)-1)
                        coordz(num_segments+1,q)=coordz(currentseg2,q+num_edge_points(currentseg2)-1)
                        radius(num_segments+1,q)=radius(currentseg2,q+num_edge_points(currentseg2)-1)
                     end do
                     if(num_edge_points(num_segments+1).eq.1) then
                        num_edge_points(num_segments+1)=2
                        coordx(num_segments+1,2)=coordx(num_segments+1,1)
                        coordy(num_segments+1,2)=coordy(num_segments+1,1)
                        coordz(num_segments+1,2)=coordz(num_segments+1,1)
                        radius(num_segments+1,2)=radius(num_segments+1,1)
                     end if
                     
                     num_segments=num_segments+1
                  else
                     lastedge2=lastedge2+num_points_seg2-1
                  end if
                  
                  
                  !       Lastly, create a segment between the two vessels
                  edge_connectivity1(num_segments+1)=edge_connectivity2(currentseg1)
                  edge_connectivity2(num_segments+1)=edge_connectivity2(currentseg2)
                  coordx(num_segments+1,1)=coordx(currentseg1,num_edge_points(currentseg1))
                  coordy(num_segments+1,1)=coordy(currentseg1,num_edge_points(currentseg1))
                  coordz(num_segments+1,1)=coordz(currentseg1,num_edge_points(currentseg1))
                  radius(num_segments+1,1)=radius(currentseg1,num_edge_points(currentseg1))
                  coordx(num_segments+1,2)=coordx(currentseg2,num_edge_points(currentseg2))
                  coordy(num_segments+1,2)=coordy(currentseg2,num_edge_points(currentseg2))
                  coordz(num_segments+1,2)=coordz(currentseg2,num_edge_points(currentseg2))
                  radius(num_segments+1,2)=radius(currentseg2,num_edge_points(currentseg2))
                  
                  
                  num_edge_points(num_segments+1)=2
                  num_segments=num_segments+1
                  if(nextseg1.ne.0) then
                     currentseg1=nextseg1
                  end if
                  if(nextseg2.ne.0) then
                     currentseg2=nextseg2
                  end if
                  
                  !       Change current segments for next step
                  !       Lastly, create a segment between the two vessels
                  
                  firstedge2=edge_connectivity2(currentseg1)
                  
                  
                  if((num_segments.gt.max_segments).or.(num_segments.gt.hardLim)) then
                     PRINT *,'Pitting Error: Maximum number of segments exceeded. Please crop the sample to a smaller volume.'
                     PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments 
                    pause
                  end if
                  
               elseif((new_connection_vector(k).eqv..false.) .and. (connection_vector(k).eqv..true.)) then 
                  lastedge1=k
                  lastedge2=k
               else
                  !       Add 2 instead of 1 to anticipate next step
                  if((k-lastedge1+1).ge.num_points_seg1) then
                     lastedge1=k
                  end if
                  if((k-lastedge2+1).ge.num_points_seg2) then
                     lastedge2=k
                  end if
                  
               end if
79             continue
               

            end do
            
            firstedge2=z(edge_connectivity2(i))/(voxel_size*sampling_freq)-vessel_begin(i)
            secondedge2=z(edge_connectivity2(j))/(voxel_size*sampling_freq)-vessel_begin(j)
            
            firstnode2=edge_connectivity2(i)
            secondnode2=edge_connectivity2(j)
            
         end if
      end do
   end do
 
   num_vess_conns=counter-1
   !       Reassign coordstart, coordend, etc.
   do i=1,num_vess_conns
      coordstart(i)=coordstartnew(i)
      coordend(i)=coordendnew(i)
      vessel1(i)=vessel1new(i)
      vessel2(i)=vessel2new(i)
   end do
   
   DEALLOCATE(connection_vector)
   DEALLOCATE(new_connection_vector)
   DEALLOCATE(vessel1new)
   DEALLOCATE(vessel2new)
   DEALLOCATE(coordstartnew)
   DEALLOCATE(coordendnew)	
 END SUBROUTINE fill_in_connections
 


 SUBROUTINE remove_short_connections
   USE gcmmn
   IMPLICIT NONE
   INTEGER :: i,j,k,p,q,node1,node2,edgecount,t
   INTEGER :: lowedge, highedge,count,counter
   INTEGER, ALLOCATABLE :: pit_node_new(:,:)
   
   
   p=num_vess_conns
   
   !       Remove connections that don't meet the minimum criterion
   do i=1,p
      if((coordend(i)-coordstart(i)+1)*voxel_size*sampling_freq.lt.min_conn_length) then
         num_vess_conns=num_vess_conns-1
         !       need to eliminate everything
         !       find pit connections
         
         do t=1,coordend(i)-coordstart(i)+1
            !       first vessel
            do j=1,num_pitted_segs(vessel1(i))-1
               if(ABS(pit_z(vessel1(i),j)-voxel_size*sampling_freq*(coordstart(i)+t-2)).lt.(voxel_size*0.5))then
                  node1=pit_node(vessel1(i),j)
                  exit
               end if
            end do
            
            !       second vessel
            do j=1,num_pitted_segs(vessel2(i))-1
               if(ABS(pit_z(vessel2(i),j)-voxel_size*sampling_freq*(coordstart(i)+t-2)).lt.(voxel_size*0.5))then
                  node2=pit_node(vessel2(i),j)
                  exit
               end if
            end do
            
            !       Find v-v connection and eliminate it
            do j=1,num_segments
               if((edge_connectivity1(j).eq.node1).and.(edge_connectivity2(j).eq.node2)) then
                  edge_connectivity1(j)=-1
                  edge_connectivity2(j)=-1
                  num_pits_vv(vessel1(i),vessel2(i))=num_pits_vv(vessel1(i),vessel2(i))-1
                  
                  if(num_pits_vv(vessel1(i),vessel2(i)).eq.0) then
                     num_vv_connections(vessel1(i))=num_vv_connections(vessel1(i))-1
                     num_vv_connections(vessel2(i))=num_vv_connections(vessel2(i))-1
                  end if
                  num_pit_connections(vessel1(i))=num_pit_connections(vessel1(i))-1
                  num_pit_connections(vessel2(i))=num_pit_connections(vessel2(i))-1
               end if
            end do
            
            
            !       Find other vessels connected to these nodes
            !       node 1
            edgecount=0
            do j=1,num_segments
               if((edge_connectivity1(j).eq.node1)) then
                  highedge=j
                  edgecount=edgecount+1
               elseif(edge_connectivity2(j).eq.node1) then
                  lowedge=j
                  edgecount=edgecount+1
               end if
            end do
            !       eliminate higher segment associated with node
            if(edgecount.eq.2) then
               
               do k=1,num_edge_points(highedge)-1
                  coordx(lowedge,k+num_edge_points(lowedge))=coordx(highedge,k+1)
                  coordy(lowedge,k+num_edge_points(lowedge))=coordy(highedge,k+1)
                  coordz(lowedge,k+num_edge_points(lowedge))=coordz(highedge,k+1)
                  radius(lowedge,k+num_edge_points(lowedge))=radius(highedge,k+1)
               end do
               num_edge_points(lowedge)=num_edge_points(lowedge)+num_edge_points(highedge)-1
               edge_connectivity2(lowedge)=edge_connectivity2(highedge)
               edge_connectivity1(highedge)=-1
               edge_connectivity2(highedge)=-1
               do j=1,num_pitted_segs(vessel1(i))-1
                  if(ABS(pit_z(vessel1(i),j)-voxel_size*sampling_freq*(coordstart(i)+t-2)).lt.(voxel_size*0.5))then
                     pit_node(vessel1(i),j)=-1
                     exit
                  end if
               end do
            end if
            
            !       node 2
            edgecount=0
            do j=1,num_segments
               if((edge_connectivity1(j).eq.node2)) then
                  highedge=j
                  edgecount=edgecount+1
               elseif(edge_connectivity2(j).eq.node2) then
                  lowedge=j
                  edgecount=edgecount+1
               end if
            end do
            !       eliminate higher segment associated with node
            if(edgecount.eq.2) then
               
               do k=1,num_edge_points(highedge)-1
                  coordx(lowedge,k+num_edge_points(lowedge))=coordx(highedge,k+1)
                  coordy(lowedge,k+num_edge_points(lowedge))=coordy(highedge,k+1)
                  coordz(lowedge,k+num_edge_points(lowedge))=coordz(highedge,k+1)
                  radius(lowedge,k+num_edge_points(lowedge))=radius(highedge,k+1)
               end do
               num_edge_points(lowedge)=num_edge_points(lowedge)+num_edge_points(highedge)-1
               edge_connectivity2(lowedge)=edge_connectivity2(highedge)
               edge_connectivity1(highedge)=-1
               edge_connectivity2(highedge)=-1
               do j=1,num_pitted_segs(vessel2(i))-1
                  if(ABS(pit_z(vessel2(i),j)-voxel_size*sampling_freq*(coordstart(i)+t-2)).lt.(voxel_size*0.5))then
                     pit_node(vessel2(i),j)=-1
                     exit
                  end if
               end do
            end if
         end do
         !       Set equal to -1  eliminate later
         coordstart(i)=-1
         coordend(i)=-1
      end if
   end do
   
   count=1
   !       reassign all of the coordstarts/coordends
   do i=1,p
      if(coordstart(i).ne.-1) then
         coordstart(count)=coordstart(i)
         coordend(count)=coordend(i)
         vessel1(count)=vessel1(i)
         vessel2(count)=vessel2(i)
         count=count+1
      end if
   end do
   
   ALLOCATE(pit_node_new(num_segments,max_sampled_points_per_seg))
   
   !       eliminate pit_nodes that no longer exist
   do i=1,number_begin_nodes
      counter=1
      q=num_pitted_segs(i)-1
      num_pitted_segs(i)=0
      do j=1,q
         if(pit_node(i,j).ne.-1) then
            pit_node_new(i,counter)=pit_node(i,j)
            num_pitted_segs(i)=num_pitted_segs(i)+1
            counter=counter+1
         end if
      end do
      if(num_pitted_segs(i).gt.0) num_pitted_segs(i)=num_pitted_segs(i)+1
   end do
   
   !       transfer back to original variable
   pit_node=0
   do i=1,number_begin_nodes
      do j=1,num_pitted_segs(i)-1
         pit_node(i,j)=pit_node_new(i,j)
      end do
   end do
   
   DEALLOCATE(pit_node_new)
   
   q=1
   !       eliminate vertices if some no longer exist
   do 89 j=1,num_nodes
      do i=1,num_segments
         if((edge_connectivity1(i).eq.j).or.(edge_connectivity2(i).eq.j)) then
            x(q)=x(j)
            y(q)=y(j)
            z(q)=z(j)
            !       move edges
            do k=1,num_segments
               if(edge_connectivity1(k).eq.j) then
                  edge_connectivity1(k)=q
               end if
               if(edge_connectivity2(k).eq.j) then
                  edge_connectivity2(k)=q
               end if
            end do
            q=q+1
            GOTO 89 
         end if
         
      end do
89    continue
      
      p=1
      !       reassign everything where the edges are not -1
      do j=1,num_segments
         if(edge_connectivity1(j).ne.(-1))then
            edge_connectivity1(p)=edge_connectivity1(j)
            edge_connectivity2(p)=edge_connectivity2(j)
            num_edge_points(p)=num_edge_points(j)
            do k=1,num_edge_points(j)
               coordx(p,k)=coordx(j,k)
               coordy(p,k)=coordy(j,k)
               coordz(p,k)=coordz(j,k)
               radius(p,k)=radius(j,k)
            end do
            p=p+1
         end if
      end do
      
      num_segments=p-1
      num_nodes=q-1
      
      
    END SUBROUTINE remove_short_connections





    SUBROUTINE model_pitfield
      USE gcmmn
      IMPLICIT NONE
      INTEGER :: i,l,n,num_bridges,num_connections
      INTEGER, ALLOCATABLE :: edge1new(:), edge2new(:),num_edge_points_new(:)
      character (len=20):: vertex
      character (len=18):: edge
      character (len=19):: points
      character (len=6):: vertex2,edge2
      LOGICAL, ALLOCATABLE :: pit_now(:),vess_conn_tag(:)
      INTEGER :: j,k,u,v,last_edge,node_counter,node2,currentseg
      DOUBLE PRECISION :: z_last_edge,xc1,xc2,yc1,yc2,zc1,zc2
      INTEGER :: p,s,r,q,pit_node_counter,vess_count
      INTEGER :: point_store,num_points_seg,m,edgeptcounter
      DOUBLE PRECISION :: point_thick,avgradius1,avgradius2
      DOUBLE PRECISION :: ax,ay,bx,by,cx,cy,aseg,bseg,cseg
      DOUBLE PRECISION :: pit_connection_angle
      DOUBLE PRECISION :: dist_center
      INTEGER, ALLOCATABLE :: pit_number(:),pit_counter(:)
      LOGICAL, ALLOCATABLE :: node_tag(:)
      LOGICAL :: node_remove
      LOGICAL, ALLOCATABLE :: remove_tag(:)
      INTEGER, ALLOCATABLE :: node_count(:)
      INTEGER :: num_isolated, num_oneconnect,num_twoplus
      DOUBLE PRECISION :: avg_pitarea,total_pitarea,stdev_pitarea
      DOUBLE PRECISION :: deviation_sq
      DOUBLE PRECISION :: avg_pitarea_bridges,total_pitarea_bridges,stdev_pitarea_bridges
      DOUBLE PRECISION :: deviation_sq_bridges,sum_diameters,avg_diameter,stdev
      INTEGER :: num_vess_bridge_conns,smin,count,nextseg
      INTEGER, ALLOCATABLE :: connect_number(:),connect(:,:)
      LOGICAL :: connected
      LOGICAL, ALLOCATABLE :: seg_tag(:) 
      
!       Read points from avizo_test file (i.e. after sampling subroutine) to get all of the base segments 
	open(unit=12,file='avizo_test.am',status='old')
   
!       Read intro part of file
      
	do i=1,3
	   read(12,*)
	end do
	read(12,"(A20)") vertex
	read(12,"(A18)") edge
	
	do i=1,14
	   read(12,*)
	end do
	
	vertex2=vertex(15:20)
	edge2=edge(13:18)
	
	read(vertex2,*) num_nodes
	read(edge2,*) num_segments
	
	
!       Coordinates of vertices
	do 12 i=1,num_nodes
	   read(12,*,iostat=k) x(i),y(i),z(i)

!       convert units to meters
	   x(i)=x(i)*convert_factor
	   y(i)=y(i)*convert_factor
	   z(i)=z(i)*convert_factor
 12	continue
	read(12,*)
	read(12,*)
	
!       Connections between vertices
	do 13 i=1,num_segments
	   read(12,*,iostat=k) edge_connectivity1(i),edge_connectivity2(i)
	   IF(k>0) exit
!       Add one because the export format starts with index 0
	   edge_connectivity1(i)=edge_connectivity1(i)+1
	   edge_connectivity2(i)=edge_connectivity2(i)+1
 13	continue
      
	read(12,*)
	read(12,*)
	
	num_edge_points=0
	
!       Number of edge points between the nodes of each segment
	do 14 i=1,num_segments
	   read(12,*) num_edge_points(i)
 14	continue
      
	read(12,*)
	read(12,*)
      
      
!       Initialize variables
	coordx=0
	coordy=0
	coordz=0
	radius=0
      
      
!       Edge point coordinates
	do 15 i=1,num_segments
	   do 51 j=1,num_edge_points(i)
	      read(12,*) coordx(i,j),coordy(i,j),coordz(i,j)
!       convert to meters
	      coordx(i,j)=coordx(i,j)*convert_factor
	      coordy(i,j)=coordy(i,j)*convert_factor
	      coordz(i,j)=coordz(i,j)*convert_factor
 51	   continue
 15	continue
	read(12,*)
	read(12,*)
      
!       Thickness (i.e. radius) of vessel at each edge point	
	do 71 i=1,num_segments
	   do 61 j=1,num_edge_points(i)
	      read(12,*) point_thick
!       convert units to meters
	      radius(i,j)=point_thick*convert_factor
 61	   continue
 71	continue
	close(12)

	ALLOCATE(pit_now(number_begin_nodes))
	ALLOCATE(pit_number(number_begin_nodes))
	ALLOCATE(pit_counter(number_begin_nodes))
	ALLOCATE(pit_no(num_vess_conns))
	ALLOCATE(num_pitted_segs(number_begin_nodes))
	ALLOCATE(num_pitted_segs_new(number_begin_nodes))
	ALLOCATE(pit_z(number_begin_nodes,max_sampled_points_per_seg))
	ALLOCATE(pit_node(number_begin_nodes,max_sampled_points_per_seg))
	ALLOCATE(num_pit_connections(number_begin_nodes))
	ALLOCATE(vess_conn_tag(num_vess_conns))


	num_pitted_segs=0
	pit_seg_counter=0
	pit_z=0
	pit_number=0
 do i=1,number_begin_nodes
    do j=1,max_sampled_points_per_seg
       pit_node(i,j)=0
    end do
 end do
 num_pit_connections=0
 
 smin=FLOOR(zbound1/(sampling_freq*voxel_size))+2
 maxpoints=maxval(num_edge_points)-1
 do 42 s=smin,smin+maxpoints-2
    pit_now=.false.
    do i=1,number_begin_nodes-1
       if((s.gt.vessel_begin(i)).and.(s.lt.vessel_finish(i))) then
          do j=i+1,number_begin_nodes
             if((s.gt.vessel_begin(j)).and.(s.lt.vessel_finish(j))) then
                xc1=coordx(i,s-vessel_begin(i)+1)
                xc2=coordx(j,s-vessel_begin(j)+1)
                yc1=coordy(i,s-vessel_begin(i)+1)
                yc2=coordy(j,s-vessel_begin(j)+1)
                zc1=coordz(i,s-vessel_begin(i)+1)
                zc2=coordz(j,s-vessel_begin(j)+1)
                
                do k=1,num_vess_conns 
                   if((vessel1(k).eq.i).and.(vessel2(k).eq.j).and.((NINT(0.5*(coordend(k)-coordstart(k))+coordstart(k))).eq.s)) then
                      if(pit_now(i).eqv..false.) then
                         pit_now(i)=.true.
                         pit_number(i)=num_nodes+1
                         x(num_nodes+1)=xc1
                         y(num_nodes+1)=yc1
                         z(num_nodes+1)=zc1
                         if(num_pitted_segs(i).eq.0) then
                            num_pitted_segs(i)=num_pitted_segs(i)+2
                         else
                            num_pitted_segs(i)=num_pitted_segs(i)+1
                         end if
                         pit_z(i,num_pitted_segs(i)-1)=z(num_nodes+1)
                         pit_node(i,num_pitted_segs(i)-1)=num_nodes+1
                         edge_connectivity1(num_segments+1)=num_nodes+1
                         num_nodes=num_nodes+1
                         pit_node_counter=pit_node_counter+1
                      else
                         edge_connectivity1(num_segments+1)=pit_number(i)
                      end if
                      if(pit_now(j).eqv..false.) then
                         pit_now(j)=.true.
                         pit_number(j)=num_nodes+1
                         x(num_nodes+1)=xc2	
                         y(num_nodes+1)=yc2
                         z(num_nodes+1)=zc2
                         
                         if(num_pitted_segs(j).eq.0) then
                            num_pitted_segs(j)=num_pitted_segs(j)+2
                         else
                            num_pitted_segs(j)=num_pitted_segs(j)+1
                         end if
                         
                         !       Label new pit nodes and z coordinates of pit nodes
                         pit_z(j,num_pitted_segs(j)-1)=z(num_nodes+1)
                         pit_node(j,num_pitted_segs(j)-1)=num_nodes+1
                         edge_connectivity2(num_segments+1)=num_nodes+1
                         num_nodes=num_nodes+1
                         pit_node_counter=pit_node_counter+1
                      else
                         edge_connectivity2(num_segments+1)=pit_number(j)
                      end if
                      
                      num_pit_connections(i)=num_pit_connections(i)+1
                      num_pit_connections(j)=num_pit_connections(j)+1
                      
                      num_segments=num_segments+1
                      pit_seg_counter=pit_seg_counter+1
                      
                      if((num_segments.ge.max_segments) .or. (num_segments.gt.hardLim)) then
                         PRINT *,'Pitting Error: Maximum number of segments exceeded'
                         PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
                      end if
                      num_edge_points(num_segments)=2
                      coordx(num_segments,1)=xc1
                      coordy(num_segments,1)=yc1
                      coordz(num_segments,1)=zc1
                      coordx(num_segments,2)=xc2
                      coordy(num_segments,2)=yc2
                      coordz(num_segments,2)=zc2
                      
                      pit_no(k)=num_segments
                      
                      avgradius1=0
                      avgradius2=0
                      
                      do m=coordstart(k),coordend(k)
                         avgradius1=avgradius1+radius(i,m-coordstart(k)+1)
                         avgradius2=avgradius2+radius(j,m-coordstart(k)+1)
                      end do
                      
                      avgradius1=avgradius1/(coordend(k)-coordstart(k)+1)
                      avgradius2=avgradius2/(coordend(k)-coordstart(k)+1)
                      
                      if(avgradius1.gt.avgradius2) then
                         radius(num_segments,1)=avgradius2
                         radius(num_segments,2)=avgradius2
                      else
                         radius(num_segments,1)=avgradius1
                         radius(num_segments,2)=avgradius1
                      end if
                   end if
                end do
             end if
          end do
       end if
    end do
42  continue
    
    !       Add pits and do analysis on them
    r=num_segments-pit_seg_counter
    !       pit_z gives you z coordinate of pit so that you can determine the no. of edge points
    
    do 48 j=1,r
       z_last_edge=z(edge_connectivity2(j))
       last_edge=edge_connectivity2(j)
       num_points_seg=num_edge_points(j)
       vess_count=1
       
       if(num_pitted_segs(j).eq.2) then
          edge_connectivity1(num_segments+1)=pit_node(j,1)
          edge_connectivity2(num_segments+1)=edge_connectivity2(j)
          edge_connectivity2(j)=pit_node(j,1)
          
          do m=1,num_points_seg
             if(pit_z(j,1).eq.coordz(j,m)) then
                num_edge_points(num_segments+1)=num_points_seg-m+1
                num_edge_points(j)=m
                exit
             end if
          end do
          
          do q=1,num_edge_points(num_segments+1)
             coordx(num_segments+1,q)=coordx(j,q+num_edge_points(j)-1)
             coordy(num_segments+1,q)=coordy(j,q+num_edge_points(j)-1)
             coordz(num_segments+1,q)=coordz(j,q+num_edge_points(j)-1)
             radius(num_segments+1,q)=radius(j,q+num_edge_points(j)-1)
          end do
          
          num_segments=num_segments+1
          if((num_segments.gt.max_segments) .or. (num_segments.gt. hardLim)) then
             PRINT *,'Pitting Error: Maximum number of segments exceeded'
             PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
          end if
       elseif(num_pitted_segs(j).gt.2) then
          edgeptcounter=0
          point_store=0
          
          do 86 k=1,num_pitted_segs(j)
             vess_count=vess_count+1
             
             if(k.eq.num_pitted_segs(j)) then
                edge_connectivity2(num_segments+1)=last_edge
                
                num_edge_points(num_segments+1)=num_points_seg-edgeptcounter+1
                
                do q=1,num_edge_points(num_segments+1)
                   coordx(num_segments+1,q)=coordx(j,q+point_store)
                   coordy(num_segments+1,q)=coordy(j,q+point_store)
                   coordz(num_segments+1,q)=coordz(j,q+point_store)
                   radius(num_segments+1,q)=radius(j,q+point_store)
                end do
                
                if(num_edge_points(num_segments+1).eq.1) then
                   num_edge_points(num_segments+1)=2
                   coordx(num_segments+1,2)=coordx(num_segments+1,1)
                   coordy(num_segments+1,2)=coordy(num_segments+1,1)
                   coordz(num_segments+1,2)=coordz(num_segments+1,1)
                   radius(num_segments+1,2)=radius(num_segments+1,1)
                end if
                
                num_segments=num_segments+1
                if((num_segments.gt.max_segments).or.(num_segments.gt.hardLim)) then
                   PRINT *,'Pitting Error: Maximum number of segments exceeded'
                   PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
                end if
                
             else
                if(k.eq.1) then
                   edge_connectivity2(j)=pit_node(j,k)
                   edge_connectivity1(num_segments+1)=pit_node(j,k)
                   
                   do m=1,num_points_seg
                      if(pit_z(j,k).eq.coordz(j,m)) then
                         num_edge_points(j)=m
                         edgeptcounter=num_edge_points(j)
                         exit
                      end if
                   end do
                   point_store=num_edge_points(j)-1
                   
                else
                   do m=1,num_points_seg
                      if(pit_z(j,k).eq.coordz(j,m)) then
                         num_edge_points(num_segments+1)=m-edgeptcounter+1
                         edgeptcounter=num_edge_points(num_segments+1)+edgeptcounter-1	  
                         exit
                      end if
                   end do
                   
                   
                   edge_connectivity2(num_segments+1)=pit_node(j,k)
                   edge_connectivity1(num_segments+2)=pit_node(j,k)
                   
                   do q=1,num_edge_points(num_segments+1)
                      coordx(num_segments+1,q)=coordx(j,q+point_store)
                      coordy(num_segments+1,q)=coordy(j,q+point_store)
                      coordz(num_segments+1,q)=coordz(j,q+point_store)
                      radius(num_segments+1,q)=radius(j,q+point_store)
                   end do
                   if(num_edge_points(num_segments).eq.1) then
                      num_edge_points(num_segments+1)=2
                      coordx(num_segments+1,2)=coordx(num_segments+1,1)
                      coordy(num_segments+1,2)=coordy(num_segments+1,1)
                      coordz(num_segments+1,2)=coordz(num_segments+1,1)
                      radius(num_segments+1,2)=radius(num_segments+1,1)
                   end if
                   point_store=num_edge_points(num_segments+1)+point_store-1
                   num_segments=num_segments+1
                   if((num_segments.gt.max_segments).or.(num_segments.gt.hardLim)) then
                      PRINT *,'Pitting Error: Maximum number of segments exceeded'
                      PRINT *,'Number of segments: ',num_segments,'Maximum number of segments: ',max_segments
                   end if
                end if
                
             end if
86           continue
             
          end if
48        continue
          
          
          !	open(unit=85,file='output-vv_connections.txt',status='unknown')
          
          !	write(85,*) 'Vessel        Number of intervessel connections'
          !	do i=1,num_backbone_segs
          !	   write(85,'(I3,12x,I3)') i, num_pit_connections(i)
          !	end do
          !	close(85)
          
          DEALLOCATE(pit_now)
          DEALLOCATE(pit_number)
          DEALLOCATE(pit_z)
          
          if(bridge_tag.eq.1) then
             ALLOCATE(node_tag(num_nodes))
             node_tag=.true.
             
             
             !       Remove all of the bridge end nodes
	   do i=num_backbone_segs+1,number_begin_nodes
	      if(num_pitted_segs(i).gt.0) then
		 do j=1,num_segments
		    if((edge_connectivity2(j).eq.pit_node(i,1)).and.((z(edge_connectivity2(j))-z(edge_connectivity1(j))).gt.0.5*voxel_size)) then
		       node_tag(edge_connectivity1(j))=.false.
		    end if
		    if((edge_connectivity1(j).eq.pit_node(i,num_pitted_segs(i)-1)).and.&
		    &((z(edge_connectivity2(j))-z(edge_connectivity1(j))).gt.0.5*voxel_size)) then
		       node_tag(edge_connectivity2(j))=.false.
		    end if
		 end do
	      else
		 node_tag(edge_connectivity1(i))=.false.
		 node_tag(edge_connectivity2(i))=.false.
	      end if
	   end do
	   
	   p=1
	   do j=1,num_nodes


	      if(node_tag(j).eqv..true.) then
		 x(p)=x(j)
		 y(p)=y(j)
		 z(p)=z(j)
       

		 do k=1,number_begin_nodes
		    do l=1,num_pitted_segs(k)-1
		       if(pit_node(k,l).eq.j) then
			  pit_node(k,l)=p
		       end if
		    end do
		 end do
		 do 72 k=1,num_segments
		    if(edge_connectivity1(k).eq.j) then
		       edge_connectivity1(k)=p
		    end if
		    if(edge_connectivity2(k).eq.j) then
		       edge_connectivity2(k)=p
		    end if	
 72		 continue
		 p=p+1

	      else
		 do 23 i=1,num_segments
		    if(edge_connectivity1(i).eq.j) then
		       edge_connectivity1(i)=-1
		    end if
		    if(edge_connectivity2(i).eq.j) then
		       edge_connectivity2(i)=-1
		    end if
 23		 continue

	      end if
	   end do

           q=1
	   do 25 j=1,num_segments
	      if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
		 edge_connectivity1(q)=edge_connectivity1(j)
		 edge_connectivity2(q)=edge_connectivity2(j)
  		 num_edge_points(q)=num_edge_points(j)
                 do l=1,num_vess_conns
		    if(pit_no(l).eq.j) then
                       pit_no(l)=q	
		    end if
		 end do
		 do m=1,num_edge_points(j)
		      coordx(q,m)=coordx(j,m)
		      coordy(q,m)=coordy(j,m)
		      coordz(q,m)=coordz(j,m)
		      radius(q,m)=radius(j,m)
		 end do
		 q=q+1
	      end if
 25	   continue
	   num_nodes=p-1
	   num_segments=q-1

    q=number_begin_nodes

    ALLOCATE(remove_tag(number_begin_nodes))
    remove_tag=.false.
    do i=num_backbone_segs+1,q
       if(num_pitted_segs(i).eq.0) then
          remove_tag(i)=.true.
       end if
    end do
    count=0

    do k=num_backbone_segs+1,q
       if(remove_tag(k).eqv..true.) then
          do j=k-count,q-1
             num_vv_connections(j)=num_vv_connections(j+1)
             num_pit_connections(j)=num_pit_connections(j+1)
             v_diam(j)=v_diam(j+1)
             num_pits_vv(j,:)=num_pits_vv(j+1,:)
             num_pits_vv(:,j)=num_pits_vv(:,j+1)
             num_pitted_segs(j)=num_pitted_segs(j+1)
             pit_node(j,:)=pit_node(j+1,:)
          end do
          do j=1,num_vess_conns
             if(vessel1(j).gt.(k-count))  vessel1(j)=vessel1(j)-1
             if(vessel2(j).gt.(k-count))  vessel2(j)=vessel2(j)-1            
          end do
          number_begin_nodes=number_begin_nodes-1
          r=r-1
          count=count+1
       end if
    end do

    DEALLOCATE(remove_tag)

    u=number_begin_nodes
    vess_conn_tag=.true.
    !       Next, remove any bridge nodes that don't connect backbone_nodes
    do 18 n=1,1000	!Repeat until no additional nodes are lost
       node_remove=.false.
       node_tag=.true.
       do i=num_backbone_segs+1,u
          if(num_pitted_segs(i).gt.0) then
             v=num_pitted_segs(i)
             do j=1,v-1
                node_counter=0
                do 20 k=1,num_segments
                   if(((edge_connectivity1(k).eq.pit_node(i,j)) .or. (edge_connectivity2(k).eq.pit_node(i,j)))) then
                      node_counter=node_counter+1
                   end if
20              continue
                   
                   if(node_counter.eq.1) then
                      node_tag(pit_node(i,j))=.false.
                      node_remove=.true.
                      num_pit_connections(i)=num_pit_connections(i)-1
                      do k=1,num_segments
                         if(edge_connectivity1(k).eq.pit_node(i,j)) then
                            do l=1,number_begin_nodes
                               do m=1,max_sampled_points_per_seg
                                  if(pit_node(l,m).eq.edge_connectivity2(k)) then
                                     num_pit_connections(l)=num_pit_connections(l)-1
                                     GOTO 57
                                  end if
                               end do
                            end do
                         elseif(edge_connectivity2(k).eq.pit_node(i,j)) then
                            do l=1,number_begin_nodes
                               do m=1,max_sampled_points_per_seg
                                  if(pit_node(l,m).eq.edge_connectivity1(k)) then
                                     num_pit_connections(l)=num_pit_connections(l)-1
                                     GOTO 57
                                  end if
                               end do
                            end do
                         end if
                      end do

57                    continue

                      if(num_pitted_segs(i).eq.2) then
                         do k=1,num_vess_conns
                            !       removal of bridges
                            if((vessel1(k).eq.i).and.(vess_conn_tag(k).eqv..true.)) then
                               vess_conn_tag(k)=.false.
                               num_vv_connections(vessel2(k))=num_vv_connections(vessel2(k))-1
                            elseif((vessel2(k).eq.i).and.(vess_conn_tag(k).eqv..true.))then
                               vess_conn_tag(k)=.false.
                               num_vv_connections(vessel1(k))=num_vv_connections(vessel1(k))-1
                            end if
                         end do
                         do k=1,num_vess_conns
                            if(vessel1(k).gt.i)  vessel1(k)=vessel1(k)-1
                            if(vessel2(k).gt.i)  vessel2(k)=vessel2(k)-1            
                         end do
                      else
                         do k=j,num_pitted_segs(i)-2
                            pit_node(i,k)=pit_node(i,k+1)
                         end do
                      end if

                      p=1
                      do k=1,num_nodes
                         if(node_tag(k).eqv..true.) then
                            x(p)=x(k)
                            y(p)=y(k)
                            z(p)=z(k)

                            !set node equal to true for the next round
                            node_tag(p)=.true.
                               
                            do m=1,number_begin_nodes
                               do l=1,num_pitted_segs(m)-1
                                  if(pit_node(m,l).eq.k) then
                                     pit_node(m,l)=p
                                  end if
                               end do
                            end do
                            do 32 m=1,num_segments
                               if(edge_connectivity1(m).eq.k) then
                                  edge_connectivity1(m)=p
                               end if
                               if(edge_connectivity2(m).eq.k) then
                                  edge_connectivity2(m)=p
                               end if
32                             continue
                               p=p+1
                            else
                               do 33 m=1,num_segments
                                  if(edge_connectivity1(m).eq.k) then
                                     edge_connectivity1(m)=-1
                                  end if
                                  if(edge_connectivity2(m).eq.k) then
                                     edge_connectivity2(m)=-1
                                  end if
33                                continue
                               end if
                            end do
                               
                            q=1
                            do 35 k=1,num_segments
                               if((edge_connectivity1(k).ne.-1).and.(edge_connectivity2(k).ne.-1)) then
                                  edge_connectivity1(q)=edge_connectivity1(k)
                                  edge_connectivity2(q)=edge_connectivity2(k)
                                  num_edge_points(q)=num_edge_points(k)
                                  
                                  do l=1,num_vess_conns
                                     if(pit_no(l).eq.k) then
                                        pit_no(l)=q	
                                     end if
                                  end do
                                  do m=1,num_edge_points(k)
                                     coordx(q,m)=coordx(k,m)
                                     coordy(q,m)=coordy(k,m)
                                     coordz(q,m)=coordz(k,m)
                                     radius(q,m)=radius(k,m)
                                  end do
                                  q=q+1
                               end if
35                             continue
                                  
                               num_nodes=p-1
                               num_segments=q-1

                               if(num_pitted_segs(i).eq.2) then
                                  do k=i,u-1
                                     num_vv_connections(k)=num_vv_connections(k+1)
                                     num_pit_connections(k)=num_pit_connections(k+1)
                                     v_diam(k)=v_diam(k+1)
                                     num_pits_vv(k,:)=num_pits_vv(k+1,:)
                                     num_pits_vv(:,k)=num_pits_vv(:,k+1)
                                     num_pitted_segs(k)=num_pitted_segs(k+1)
                                     pit_node(k,:)=pit_node(k+1,:)
                                  end do
                                  number_begin_nodes=number_begin_nodes-1
                                  r=r-1
                               else
                                  num_pitted_segs(i)=num_pitted_segs(i)-1
                               end if                    
                         end if
                      end do
                   end if
                end do
                
                if(node_remove.eqv..false.) EXIT !last step
18              continue


                   p=1
                   do j=1,num_vess_conns
                      if(vess_conn_tag(j).eqv..true.) then
                         vessel1(p)=vessel1(j)
                         vessel2(p)=vessel2(j)
                         coordstart(p)=coordstart(j)
                         coordend(p)=coordend(j)
                         pit_no(p)=pit_no(j)
                         p=p+1
                      end if
                   end do
                   num_vess_conns=p-1
                   
                   num_pitted_segs_new=0
                   do j=1,num_nodes
                      if(node_tag(j).eqv..true.) then
                         do k=1,number_begin_nodes
                            do l=1,num_pitted_segs(k)-1
                               if(pit_node(k,l).eq.j) then
                                  if(num_pitted_segs_new(k).eq.0) then
                                     num_pitted_segs_new(k)=2
                                  else
                                     num_pitted_segs_new(k)=num_pitted_segs_new(k)+1
                                  end if
                               end if
                            end do
                         end do
                      end if
                   end do
                   
                   num_pitted_segs=0
                   do l=1,number_begin_nodes
                      num_pitted_segs(l)=num_pitted_segs_new(l)
                   end do
                   
                   
                   !       Remove individual nodes that are along backbone & bridge vessels
                   
                   ALLOCATE(node_count(num_nodes))
                   !       Find the number of connections that each node has and store them
                   
                   node_count=0
                   do i=1,num_nodes
                      do 26 k=1,num_segments
                         if((edge_connectivity1(k).eq.i) .or.(edge_connectivity2(k).eq.i)) then
                            if((z(edge_connectivity2(k))-z(edge_connectivity1(k))).gt.(0.5*voxel_size))then
                               node_count(i)=node_count(i)+1
                        
                            else  !pit node (possibly a bridge one)
                               node_count(i)=node_count(i)+3 !inflate this so that you don't eliminate bridge pit connections by accident
                            end if
                         end if
26                       continue
                      end do
                      node_tag=.true.

                      
  
!       For any node that is along a vessel and only has 2 connections (as long as it is not a bridge pit connection) remove the node
    do i=1,number_begin_nodes
!       initial segment
       if(i.le.num_backbone_segs) then
          node2=pit_node(i,1)
       else
          node2=pit_node(i,2)
       end if
       do k=1,num_segments
          if((edge_connectivity2(k).eq.node2).and.(abs(z(edge_connectivity2(k))-z(edge_connectivity1(k))).gt.(0.5*voxel_size))) then
             currentseg=k
          end if
       end do

   do n=1,1000
       do j=1,num_segments
          if((edge_connectivity2(j).eq.node2).and.(abs(z(edge_connectivity2(j))-z(edge_connectivity1(j))).gt.(0.5*voxel_size))) then
             do k=1,num_segments
                if((edge_connectivity1(k).eq.node2).and.(abs(z(edge_connectivity2(k))-z(edge_connectivity1(k)))&
                     &.gt.(0.5*voxel_size))) then
                   nextseg=k
                   exit
                end if
             end do
             if(node_count(node2).eq.2)then
                node_tag(node2)=.false.
                edge_connectivity2(currentseg)=edge_connectivity2(nextseg)
                do l=2,num_edge_points(nextseg)
                   coordx(currentseg,l+num_edge_points(currentseg)-1)=coordx(nextseg,l)
                   coordy(currentseg,l+num_edge_points(currentseg)-1)=coordy(nextseg,l)
                   coordz(currentseg,l+num_edge_points(currentseg)-1)=coordz(nextseg,l)
                   radius(currentseg,l+num_edge_points(currentseg)-1)=radius(nextseg,l)
                end do
                num_edge_points(currentseg)=num_edge_points(currentseg)+num_edge_points(k)-1                      
             else
                currentseg=nextseg
             end if
             node2=edge_connectivity2(nextseg)
          end if
       end do
    end do
    end do
    
    pit_counter=0
    p=1
    do j=1,num_nodes
       if(node_tag(j).eqv..true.) then
          x(p)=x(j)
          y(p)=y(j)
          z(p)=z(j)
      

          do k=1,number_begin_nodes
             do l=1,num_pitted_segs(k)-1
                if(pit_node(k,l).eq.j) then
                   pit_counter(k)=pit_counter(k)+1
                   pit_node(k,pit_counter(k))=p
                   
                end if
             end do
          end do
          
          do k=1,num_segments
             if(edge_connectivity1(k).eq.j) then
                edge_connectivity1(k)=p
             end if
             if(edge_connectivity2(k).eq.j) then
                edge_connectivity2(k)=p
             end if
          end do
          p=p+1
       else
          do i=1,num_segments
             if(edge_connectivity1(i).eq.j) then
                 edge_connectivity1(i)=-1
             end if
             if(edge_connectivity2(i).eq.j) then
                edge_connectivity2(i)=-1
             end if
          end do
       end if
    end do
    
    do k=1,number_begin_nodes
       if(pit_counter(k).ne.0) then
          num_pitted_segs(k)=pit_counter(k)+1
       else
          num_pitted_segs(k)=0
       end if
    end do
    
    q=1
    do j=1,num_segments
       if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
          edge_connectivity1(q)=edge_connectivity1(j)
          edge_connectivity2(q)=edge_connectivity2(j)
          num_edge_points(q)=num_edge_points(j)
          do l=1,num_vess_conns
             if(pit_no(l).eq.j) then
                pit_no(l)=q	
             end if
          end do
          do m=1,num_edge_points(j)
             coordx(q,m)=coordx(j,m)
             coordy(q,m)=coordy(j,m)
             coordz(q,m)=coordz(j,m)
             radius(q,m)=radius(j,m)
          end do
          q=q+1
       end if
    end do
    
    num_nodes=p-1
    num_segments=q-1
    

    ALLOCATE(connect(number_begin_nodes,number_begin_nodes))
    ALLOCATE(connect_number(number_begin_nodes))
    ALLOCATE(seg_tag(number_begin_nodes))
    seg_tag=.false.
    connect=0
    connect_number=0

!Remove segments that are not connected to the backbone in some way
  !Round 1, tag all backbone connections
    do i=1,number_begin_nodes
       do j=1,number_begin_nodes
          if(num_pits_vv(i,j).ne.0) then
             connect_number(i)=connect_number(i)+1
             connect(i,connect_number(i))=j   
             connect_number(j)=connect_number(j)+1
             connect(j,connect_number(j))=i   
          end if
       end do
    end do
    !Round 2, build up connections from backbone segs
    do i=1,num_backbone_segs
       seg_tag(i)=.true.
    end do
    connected=.true.
    do i=1,1000
       if(connected.eqv..false.) EXIT
       connected=.false.
       do j=1,number_begin_nodes
          if(seg_tag(j).eqv..true.) then
             do k=1,connect_number(j)
                if(seg_tag(connect(j,k)).eqv..false.) then
                   seg_tag(connect(j,k))=.true.
                   connected=.true.
                end if
             end do
          end if
       end do
    end do

!Now, we have all of the begin nodes that are connected to backbones, so re-write everything
    node_tag=.true.
    do i=1,number_begin_nodes
       if(seg_tag(i).eqv..false.) then
          do j=1,num_pitted_segs(i)-1
             node_tag(pit_node(i,j))=.false.
          end do
       end if
    end do

    p=1
    do j=1,num_nodes
       if(node_tag(j).eqv..true.) then
          x(p)=x(j)
          y(p)=y(j)
          z(p)=z(j)

          do k=1,number_begin_nodes
             do l=1,num_pitted_segs(k)-1
                if(pit_node(k,l).eq.j) then
                   pit_node(k,l)=p
                end if
             end do
          end do
          do k=1,num_segments
             if(edge_connectivity1(k).eq.j) then
                edge_connectivity1(k)=p
             end if
             if(edge_connectivity2(k).eq.j) then
                edge_connectivity2(k)=p
             end if
          end do
          p=p+1
          
       else
          do i=1,num_segments
             if(edge_connectivity1(i).eq.j) then
                edge_connectivity1(i)=-1
             end if
             if(edge_connectivity2(i).eq.j) then
                edge_connectivity2(i)=-1
             end if
          end do
          
       end if
    end do

    q=1
    do j=1,num_segments
       if((edge_connectivity1(j).ne.-1).and.(edge_connectivity2(j).ne.-1)) then
          edge_connectivity1(q)=edge_connectivity1(j)
          edge_connectivity2(q)=edge_connectivity2(j)
          num_edge_points(q)=num_edge_points(j)
          do l=1,num_vess_conns
             if(pit_no(l).eq.j) then
                pit_no(l)=q	
             end if
          end do
          do m=1,num_edge_points(j)
             coordx(q,m)=coordx(j,m)
             coordy(q,m)=coordy(j,m)
             coordz(q,m)=coordz(j,m)
             radius(q,m)=radius(j,m)
          end do
          q=q+1
       end if
    end do
    num_nodes=p-1
    num_segments=q-1
    
    q=number_begin_nodes
    count=0
    vess_conn_tag=.true.

    do k=num_backbone_segs+1,q
       if(seg_tag(k).eqv..false.) then
          do j=k-count,q-1
             num_vv_connections(j)=num_vv_connections(j+1)
             num_pit_connections(j)=num_pit_connections(j+1)
             v_diam(j)=v_diam(j+1)
             num_pits_vv(j,:)=num_pits_vv(j+1,:)
             num_pits_vv(:,j)=num_pits_vv(:,j+1)
             num_pitted_segs(j)=num_pitted_segs(j+1)
             pit_node(j,:)=pit_node(j+1,:)
          end do
          do j=1,num_vess_conns
             if((vessel1(j).eq.(k-count)).or.(vessel2(j).eq.(k-count))) then
                vess_conn_tag(j)=.false.
             end if
          end do
          do j=1,num_vess_conns
             if(vessel1(j).gt.(k-count))  vessel1(j)=vessel1(j)-1
             if(vessel2(j).gt.(k-count))  vessel2(j)=vessel2(j)-1            
          end do

          number_begin_nodes=number_begin_nodes-1
          r=r-1
          count=count+1
       end if
    end do

    p=1
    do k=1,num_vess_conns
       if(vess_conn_tag(k).eqv..true.) then
          vessel1(p)=vessel1(k)
          vessel2(p)=vessel2(k)
          coordstart(p)=coordstart(k)
          coordend(p)=coordend(k)
          pit_no(p)=pit_no(k)
          p=p+1
       end if
    end do
    num_vess_conns=p-1


    DEALLOCATE(seg_tag)
    DEALLOCATE(connect)
    DEALLOCATE(connect_number)
    DEALLOCATE(node_tag)
    DEALLOCATE(node_count)
 end if
 

	   num_bridges=0

!       save all bridge segments to a file for flow program
    open(unit=80,file='bridge_segs.txt',status='unknown')
    do 92 j=num_backbone_segs+1,number_begin_nodes
       if(num_pitted_segs(j).gt.0) then
          do k=1,num_pitted_segs(j)-1     
             do l=1,num_segments
                if((edge_connectivity1(l).eq.pit_node(j,k)).and.(ABS(z(edge_connectivity1(l))-&
                     &z(edge_connectivity2(l))).lt.0.5*voxel_size)) then
                   write(80,*) l
                end if
                if((edge_connectivity2(l).eq.pit_node(j,k)).and.(ABS(z(edge_connectivity1(l))-&
                     &z(edge_connectivity2(l))).lt.0.5*voxel_size)) then
                   write(80,*) l           
                end if
             end do
          end do
          num_bridges=num_bridges+1
       end if
92 continue
          
       close(80)


       if(bridge_tag.eq.1) then
          open(unit=49,file='output-bridge_properties.txt',status='unknown')
          write(49,*) 'Number of bridges: ', num_bridges
          close(49)
       end if
       
       
       open(unit=44,file='output-angles_of_connections.txt',status='unknown')
       write(44,*)  'Angle relative to radial dir. (degrees)   Radial Position (m)'
       
       open(unit=45,file='output-angles_of_connections_bridges.txt', status='unknown')
       write(45,*)  'Angle relative to radial dir. (degrees)   Radial Position (m)'
       
       do j=1,number_begin_nodes
          do 89 i=1,num_pitted_segs(j)-1
             
             !       Analyze pits
             !       Find pit connection vector
             
             !       Find node that is closer to center
             
             !       In order to avoid duplicate vv connections, only look at edge1
             do k=1,num_segments
		if((edge_connectivity1(k).eq.pit_node(j,i)).and.((z(edge_connectivity2(k))&
       & -z(edge_connectivity1(k))).lt.0.5*voxel_size))then
     do m=1,number_begin_nodes
        do n=1,num_pitted_segs(m)-1
           if(pit_node(m,n).eq.edge_connectivity2(k)) then
              s=NINT((z(edge_connectivity1(k)))/(sampling_freq*voxel_size)-vessel_begin(j))+1
              ax=x(edge_connectivity1(k))-xcenter_interp(s)
              ay=y(edge_connectivity1(k))-ycenter_interp(s)
              bx=(x(edge_connectivity2(k))+x(edge_connectivity1(k)))/2-xcenter_interp(s)
              by=(y(edge_connectivity2(k))+y(edge_connectivity1(k)))/2-ycenter_interp(s)
              cx=(x(edge_connectivity2(k))-x(edge_connectivity1(k)))/2
              cy=(y(edge_connectivity2(k))-y(edge_connectivity1(k)))/2
              aseg=dsqrt(ax*ax+ay*ay)
              bseg=dsqrt(bx*bx+by*by)
              cseg=dsqrt(cx*cx+cy*cy)
              pit_connection_angle=180/3.14159*ACOS((bseg*bseg+cseg*cseg-aseg*aseg)/(2*bseg*cseg))
              
              !       convert angle to different format
              pit_connection_angle=-(pit_connection_angle-180)
              if(pit_connection_angle.gt.90) then
                 pit_connection_angle=pit_connection_angle-180
              end if
              dist_center=dsqrt((x(edge_connectivity1(k))-xcenter_interp(s))**2+(y(edge_connectivity1(k))-ycenter_interp(s))**2)
              if((j.le.num_backbone_segs).and.(m.le.num_backbone_segs)) then !non-bridge connection
                 write(44,'(F18.14,25x,F21.19)') pit_connection_angle,dist_center
              else !bridge connection
                 write(45,'(F18.14,25x,F21.19)') pit_connection_angle,dist_center
              end if
           end if
        end do
     end do
  end if
end do
89 continue
	end do
	close(44)
	close(45)

!       BELOW IS A DIFFERENT OPTION FOR THE PIT AREA--I.E. A SERIES OF PITFIELDS RATHER THAN TOTAL PIT AREA

	if(pitting_tag.eq.1) then

	   total_pitarea=0
	   total_pitarea_bridges=0
	   num_vess_bridge_conns=0
	   
	   open(unit=80,file='output-pit_area.txt',status='unknown')
	   write(80,*) 'Vessel#1  Vessel#2     Pit area between 2 vessels'
	   do i=1,num_backbone_segs
	      do j=1,num_backbone_segs
		 if(num_pits_vv(i,j).ne.0) then
		    if(v_diam(i).gt.v_diam(j)) then
		       write(80,'(I3,7x,I3,10x,E22.15)') i, j, num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq
		       total_pitarea=total_pitarea+num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq
		    else
		       write(80,'(I3,7x,I3,10x,E22.15)') i, j, num_pits_vv(i,j)*voxel_size*v_diam(i)*sampling_freq
		       total_pitarea=total_pitarea+num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq
		    end if
		 end if
	      end do
	   end do
	   write(80,*)
	   if(num_bridges.gt.0) then
	      write(80,*) 'Bridge connections'
	      do i=1,number_begin_nodes
		 do j=num_backbone_segs+1,number_begin_nodes
		    if(num_pits_vv(i,j).ne.0) then
		       if(v_diam(i).gt.v_diam(j)) then
			  write(80,'(I3,7x,I3,10x,E22.15)') i, j, num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq
			  total_pitarea_bridges=total_pitarea_bridges+num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq
		       else
			  write(80,'(I3,7x,I3,10x,E22.15)') i, j, num_pits_vv(i,j)*voxel_size*v_diam(i)*sampling_freq
			  total_pitarea_bridges=total_pitarea_bridges+num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq
		       end if
		    end if
		 end do
	      end do
	   end if
	   close(80)

	   open(unit=87,file='output-pit_area_by_pit.txt',status='unknown')
	   write(87,*) 'Vessel#1  Vessel#2     Pit area between 2 vessels   Starting Coordinate'
	   do i=1,num_vess_conns
	      if((vessel1(i).le.num_backbone_segs).and.(vessel2(i).le.num_backbone_segs))then
		 if(v_diam(vessel1(i)).gt.v_diam(vessel2(i))) then
		    write(87,'(I3,7x,I3,10x,E22.15,15x,I5)') vessel1(i), vessel2(i),(coordend(i)-coordstart(i)+1)&
		    &*sampling_freq*voxel_size*v_diam(vessel2(i)),coordstart(i)
		 else
		    write(87,'(I3,7x,I3,10x,E22.15,15x,I5)') vessel1(i), vessel2(i),(coordend(i)-coordstart(i)+1)&
		    &*sampling_freq*voxel_size*v_diam(vessel1(i)),coordstart(i)
		 end if
	      end if
	   end do
	   write(87,*)
	   if(num_bridges.gt.0) then
	      write(87,*) 'Bridge Connections'
	      do i=1,num_vess_conns
		 if((vessel1(i).gt.num_backbone_segs).or.(vessel2(i).gt.num_backbone_segs))then
		    if(v_diam(vessel1(i)).gt.v_diam(vessel2(i))) then
		       write(87,'(I3,7x,I3,10x,E22.15,15x,I5)') vessel1(i), vessel2(i),(coordend(i)-coordstart(i)+1)&
		       &*sampling_freq*voxel_size*v_diam(vessel2(i)),coordstart(i)
         num_vess_bridge_conns=num_vess_bridge_conns+1
		    else
		       write(87,'(I3,7x,I3,10x,E22.15,15x,I5)') vessel1(i), vessel2(i),(coordend(i)-coordstart(i)+1)&
		       &*sampling_freq*voxel_size*v_diam(vessel1(i)),coordstart(i)
      num_vess_bridge_conns=num_vess_bridge_conns+1
		    end if
		 end if
	      end do
	   end if
	   close(87)
	   
	   deviation_sq=0
	   if((num_vess_conns-num_vess_bridge_conns).gt.0) then
	      avg_pitarea=total_pitarea/(num_vess_conns-num_vess_bridge_conns)
	   else
	      avg_pitarea=0
	   end if
	   
	   if(num_vess_bridge_conns.gt.0) then
	      avg_pitarea_bridges=total_pitarea_bridges/num_vess_bridge_conns
	   end if

	   do i=1,number_begin_nodes
	      do j=1,number_begin_nodes
		 if(num_pits_vv(i,j).ne.0) then
		    if((i.le.num_backbone_segs).and.(j.le.num_backbone_segs)) then
		       if(v_diam(i).gt.v_diam(j)) then
			  deviation_sq=deviation_sq+(num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq-avg_pitarea)**2.d0
		       else
			  deviation_sq=deviation_sq+(num_pits_vv(i,j)*voxel_size*v_diam(i)*sampling_freq-avg_pitarea)**2.d0  
		       end if
		    elseif(num_vess_bridge_conns.gt.0) then
		       if(v_diam(i).gt.v_diam(j)) then
			  deviation_sq_bridges=deviation_sq_bridges+(num_pits_vv(i,j)*voxel_size*v_diam(j)*sampling_freq-avg_pitarea_bridges)**2.d0
		       else
			  deviation_sq_bridges=deviation_sq_bridges+(num_pits_vv(i,j)*voxel_size*v_diam(i)*sampling_freq-avg_pitarea_bridges)**2.d0  
		       end if  
		    end if
		 end if
	      end do
	   end do
	   
	   if((num_vess_conns-num_vess_bridge_conns).gt.0) then
	      stdev_pitarea=sqrt(deviation_sq/(num_vess_conns-num_vess_bridge_conns))
	   else
	      stdev_pitarea=0
	   end if
	   
	   if(num_vess_bridge_conns.gt.0) then
	      stdev_pitarea_bridges=sqrt(deviation_sq_bridges/num_vess_bridge_conns)
	   end if

	   open(unit=47,file='output-pit_properties.txt',status='unknown')
	   write(47,*) 'Number of intervessel pitfields: ', num_vess_conns-num_vess_bridge_conns
	   write(47,*) 'Total Pit Area (meters^2): ', total_pitarea
	   write(47,*) 'Average Pit Area per Connection (meters^2): ', avg_pitarea
	   write(47,*) 'Standard Deviation (meters^2): ', stdev_pitarea
	   if((bridge_tag.eq.1).and.(num_vess_bridge_conns.gt.0)) then
	      write(47,*)
	      write(47,*) 'Number of vessel-bridge pitfields: ', num_vess_bridge_conns
	      write(47,*) 'Total Bridge Pit Area (meters^2): ', total_pitarea_bridges
	      write(47,*) 'Average Bridge Pit Area per Connection (meters^2): ', avg_pitarea_bridges
	      write(47,*) 'Standard Deviation (meters^2): ', stdev_pitarea_bridges
	   end if
	   close(47)
	   
	   if((bridge_tag.eq.1).and.(num_vess_bridge_conns.gt.0)) then
	      write(55,*)
              write(55,*) 'Bridge no.   Bridge Diameter           Radial Coordinate          x                            y    '&
        &'                   z'
	      
	      sum_diameters=0
	      deviation_sq=0
	      
	      do 63 j=num_backbone_segs+1,num_backbone_segs+num_bridges	
		 dist_center=dsqrt((x(edge_connectivity1(j))-xcenter_interp(INT((max_sampled_points_per_seg-extra_points)/2)))**2&
		 &+(y(edge_connectivity1(j))-ycenter_interp(INT((max_sampled_points_per_seg-extra_points)/2)))**2)+dist_center	 
		 write(55,'(I3,11x,F23.21,3x,F23.21,5x,F23.21,5x,F23.21,5x,F23.21)')j, v_diam(j),dist_center/num_edge_points(j)&
               &,x(edge_connectivity1(j)),y(edge_connectivity1(j)),z(edge_connectivity1(j))
		 sum_diameters=sum_diameters+v_diam(j)
 63	      continue

	      if(r.gt.num_backbone_segs) then
		 avg_diameter=sum_diameters/num_bridges
		 do 64 j=num_backbone_segs+1,r
		    do 67 i=1,num_edge_points(j)
		       deviation_sq=(2.d0*radius(j,i)-avg_diameter)**2.d0/num_edge_points(j)+deviation_sq
 67		    continue
 64		 continue
		 stdev=sqrt(deviation_sq/num_bridges)
		 write(36,*)
		 write(36,*) 'Number of Bridges: ',num_bridges
		 write(36,*) 'Average Bridge Diameter (meters): ', avg_diameter
		 write(36,*) 'Standard Deviation (meters): ', stdev
	      end if
	   end if
	   
	   close(55)
	   close(36)
	   
	   
	   open(unit=88,file='output-intervessel_connections.txt',status='unknown')
	   write(88,*) 'Vessel        Number of intervessel connections (where connections are only counted once)'
	   num_isolated=0
	   num_oneconnect=0
	   num_twoplus=0
	   do i=1,num_backbone_segs
	      write(88,'(I3,10x,I3)') i, num_vv_connections(i)
	      if(num_vv_connections(i).eq.0) then
		 num_isolated=num_isolated+1
	      end if
	      if(num_vv_connections(i).eq.1) then
		 num_oneconnect=num_oneconnect+1
	      end if
	      if(num_vv_connections(i).ge.2) then
		 num_twoplus=num_twoplus+1
	      end if
	   end do
	   
	   write(88,*) '0 connections  1 connection  >2 connections'
	   write(88,'(I3,12x,I3,11x,I3)') num_isolated,num_oneconnect,num_twoplus
	   write(88,*) 'Percentages'
	   write(88,*) '0 connections  1 connection  >2 connections'
	   write(88,'(F9.5,6x,F9.5,5x,F9.5)') DBLE(num_isolated)/DBLE(num_backbone_segs)*100,&
	   &DBLE(num_oneconnect)/DBLE(num_backbone_segs)*100,DBLE(num_twoplus)/DBLE(num_backbone_segs)*100
	   close(88)
	end if
	DEALLOCATE(pit_counter)
	DEALLOCATE(num_pitted_segs_new)
	DEALLOCATE(vessel1)
	DEALLOCATE(vessel2)
	DEALLOCATE(num_pits_vv)
	DEALLOCATE(num_vv_connections)
	DEALLOCATE(num_pit_connections)
	END SUBROUTINE model_pitfield


 SUBROUTINE specify_nodes
   USE gcmmn
   IMPLICIT NONE
   INTEGER :: i,j,k,node_counter
   LOGICAL :: edge1,edge2,bridge_node
   
   
   !       Rewrite files after renumbering scheme
   open(unit=29,file='top_nodes.txt',status='unknown')
   open(unit=30,file='bottom_nodes.txt',status='old')
   open(unit=31,file='internal_nodes.txt',status='unknown')
      
   do 32 i=1,num_nodes
      bridge_node=.false.
      if((pitting_tag.eq.1).and.(bridge_tag.eq.1)) then
         !       determine whether node is a bridge node or a vessel node
         do k=num_backbone_segs+1,number_begin_nodes
            do j=1,num_pitted_segs(k)-1
               if(pit_node(k,j).eq.i) then
                  bridge_node=.true.
                  GOTO 48
               end if
            end do
         end do
      end if
48    continue
      node_counter=0
      edge1=.false.
      edge2=.false.
      do k=1,num_segments
         if(edge_connectivity1(k).eq.i) then
            node_counter=node_counter+1
            edge1=.true.
         elseif(edge_connectivity2(k).eq.i) then
            node_counter=node_counter+1
            edge2=.true.
         end if
         if(k.eq.num_segments) then
            if(node_counter.eq.1) then
               if(edge1.eqv..true.) then
                  write(30,*) i
               elseif(edge2.eqv..true.) then
                  if(bridge_node.eqv..false.) then
                     write(29,*) i
                  end if
               end if
            else
               write(31,*) i
            end if
         end if
      end do
32    continue
      
      
      close(29)
      close(30)
      close(31)

      if(pitting_tag.eq.1) then
         DEALLOCATE(pit_node)	
         DEALLOCATE(num_pitted_segs)
      end if
      DEALLOCATE(v_diam)
    END SUBROUTINE specify_nodes
    
	SUBROUTINE avizo_export
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j

	num_points=0
	do i=1,num_segments
	   num_points=num_points+num_edge_points(i)
	end do

	open(unit=85,file='avizo_export.am',status='unknown')
	write(85,'(a)') '# AmiraMesh 3D ASCII 2.0'
	write(85,*)
	write(85,*)
        write(85,'("define VERTEX",i7)') num_nodes
        write(85,'(a,I7,1x)')'define EDGE', num_segments
        write(85,'(a,I7)')'define POINT', num_points
	write(85,*)
        write(85,'(a)')'Parameters {'
        write(85,'(a)')'    ContentType "HxSpatialGraph"'
        write(85,'(a)')'}'
	write(85,*)
	write(85,'(a)')'VERTEX { float[3] VertexCoordinates } @1'
	write(85,'(a)')'EDGE { int[2] EdgeConnectivity } @2'
	write(85,'(a)')'EDGE { int NumEdgePoints } @3'
	write(85,'(a)')'POINT { float[3] EdgePointCoordinates } @4'
	write(85,'(a)')'POINT { float thickness } @5'
	write(85,'(a)')'EDGE { float FlowRate } @6'
	write(85,*)
        write(85,'(a)')'# Data section follows'
	write(85,'(a)')'@1'
	do 86 i=1,num_nodes
!       Convert back to microns
	   write(85,'(3(F13.7,1x))')x(i)/convert_factor,y(i)/convert_factor,z(i)/convert_factor
 86	continue
	write(85,*)
	write(85,'(a)')'@2'
	do 87 i=1,num_segments
!       Subtract 1 because numbering starts from zero
	   write(85,'(2I6)') edge_connectivity1(i)-1,edge_connectivity2(i)-1
 87	continue
	write(85,*)
	write(85,'(a)')'@3'
	do 88 i=1,num_segments
	   write(85,'(I5)') num_edge_points(i)
 88	continue
	write(85,*)
	write(85,'(a)')'@4'
	do 89 i=1,num_segments
	   do 93 j=1,num_edge_points(i)
!       Convert back to microns
	   write(85,'(3(F13.7,1x))') coordx(i,j)/convert_factor,coordy(i,j)/convert_factor,coordz(i,j)/convert_factor
 93	   continue
 89	continue
	write(85,*)
	write(85,'(a)')'@5'
	do 90 i=1,num_segments
	   do 91 j=1,num_edge_points(i)
!       Convert back to microns
	   write(85,'(F13.7,1x)') radius(i,j)/convert_factor
 91	   continue
 90	continue
	write(85,*)
	write(85,'(a)')'@6'
	do 32 i=1,num_segments
	   if(z(edge_connectivity1(i)).eq.z(edge_connectivity2(i))) then
	      write(85,'(I1,1x)') 1
	   else
	      write(85,'(I1,1x)') 0
	   end if
 32	continue
        close(85)

	END SUBROUTINE avizo_export


	SUBROUTINE avizo_flow_input
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j

	num_points=0
	do i=1,num_segments
	   num_points=num_points+num_edge_points(i)
	end do

	open(unit=85,file='avizo_flow_input.am',status='unknown')
	write(85,'(a)') '# AmiraMesh 3D ASCII 2.0'
	write(85,*)
	write(85,*)
        write(85,'("define VERTEX",i7)') num_nodes
        write(85,'(a,I7,1x)')'define EDGE', num_segments
        write(85,'(a,I7)')'define POINT', num_points
	write(85,*)
        write(85,'(a)')'Parameters {'
        write(85,'(a)')'    ContentType "HxSpatialGraph"'
        write(85,'(a)')'}'
	write(85,*)
	write(85,'(a)')'VERTEX { float[3] VertexCoordinates } @1'
	write(85,'(a)')'EDGE { int[2] EdgeConnectivity } @2'
	write(85,'(a)')'EDGE { int NumEdgePoints } @3'
	write(85,'(a)')'POINT { float[3] EdgePointCoordinates } @4'
	write(85,'(a)')'POINT { float thickness } @5'
	write(85,'(a)')'POINT { float Length } @6'
	write(85,*)
        write(85,'(a)')'# Data section follows'
	write(85,'(a)')'@1'
	do 86 i=1,num_nodes
!       Convert back to microns
	   write(85,'(3(F14.8,1x))')x(i)/convert_factor,y(i)/convert_factor,z(i)/convert_factor
 86	continue
	write(85,*)
	write(85,'(a)')'@2'
	do 87 i=1,num_segments
!       Subtract 1 because numbering starts from zero
	   write(85,'(2I5)') edge_connectivity1(i)-1,edge_connectivity2(i)-1
 87	continue
	write(85,*)
	write(85,'(a)')'@3'
	do 88 i=1,num_segments
	   write(85,'(I4)') num_edge_points(i)
 88	continue
	write(85,*)
	write(85,'(a)')'@4'

	do 89 i=1,num_segments
	   do 93 j=1,num_edge_points(i)
!       Convert back to microns
	   write(85,'(3(F14.8,1x))') coordx(i,j)/convert_factor,coordy(i,j)/convert_factor,coordz(i,j)/convert_factor
 93	   continue
 89	continue
	write(85,*)
	write(85,'(a)')'@5'
	do 90 i=1,num_segments
	   do 91 j=1,num_edge_points(i)
!       Convert back to microns
	   write(85,'(F14.8,1x)') radius(i,j)/convert_factor
 91	   continue
 90	continue
	write(85,*)
!       Length of pitfield connection
	write(85,'(a)')'@6'

	do 32 i=1,num_segments
	   do 38  j=1,num_vess_conns
	      if(pitting_tag.eq.1) then
		 if(pit_no(j).eq.i) then
		    write(85,'(I4,1x)') coordend(j)-coordstart(j)+1
		    GOTO 32
		 end if
	      else
		 write(85,'(I4,1x)') 0
	      end if
 38	   continue

	   !If you don't find any segments, then it's not a pit connection
!       EVENTUALLY CHANGE THIS TO 1?? 
	   write(85,'(I4,1x)') 0
 32	continue
        close(85)
	DEALLOCATE(coordx)
	DEALLOCATE(coordy)
	DEALLOCATE(coordz)
	if(pitting_tag.eq.1) then
	   DEALLOCATE(pit_no)
	   DEALLOCATE(coordstart)
	   DEALLOCATE(coordend)
	end if
	END SUBROUTINE avizo_flow_input



	SUBROUTINE avizo_store
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j
	DOUBLE PRECISION :: coordx1,coordy1,coordz1
        DOUBLE PRECISION :: point_thick



	open(unit=85,file='avizo_store.am',status='unknown')
	write(85,'(a)') '# AmiraMesh 3D ASCII 2.0'
	write(85,*)
	write(85,*)
        write(85,'("define VERTEX",i7)') num_nodes
        write(85,'(a,I7,1x)')'define EDGE', num_segments
        write(85,'(a,I7)')'define POINT', num_points
	write(85,*)
        write(85,'(a)')'Parameters {'
        write(85,'(a)')'    ContentType "HxSpatialGraph"'
        write(85,'(a)')'}'
	write(85,*)
	write(85,'(a)')'VERTEX { float[3] VertexCoordinates } @1'
	write(85,'(a)')'EDGE { int[2] EdgeConnectivity } @2'
	write(85,'(a)')'EDGE { int NumEdgePoints } @3'
	write(85,'(a)')'POINT { float[3] EdgePointCoordinates } @4'
	write(85,'(a)')'POINT { float thickness } @5'
	write(85,*)
        write(85,'(a)')'# Data section follows'
	write(85,'(a)')'@1'
	do 86 i=1,num_nodes
!       Convert back to microns
	   write(85,'(3(F14.8,1x))')x(i)/convert_factor,y(i)/convert_factor,z(i)/convert_factor
 86	continue
	write(85,*)
	write(85,'(a)')'@2'
	do 87 i=1,num_segments
!       Subtract 1 because numbering starts from zero
	   write(85,'(2I6)') edge_connectivity1(i)-1,edge_connectivity2(i)-1
 87	continue
	write(85,*)
	write(85,'(a)')'@3'
	do 88 i=1,num_segments
	read(11,*) num_edge_points(i)
	   write(85,'(I5)') num_edge_points(i)
 88	continue
	write(85,*)

	read(11,*)
	read(11,*)

	write(85,'(a)')'@4'
	do 89 i=1,num_segments
	   do 93 j=1,num_edge_points(i)
!       Convert back to microns
        read(11,*) coordx1, coordy1, coordz1
	write(85,'(3(F14.8,1x))') coordx1,coordy1, coordz1
 93	   continue
 89	continue

	read(11,*)
	read(11,*)
	write(85,*)
	write(85,'(a)')'@5'
	do 90 i=1,num_segments
	   do 91 j=1,num_edge_points(i)
	      read(11,*) point_thick
!       Convert back to microns
	      write(85,'(F14.8,1x)') point_thick
 91	   continue
 90	continue

	close(11)

        close(85)
	END SUBROUTINE avizo_store


	SUBROUTINE avizo_test
	USE gcmmn
	IMPLICIT NONE
	INTEGER :: i,j

	num_points=0
	do i=1,num_segments
	   num_points=num_points+num_edge_points(i)
	end do


	open(unit=85,file='avizo_test.am',status='unknown')
	write(85,'(a)') '# AmiraMesh 3D ASCII 2.0'
	write(85,*)
	write(85,*)
        write(85,'("define VERTEX",i7)') num_nodes
        write(85,'(a,I7,1x)')'define EDGE', num_segments
        write(85,'(a,I7)')'define POINT', num_points
	write(85,*)
        write(85,'(a)')'Parameters {'
        write(85,'(a)')'    ContentType "HxSpatialGraph"'
        write(85,'(a)')'}'
	write(85,*)
	write(85,'(a)')'VERTEX { float[3] VertexCoordinates } @1'
	write(85,'(a)')'EDGE { int[2] EdgeConnectivity } @2'
	write(85,'(a)')'EDGE { int NumEdgePoints } @3'
	write(85,'(a)')'POINT { float[3] EdgePointCoordinates } @4'
	write(85,'(a)')'POINT { float thickness } @5'
	write(85,*)
        write(85,'(a)')'# Data section follows'
	write(85,'(a)')'@1'



	do 86 i=1,num_nodes
!       Convert back to microns
	   write(85,'(3(F14.8,1x))')x(i)/convert_factor,y(i)/convert_factor,z(i)/convert_factor
 86	continue
	write(85,*)
	write(85,'(a)')'@2'

	do 87 i=1,num_segments
!       Subtract 1 because numbering starts from zero
	   write(85,'(2I5)') edge_connectivity1(i)-1,edge_connectivity2(i)-1
 87	continue
	write(85,*)
	write(85,'(a)')'@3'
	do 88 i=1,num_segments
	   write(85,'(I4)') num_edge_points(i)
 88	continue
	write(85,*)

	write(85,'(a)')'@4'

	do 89 i=1,num_segments
	   do 93 j=1,num_edge_points(i)
!       Convert back to microns
	   write(85,'(3(F14.8,1x))') coordx_in(i,j)/convert_factor, coordy_in(i,j)/convert_factor,coordz_in(i,j)/convert_factor
 93	   continue
 89	continue
	write(85,*)
	write(85,'(a)')'@5'
	do 90 i=1,num_segments
	   do 91 j=1,num_edge_points(i)
!       Convert back to microns
	   write(85,'(F14.8,1x)') radius_in(i,j)/convert_factor
 91	   continue
 90	continue
        close(85)
	END SUBROUTINE avizo_test




	SUBROUTINE outro
	USE gcmmn
	IMPLICIT NONE

	DEALLOCATE(num_edge_points)
	DEALLOCATE(x)
	DEALLOCATE(y)
	DEALLOCATE(z)
	DEALLOCATE(radius)
	DEALLOCATE(edge_connectivity1)
	DEALLOCATE(edge_connectivity2)
	DEALLOCATE(xcenter_interp)
	DEALLOCATE(ycenter_interp)
	DEALLOCATE(vessel_begin)
	DEALLOCATE(vessel_finish)

	END SUBROUTINE outro
